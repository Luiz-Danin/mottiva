/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  luiz
 * Created: 27/05/2020
 */
-- MySQL Workbench Forward Engineering

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `rota` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_rota` VARCHAR(150) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `area` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_area` VARCHAR(150) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pdv` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_rota` INT NOT NULL,
  `nome_pdv` VARCHAR(250) NULL,
  `funcionario` VARCHAR(150) NULL,
  `telefone` VARCHAR(20) NULL,
  `endereco` VARCHAR(250) NULL,
  `data_cadastro` DATETIME NULL,
  `data_edicao` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rota_ponto1_idx` (`id_rota` ASC),
  CONSTRAINT `fk_rota_ponto1`
    FOREIGN KEY (`id_rota`)
    REFERENCES `rota` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `entrega` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_pdv` INT NOT NULL,
  `data_entrega` DATETIME NULL,
  `data_entrrega_final` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_entrega_rota_entrega1_idx` (`id_pdv` ASC),
  CONSTRAINT `fk_entrega_rota_entrega1`
    FOREIGN KEY (`id_pdv`)
    REFERENCES `pdv` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `area_rota` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_area` INT NOT NULL,
  `id_rota` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_area_entrega_has_ponto_entrega_ponto_entrega1_idx` (`id_rota` ASC),
  INDEX `fk_area_entrega_has_ponto_entrega_area_entrega_idx` (`id_area` ASC),
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_area_entrega`
    FOREIGN KEY (`id_area`)
    REFERENCES `area` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_ponto_entrega1`
    FOREIGN KEY (`id_rota`)
    REFERENCES `rota` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `edicao` VARCHAR(100) NULL,
  `nome` VARCHAR(500) NULL,
  `descricao` TEXT NULL,
  `peso` DOUBLE NULL,
  `valor` DOUBLE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `situacao_financeiro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `item_produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_entrega` INT NOT NULL,
  `id_produto` INT NOT NULL,
  `encalhe` VARCHAR(500) NULL,
  `reparte` VARCHAR(500) NULL,
  `data_cadastro` DATETIME NULL,
  `data_devolucao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_item_entregra_produto1_idx` (`id_produto` ASC),
  INDEX `fk_item_entregra_entrega1_idx` (`id_entrega` ASC),
  CONSTRAINT `fk_item_entregra_produto1`
    FOREIGN KEY (`id_produto`)
    REFERENCES `produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_entregra_entrega1`
    FOREIGN KEY (`id_entrega`)
    REFERENCES `entrega` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `financeiro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_item` INT NOT NULL,
  `id_situacao` INT NOT NULL,
  `valor` VARCHAR(100) NULL,
  `data` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_financeiro_tipo_financeiro1_idx` (`id_situacao` ASC),
  INDEX `fk_financeiro_item_entregra1_idx` (`id_item` ASC),
  CONSTRAINT `fk_financeiro_tipo_financeiro1`
    FOREIGN KEY (`id_situacao`)
    REFERENCES `situacao_financeiro` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_financeiro_item_entregra1`
    FOREIGN KEY (`id_item`)
    REFERENCES `item_produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` INT NOT NULL,
  `perfil_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_usuario_perfil1_idx` (`perfil_id` ASC),
  CONSTRAINT `fk_usuario_perfil1`
    FOREIGN KEY (`perfil_id`)
    REFERENCES `perfil` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;