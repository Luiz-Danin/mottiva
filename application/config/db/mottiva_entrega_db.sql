-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 24-Nov-2020 às 23:12
-- Versão do servidor: 10.4.13-MariaDB
-- versão do PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `mottiva_entrega_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nome_area` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `area`
--

INSERT INTO `area` (`id`, `nome_area`) VALUES
(1, 'Belém'),
(2, 'Ananindeua/Marituba'),
(3, 'Icoaraci'),
(4, 'BL do PA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `area_rota`
--

CREATE TABLE `area_rota` (
  `id` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `area_rota`
--

INSERT INTO `area_rota` (`id`, `id_area`, `id_rota`) VALUES
(7, 1, 1),
(8, 1, 2),
(9, 2, 3),
(10, 2, 4),
(11, 3, 5),
(12, 3, 6),
(13, 4, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `autoria_acesso`
--

CREATE TABLE `autoria_acesso` (
  `id` int(11) NOT NULL,
  `session_id` varchar(100) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `localizacao` varchar(200) NOT NULL,
  `is_robot` tinyint(1) NOT NULL,
  `is_mobile` varchar(200) NOT NULL,
  `agent_string` varchar(50) NOT NULL,
  `plataforma` varchar(50) NOT NULL,
  `navegador` varchar(50) NOT NULL,
  `sistema` varchar(50) NOT NULL,
  `data_acesso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `autoria_acesso`
--

INSERT INTO `autoria_acesso` (`id`, `session_id`, `id_usuario`, `ip`, `localizacao`, `is_robot`, `is_mobile`, `agent_string`, `plataforma`, `navegador`, `sistema`, `data_acesso`) VALUES
(1, '19ca14e7ea6328a42e0eb13d585e4c22', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-27 21:40:51'),
(2, 'a1d0c6e83f027327d8461063f4ac58a6', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-27 22:43:43'),
(3, 'c81e728d9d4c2f636f067f89cc14862c', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-28 00:16:02'),
(4, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '45.235.222.117', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:45.235.222.117', 0, '1', 'Mozilla/5.0 (Linux; Android 6.0; LG-X230) AppleWeb', 'Android', 'Chrome - 81.0.4044.138', 'Android', '2020-05-28 18:37:04'),
(5, '98f13708210194c475687be6106a3b84', 1, '45.235.222.117', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:45.235.222.117', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ', 'Windows 7', 'Firefox - 76.0', 'Windows 7', '2020-05-28 18:39:54'),
(6, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-28 22:20:32'),
(7, '6512bd43d9caa6e02c990b0a82652dca', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ', 'Windows 7', 'Firefox - 76.0', 'Windows 7', '2020-05-29 09:13:18'),
(8, 'd9d4f495e875a2e075a1a4a6e1b9770f', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-29 19:33:59'),
(9, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '1', 'Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple', 'Android', 'Chrome - 83.0.4103.83', 'Android', '2020-05-29 21:27:06'),
(10, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '2804:14c:5988:93a2:3055:dca4:141d:6072', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:3055:dca4:141d:6072', 0, '1', 'Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple', 'Android', 'Chrome - 83.0.4103.83', 'Android', '2020-05-30 13:17:14'),
(11, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '186.207.197.131', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-30 17:23:03'),
(12, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '2804:14c:5988:93a2:4d08:66c1:4eea:5be7', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:4d08:66c1:4eea:5be7', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-30 21:19:16'),
(13, 'e4da3b7fbbce2345d7772b0674a318d5', 1, '2804:14c:5988:93a2:d8da:77b7:45f8:8dde', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:d8da:77b7:45f8:8dde', 0, '1', 'Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple', 'Android', 'Chrome - 83.0.4103.83', 'Android', '2020-05-31 15:20:05'),
(14, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93a2:14c7:85f9:4a67:632c', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:14c7:85f9:4a67:632c', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-05-31 20:53:34'),
(15, 'a87ff679a2f3e71d9181a67b7542122c', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ', 'Windows 7', 'Firefox - 76.0', 'Windows 7', '2020-06-01 14:58:02'),
(16, 'c74d97b01eae257e44aa9d5bade97baf', 1, '2804:14c:5988:93a2:254f:b20c:3967:541e', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:254f:b20c:3967:541e', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.61', 'Linux', '2020-06-13 15:48:48'),
(17, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93a2:85ce:3814:d599:d1ed', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:85ce:3814:d599:d1ed', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 83.0.4103.97', 'Linux', '2020-06-14 17:44:36'),
(18, 'aab3238922bcc25a6f606eb525ffdc56', 1, '2804:14c:5988:93a2:bdb1:d17:3363:df1e', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:bdb1:d17:3363:df1e', 0, '1', 'Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple', 'Android', 'Chrome - 83.0.4103.101', 'Android', '2020-06-14 22:20:37'),
(19, '8f14e45fceea167a5a36dedd4bea2543', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:77.0) ', 'Windows 7', 'Firefox - 77.0', 'Windows 7', '2020-06-15 14:14:12'),
(20, 'c9f0f895fb98ab9159f51fd0297e236d', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:78.0) ', 'Windows 7', 'Firefox - 78.0', 'Windows 7', '2020-07-22 15:00:45'),
(21, '8f14e45fceea167a5a36dedd4bea2543', 1, '2804:14c:5988:840c:83d:831c:d0b9:cced', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:840c:83d:831c:d0b9:cced', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 84.0.4147.89', 'Android', '2020-07-22 20:37:17'),
(22, '8e296a067a37563370ded05f5a3bf3ec', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:78.0) ', 'Windows 7', 'Firefox - 78.0', 'Windows 7', '2020-07-23 11:04:58'),
(23, '8f14e45fceea167a5a36dedd4bea2543', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 'Windows 7', 'Chrome - 84.0.4147.89', 'Windows 7', '2020-07-23 16:28:04'),
(24, '6512bd43d9caa6e02c990b0a82652dca', 1, '177.65.188.215', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.188.215', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) G', 'Linux', 'Firefox - 78.0', 'Linux', '2020-07-27 20:39:59'),
(25, 'd3d9446802a44259755d38e6d163e820', 1, '177.65.188.215', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.188.215', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) G', 'Linux', 'Firefox - 78.0', 'Linux', '2020-07-27 22:40:13'),
(26, 'e369853df766fa44e1ed0ff613f563bd', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:79.0) ', 'Windows 7', 'Firefox - 79.0', 'Windows 7', '2020-07-28 13:29:43'),
(27, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:79.0) ', 'Windows 7', 'Firefox - 79.0', 'Windows 7', '2020-07-29 15:04:28'),
(28, 'a5771bce93e200c36f7cd9dfd0e5deaa', 1, '177.65.188.215', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.188.215', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 84.0.4147.89', 'Linux', '2020-07-29 23:07:44'),
(29, 'e369853df766fa44e1ed0ff613f563bd', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:77.0) ', 'Windows 7', 'Firefox - 77.0', 'Windows 7', '2020-07-30 15:39:44'),
(30, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '2804:14c:5988:93e3:619f:ec95:f441:a474', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:619f:ec95:f441:a474', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 84.0.4147.89', 'Linux', '2020-08-03 19:35:19'),
(31, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 1, '2804:14c:5988:93e3:619f:ec95:f441:a474', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:619f:ec95:f441:a474', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 84.0.4147.89', 'Linux', '2020-08-03 21:32:17'),
(32, '8f14e45fceea167a5a36dedd4bea2543', 1, '2804:14c:5988:93e3:bc55:b045:a229:42d4', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:bc55:b045:a229:42d4', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) G', 'Linux', 'Firefox - 79.0', 'Linux', '2020-08-04 19:01:07'),
(33, 'd645920e395fedad7bbbed0eca3fe2e0', 1, '2804:14c:5988:93e3:fcc1:80c2:5208:9334', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:fcc1:80c2:5208:9334', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 84.0.4147.125', 'Linux', '2020-08-16 19:41:58'),
(34, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 1, '2804:14c:5988:93e3:3469:9487:4ea8:8c85', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:3469:9487:4ea8:8c85', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 84.0.4147.125', 'Linux', '2020-08-25 20:59:19'),
(35, 'c81e728d9d4c2f636f067f89cc14862c', 1, '2804:14c:5988:93e3:25dd:e5d5:5f90:9a46', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:25dd:e5d5:5f90:9a46', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 85.0.4183.83', 'Linux', '2020-08-26 21:45:00'),
(36, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '2804:14c:5988:93e3:a510:7f10:3ef5:86de', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:a510:7f10:3ef5:86de', 0, '0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 'Linux', 'Chrome - 85.0.4183.83', 'Linux', '2020-09-01 21:32:30'),
(37, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '2804:14c:5988:93e3:e4de:5cc6:5500:4a8d', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:e4de:5cc6:5500:4a8d', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-07 21:09:32'),
(38, '98f13708210194c475687be6106a3b84', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-08 13:45:50'),
(39, '6ea9ab1baa0efb9e19094440c317e21b', 1, '2804:14c:5988:93e3:c076:8174:5d3f:39b8', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c076:8174:5d3f:39b8', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-08 22:23:37'),
(40, 'aab3238922bcc25a6f606eb525ffdc56', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-09 13:32:14'),
(41, '2838023a778dfaecdc212708f721b788', 1, '2804:14c:5988:93e3:4d8a:eaa8:933e:99bb', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:4d8a:eaa8:933e:99bb', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-09 19:46:57'),
(42, '6512bd43d9caa6e02c990b0a82652dca', 1, '2804:4df4:fffe:4f30:c5e2:d971:2fd8:d2fd', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:2804:4df4:fffe:4f30:c5e2:d971:2fd8:d2fd', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 85.0.4183.83', 'Windows 7', '2020-09-10 04:29:26'),
(43, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-10 13:23:29'),
(44, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '45.235.222.199', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:45.235.222.199', 0, '1', 'Mozilla/5.0 (Android 9; Mobile; rv:80.0) Gecko/80.', 'Android', 'Firefox - 80.0', 'Android', '2020-09-12 12:23:17'),
(45, '1ff1de774005f8da13f42943881c655f', 1, '2804:14c:5988:93e3:bc1b:c2be:70af:d05e', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:bc1b:c2be:70af:d05e', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-13 15:28:28'),
(46, '182be0c5cdcd5072bb1864cdee4d3d6e', 1, '2804:14c:5988:93e3:bc1b:c2be:70af:d05e', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:bc1b:c2be:70af:d05e', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-13 23:35:42'),
(47, '8f14e45fceea167a5a36dedd4bea2543', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-14 11:20:07'),
(48, 'aab3238922bcc25a6f606eb525ffdc56', 1, '2804:14c:5988:93e3:6597:a102:f514:b1f3', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:6597:a102:f514:b1f3', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-14 20:44:29'),
(49, '1ff1de774005f8da13f42943881c655f', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-15 16:45:43'),
(50, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93e3:ec80:d149:a652:e571', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:ec80:d149:a652:e571', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-15 21:07:11'),
(51, 'cfcd208495d565ef66e7dff9f98764da', 1, '2804:14c:5988:93e3:783d:72ae:699:a085', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:783d:72ae:699:a085', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-16 20:43:00'),
(52, 'c9f0f895fb98ab9159f51fd0297e236d', 1, '2804:14c:5988:93e3:4d36:4ec6:a97b:7bde', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:4d36:4ec6:a97b:7bde', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-16 23:27:05'),
(53, '4e732ced3463d06de0ca9a15b6153677', 1, '2804:14c:5988:93e3:4d36:4ec6:a97b:7bde', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:4d36:4ec6:a97b:7bde', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-17 02:02:51'),
(54, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '187.24.237.203', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.237.203', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-17 09:27:13'),
(55, '6f4922f45568161a8cdf4ad2299f6d23', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-17 14:22:20'),
(56, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 1, '2804:14c:5988:93e3:142b:c478:b412:8f7c', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:142b:c478:b412:8f7c', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-17 20:54:01'),
(57, 'd645920e395fedad7bbbed0eca3fe2e0', 1, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 16:13:51'),
(58, '3c59dc048e8850243be8079a5c74d079', 1, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 17:04:26'),
(59, 'd3d9446802a44259755d38e6d163e820', 2, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 19:40:10'),
(60, '1679091c5a880faf6fb5e6087eb1b2dc', 2, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 21:19:06'),
(61, 'c20ad4d76fe97759aa27a0c99bff6710', 2, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 21:36:15'),
(62, '45c48cce2e2d7fbdea1afc51c7c6ad26', 3, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 22:00:10'),
(63, 'e4da3b7fbbce2345d7772b0674a318d5', 2, '2804:14c:5988:93e3:c18b:1fc9:6288:5099', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c18b:1fc9:6288:5099', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-19 23:08:02'),
(64, 'd67d8ab4f4c10bf22aa353e27879133c', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-19 23:30:49'),
(65, '3416a75f4cea9109507cacd8e2f2aefc', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-20 07:13:46'),
(66, 'e369853df766fa44e1ed0ff613f563bd', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-20 09:44:42'),
(67, 'c20ad4d76fe97759aa27a0c99bff6710', 2, '187.24.248.250', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.248.250', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-20 11:06:25'),
(68, '182be0c5cdcd5072bb1864cdee4d3d6e', 1, '187.24.248.250', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.248.250', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-20 11:21:49'),
(69, '8e296a067a37563370ded05f5a3bf3ec', 1, '2804:4df4:fffe:2fa0:d50e:5e7d:9d88:8ea4', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:2804:4df4:fffe:2fa0:d50e:5e7d:9d88:8ea4', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 85.0.4183.102', 'Windows 7', '2020-09-20 19:47:53'),
(70, '37693cfc748049e45d87b8c7d8b9aacd', 1, '2804:4df4:fffe:2fa0:d50e:5e7d:9d88:8ea4', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:2804:4df4:fffe:2fa0:d50e:5e7d:9d88:8ea4', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 85.0.4183.102', 'Windows 7', '2020-09-20 19:48:34'),
(71, '98f13708210194c475687be6106a3b84', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-21 11:01:37'),
(72, 'c81e728d9d4c2f636f067f89cc14862c', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-21 11:05:02'),
(73, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-21 11:07:20'),
(74, 'c74d97b01eae257e44aa9d5bade97baf', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-21 11:35:21'),
(75, 'c9f0f895fb98ab9159f51fd0297e236d', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-21 13:15:03'),
(76, '6512bd43d9caa6e02c990b0a82652dca', 2, '187.24.114.200', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.114.200', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-21 13:22:20'),
(77, 'aab3238922bcc25a6f606eb525ffdc56', 3, '187.24.114.200', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.114.200', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.101', 'Android', '2020-09-21 13:30:29'),
(78, 'd645920e395fedad7bbbed0eca3fe2e0', 1, '2804:14c:5988:93e3:edf3:fd37:f845:229a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:edf3:fd37:f845:229a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-21 20:18:50'),
(79, '8f14e45fceea167a5a36dedd4bea2543', 1, '2804:14c:5988:93e3:edf3:fd37:f845:229a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:edf3:fd37:f845:229a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-21 22:55:07'),
(80, 'c51ce410c124a10e0db5e4b97fc2af39', 1, '2804:14c:5988:93e3:cd98:1a04:6d60:812f', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:cd98:1a04:6d60:812f', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) G', 'Linux', 'Firefox - 80.0', 'Linux', '2020-09-22 21:00:48'),
(81, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:80.0) ', 'Windows 7', 'Firefox - 80.0', 'Windows 7', '2020-09-23 10:57:47'),
(82, 'aab3238922bcc25a6f606eb525ffdc56', 1, '2804:214:81b8:cee4:a5e1:a1f2:665d:3758', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:214:81b8:cee4:a5e1:a1f2:665d:3758', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 85.0.4183.121', 'Windows 7', '2020-09-23 19:39:35'),
(83, '4e732ced3463d06de0ca9a15b6153677', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-09-25 14:35:54'),
(84, 'f457c545a9ded88f18ecee47145a72c0', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-09-25 15:13:54'),
(85, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93e3:fdda:a5fe:4887:d7e3', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:fdda:a5fe:4887:d7e3', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-09-28 19:16:16'),
(86, '182be0c5cdcd5072bb1864cdee4d3d6e', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-02 13:34:58'),
(87, 'c9f0f895fb98ab9159f51fd0297e236d', 1, '2804:14c:5988:93e3:3c75:4184:c80b:9db4', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:3c75:4184:c80b:9db4', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-06 23:32:08'),
(88, '70efdf2ec9b086079795c442636b55fb', 1, '2804:14c:5988:93e3:3c75:4184:c80b:9db4', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:3c75:4184:c80b:9db4', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-06 23:44:22'),
(89, 'b6d767d2f8ed5d21a44b0e5886680cb9', 2, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-07 21:56:32'),
(90, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 00:24:01'),
(91, '6512bd43d9caa6e02c990b0a82652dca', 2, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 00:24:17'),
(92, '02e74f10e0327ad868d138f2b4fdd6f0', 2, '187.24.239.167', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.239.167', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-08 00:28:41'),
(93, '8e296a067a37563370ded05f5a3bf3ec', 1, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 00:37:34'),
(94, 'd3d9446802a44259755d38e6d163e820', 2, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 01:01:10'),
(95, '70efdf2ec9b086079795c442636b55fb', 2, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 01:01:36'),
(96, 'e369853df766fa44e1ed0ff613f563bd', 2, '2804:14c:5988:93e3:c523:92bb:270c:8701', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c523:92bb:270c:8701', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 01:08:35'),
(97, '02e74f10e0327ad868d138f2b4fdd6f0', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-08 14:47:46'),
(98, 'b6d767d2f8ed5d21a44b0e5886680cb9', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-08 16:37:53'),
(99, 'd645920e395fedad7bbbed0eca3fe2e0', 2, '2804:14c:5988:93e3:c067:aabe:7a2d:1fba', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:c067:aabe:7a2d:1fba', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-08 23:01:52'),
(100, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 2, '187.24.101.14', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.101.14', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-09 01:56:58'),
(101, 'c74d97b01eae257e44aa9d5bade97baf', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-09 09:22:39'),
(102, '6f4922f45568161a8cdf4ad2299f6d23', 2, '187.24.96.253', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.96.253', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-09 13:48:19'),
(103, 'c16a5320fa475530d9583c34fd356ef5', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-09 13:54:32'),
(104, '8f14e45fceea167a5a36dedd4bea2543', 2, '187.24.100.141', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.100.141', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-09 16:20:04'),
(105, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 2, '187.24.100.141', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.100.141', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-09 23:35:31'),
(106, 'd3d9446802a44259755d38e6d163e820', 2, '2804:14c:5988:93e3:84d7:d6a6:d2b6:a6b2', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:84d7:d6a6:d2b6:a6b2', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-10 23:36:10'),
(107, 'f457c545a9ded88f18ecee47145a72c0', 2, '187.24.252.56', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.252.56', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-11 00:07:57'),
(108, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 2, '2804:14c:5988:93e3:84d7:d6a6:d2b6:a6b2', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:84d7:d6a6:d2b6:a6b2', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-11 02:20:38'),
(109, '8e296a067a37563370ded05f5a3bf3ec', 2, '187.24.252.56', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.252.56', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-11 02:44:58'),
(110, 'e369853df766fa44e1ed0ff613f563bd', 2, '2804:14c:5988:93e3:191d:ef08:d46a:74ca', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:191d:ef08:d46a:74ca', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-11 16:43:48'),
(111, 'd3d9446802a44259755d38e6d163e820', 2, '187.24.252.56', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.252.56', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-11 16:45:09'),
(112, '02e74f10e0327ad868d138f2b4fdd6f0', 2, '2804:14c:5988:93e3:191d:ef08:d46a:74ca', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:191d:ef08:d46a:74ca', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-11 21:29:39'),
(113, '1c383cd30b7c298ab50293adfecb7b18', 2, '187.24.252.56', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.252.56', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-11 22:03:44'),
(114, 'c20ad4d76fe97759aa27a0c99bff6710', 2, '187.24.252.56', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.252.56', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-11 23:48:12'),
(115, '642e92efb79421734881b53e1e1b18b6', 2, '2804:14c:5988:93e3:191d:ef08:d46a:74ca', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:191d:ef08:d46a:74ca', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-12 00:19:58'),
(116, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '2804:14c:5988:93e3:191d:ef08:d46a:74ca', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:191d:ef08:d46a:74ca', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-12 00:21:09'),
(117, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 2, '2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-12 15:35:16'),
(118, '6ea9ab1baa0efb9e19094440c317e21b', 2, '2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-12 20:46:42'),
(119, 'a5bfc9e07964f8dddeb95fc584cd965d', 2, '2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-12 22:35:55'),
(120, 'd3d9446802a44259755d38e6d163e820', 4, '2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:a18c:a73a:c690:fe8a', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-13 00:39:32'),
(121, '1f0e3dad99908345f7439f8ffabdffc4', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-13 12:05:20'),
(122, 'c74d97b01eae257e44aa9d5bade97baf', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-13 14:57:56'),
(123, 'c20ad4d76fe97759aa27a0c99bff6710', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-13 14:59:16'),
(124, 'd3d9446802a44259755d38e6d163e820', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 86.0.4240.75', 'Windows 7', '2020-10-13 15:41:10'),
(125, '1ff1de774005f8da13f42943881c655f', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 86.0.4240.75', 'Windows 7', '2020-10-13 15:46:33'),
(126, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebK', 'Windows 7', 'Chrome - 86.0.4240.75', 'Windows 7', '2020-10-13 15:59:25'),
(127, 'c51ce410c124a10e0db5e4b97fc2af39', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-13 16:02:20'),
(128, '1679091c5a880faf6fb5e6087eb1b2dc', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-13 23:23:06'),
(129, 'c20ad4d76fe97759aa27a0c99bff6710', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-14 10:53:43'),
(130, '02e74f10e0327ad868d138f2b4fdd6f0', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 9; ASUS_X01BDA) AppleW', 'Android', 'Chrome - 86.0.4240.75', 'Android', '2020-10-14 10:55:34'),
(131, 'aab3238922bcc25a6f606eb525ffdc56', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-14 11:01:20'),
(132, 'c81e728d9d4c2f636f067f89cc14862c', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-14 11:51:00'),
(133, 'd3d9446802a44259755d38e6d163e820', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-14 11:51:10'),
(134, '19ca14e7ea6328a42e0eb13d585e4c22', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-14 14:35:57'),
(135, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-14 16:16:21'),
(136, 'c16a5320fa475530d9583c34fd356ef5', 4, '2804:14c:5988:93e3:9890:8db2:e3a3:76ec', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93e3:9890:8db2:e3a3:76ec', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-14 19:45:42'),
(137, '6ea9ab1baa0efb9e19094440c317e21b', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-14 22:56:59'),
(138, '45c48cce2e2d7fbdea1afc51c7c6ad26', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 01:08:07'),
(139, '98f13708210194c475687be6106a3b84', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 01:11:22'),
(140, 'd3d9446802a44259755d38e6d163e820', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 01:13:20'),
(141, '3c59dc048e8850243be8079a5c74d079', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-15 11:31:43'),
(142, 'd3d9446802a44259755d38e6d163e820', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 14:03:16'),
(143, 'c4ca4238a0b923820dcc509a6f75849b', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 14:06:01'),
(144, '70efdf2ec9b086079795c442636b55fb', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 14:09:20'),
(145, '45c48cce2e2d7fbdea1afc51c7c6ad26', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-15 14:44:02'),
(146, '6ea9ab1baa0efb9e19094440c317e21b', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 14:55:40'),
(147, '4e732ced3463d06de0ca9a15b6153677', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-15 17:43:58'),
(148, 'b6d767d2f8ed5d21a44b0e5886680cb9', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-15 21:31:24'),
(149, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-15 23:57:02'),
(150, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 85.0.4183.127', 'Android', '2020-10-16 00:13:17'),
(151, '642e92efb79421734881b53e1e1b18b6', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-16 00:27:51'),
(152, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-16 00:56:03'),
(153, '1ff1de774005f8da13f42943881c655f', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-16 01:13:30'),
(154, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-16 13:48:48'),
(155, '45c48cce2e2d7fbdea1afc51c7c6ad26', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-16 16:15:02');
INSERT INTO `autoria_acesso` (`id`, `session_id`, `id_usuario`, `ip`, `localizacao`, `is_robot`, `is_mobile`, `agent_string`, `plataforma`, `navegador`, `sistema`, `data_acesso`) VALUES
(156, '1ff1de774005f8da13f42943881c655f', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-16 16:15:28'),
(157, '3416a75f4cea9109507cacd8e2f2aefc', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-17 20:35:53'),
(158, 'b6d767d2f8ed5d21a44b0e5886680cb9', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-17 20:46:51'),
(159, '67c6a1e7ce56d3d6fa748ab6d9af3fd7', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-17 23:55:54'),
(160, 'a87ff679a2f3e71d9181a67b7542122c', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 00:27:04'),
(161, 'c16a5320fa475530d9583c34fd356ef5', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 00:34:44'),
(162, '6c8349cc7260ae62e3b1396831a8398f', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 00:39:56'),
(163, '3c59dc048e8850243be8079a5c74d079', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 00:47:41'),
(164, 'd3d9446802a44259755d38e6d163e820', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 01:17:52'),
(165, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 01:46:15'),
(166, 'd3d9446802a44259755d38e6d163e820', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 01:54:12'),
(167, '19ca14e7ea6328a42e0eb13d585e4c22', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 02:00:56'),
(168, '02e74f10e0327ad868d138f2b4fdd6f0', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 02:06:30'),
(169, 'c20ad4d76fe97759aa27a0c99bff6710', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 02:08:49'),
(170, '02e74f10e0327ad868d138f2b4fdd6f0', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 02:11:41'),
(171, '98f13708210194c475687be6106a3b84', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 02:12:51'),
(172, 'd3d9446802a44259755d38e6d163e820', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 02:13:10'),
(173, 'd3d9446802a44259755d38e6d163e820', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 02:13:47'),
(174, 'c74d97b01eae257e44aa9d5bade97baf', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 02:21:36'),
(175, '33e75ff09dd601bbe69f351039152189', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 02:26:30'),
(176, 'd645920e395fedad7bbbed0eca3fe2e0', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 20:52:57'),
(177, '6f4922f45568161a8cdf4ad2299f6d23', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 20:58:21'),
(178, '33e75ff09dd601bbe69f351039152189', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-18 21:40:53'),
(179, '1ff1de774005f8da13f42943881c655f', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 22:03:36'),
(180, '1679091c5a880faf6fb5e6087eb1b2dc', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-18 22:11:00'),
(181, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:81.0) G', 'Linux', 'Firefox - 81.0', 'Linux', '2020-10-19 17:16:51'),
(182, '19ca14e7ea6328a42e0eb13d585e4c22', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-20 15:17:42'),
(183, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-22 16:17:11'),
(184, '98f13708210194c475687be6106a3b84', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-22 16:49:28'),
(185, '182be0c5cdcd5072bb1864cdee4d3d6e', 2, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-22 19:18:38'),
(186, 'c16a5320fa475530d9583c34fd356ef5', 3, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.99', 'Android', '2020-10-22 19:19:31'),
(187, '37693cfc748049e45d87b8c7d8b9aacd', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-25 19:29:40'),
(188, '8f14e45fceea167a5a36dedd4bea2543', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-25 21:59:06'),
(189, '6364d3f0f495b6ab9dcf8d3b5c6e0b01', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-27 00:06:40'),
(190, 'd3d9446802a44259755d38e6d163e820', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-27 12:46:11'),
(191, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-27 20:29:23'),
(192, '33e75ff09dd601bbe69f351039152189', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-28 10:58:32'),
(193, 'e4da3b7fbbce2345d7772b0674a318d5', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-28 16:15:01'),
(194, '37693cfc748049e45d87b8c7d8b9aacd', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-28 22:07:28'),
(195, 'c20ad4d76fe97759aa27a0c99bff6710', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:81.0) ', 'Windows 7', 'Firefox - 81.0', 'Windows 7', '2020-10-29 10:24:50'),
(196, '1c383cd30b7c298ab50293adfecb7b18', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-10-30 22:10:45'),
(197, 'c51ce410c124a10e0db5e4b97fc2af39', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-02 16:43:37'),
(198, '1ff1de774005f8da13f42943881c655f', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-02 19:29:36'),
(199, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-02 21:42:00'),
(200, '98f13708210194c475687be6106a3b84', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.110', 'Android', '2020-11-02 22:51:28'),
(201, '1ff1de774005f8da13f42943881c655f', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.110', 'Android', '2020-11-02 23:14:30'),
(202, '1ff1de774005f8da13f42943881c655f', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.110', 'Android', '2020-11-03 04:31:46'),
(203, 'd645920e395fedad7bbbed0eca3fe2e0', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-03 22:05:59'),
(204, '1f0e3dad99908345f7439f8ffabdffc4', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.110', 'Android', '2020-11-04 15:36:27'),
(205, '17e62166fc8586dfa4d1bc0e1742c08b', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-04 23:40:49'),
(206, '4e732ced3463d06de0ca9a15b6153677', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-05 15:55:50'),
(207, '8e296a067a37563370ded05f5a3bf3ec', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-06 00:05:58'),
(208, '33e75ff09dd601bbe69f351039152189', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-06 10:01:50'),
(209, 'c16a5320fa475530d9583c34fd356ef5', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-06 12:28:43'),
(210, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-06 19:01:30'),
(211, 'c9f0f895fb98ab9159f51fd0297e236d', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-06 22:04:09'),
(212, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-08 17:08:18'),
(213, 'aab3238922bcc25a6f606eb525ffdc56', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-08 20:56:39'),
(214, 'c74d97b01eae257e44aa9d5bade97baf', 1, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-08 23:33:18'),
(215, '4e732ced3463d06de0ca9a15b6153677', 2, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 00:34:30'),
(216, '37693cfc748049e45d87b8c7d8b9aacd', 3, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 00:41:39'),
(217, 'c51ce410c124a10e0db5e4b97fc2af39', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 00:43:14'),
(218, '1679091c5a880faf6fb5e6087eb1b2dc', 1, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 01:01:06'),
(219, 'a1d0c6e83f027327d8461063f4ac58a6', 2, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 01:30:46'),
(220, 'c51ce410c124a10e0db5e4b97fc2af39', 3, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 01:50:30'),
(221, 'c16a5320fa475530d9583c34fd356ef5', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 01:53:40'),
(222, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 10:02:50'),
(223, '6512bd43d9caa6e02c990b0a82652dca', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 11:49:13'),
(224, '6f4922f45568161a8cdf4ad2299f6d23', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 12:02:23'),
(225, '98f13708210194c475687be6106a3b84', 3, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 12:04:58'),
(226, '4e732ced3463d06de0ca9a15b6153677', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 12:05:56'),
(227, '6f4922f45568161a8cdf4ad2299f6d23', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 16:01:20'),
(228, '98f13708210194c475687be6106a3b84', 2, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 16:04:28'),
(229, '3c59dc048e8850243be8079a5c74d079', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 16:09:24'),
(230, '19ca14e7ea6328a42e0eb13d585e4c22', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-09 16:14:37'),
(231, '37693cfc748049e45d87b8c7d8b9aacd', 3, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 9; ASUS_X01BDA) AppleW', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-09 16:14:42'),
(232, '37693cfc748049e45d87b8c7d8b9aacd', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-09 21:42:24'),
(233, 'c20ad4d76fe97759aa27a0c99bff6710', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-10 00:34:28'),
(234, 'a5771bce93e200c36f7cd9dfd0e5deaa', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-10 22:21:42'),
(235, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-10 22:22:13'),
(236, 'aab3238922bcc25a6f606eb525ffdc56', 2, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-10 23:04:46'),
(237, '6512bd43d9caa6e02c990b0a82652dca', 1, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-10 23:18:11'),
(238, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 2, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-10 23:20:01'),
(239, 'c4ca4238a0b923820dcc509a6f75849b', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-11 07:06:01'),
(240, 'c51ce410c124a10e0db5e4b97fc2af39', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-11 23:27:18'),
(241, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-12 01:07:33'),
(242, '98f13708210194c475687be6106a3b84', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-12 09:48:29'),
(243, '8f14e45fceea167a5a36dedd4bea2543', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-12 11:35:03'),
(244, '1f0e3dad99908345f7439f8ffabdffc4', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-12 11:37:19'),
(245, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-12 22:06:13'),
(246, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '177.65.177.181', 'Cidade: Ananindeua/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.3656,-48.3722/ CEP: 67000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-12 22:45:58'),
(247, '1679091c5a880faf6fb5e6087eb1b2dc', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.185', 'Android', '2020-11-13 02:36:02'),
(248, 'c9f0f895fb98ab9159f51fd0297e236d', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-13 21:14:08'),
(249, 'd9d4f495e875a2e075a1a4a6e1b9770f', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-15 20:49:52'),
(250, '70efdf2ec9b086079795c442636b55fb', 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-16 00:27:31'),
(251, 'f7177163c833dff4b38fc8d2872f1ec6', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-16 00:27:49'),
(252, '1c383cd30b7c298ab50293adfecb7b18', 1, '187.24.243.1', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:187.24.243.1', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.198', 'Android', '2020-11-16 13:26:37'),
(253, 'd3d9446802a44259755d38e6d163e820', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-16 14:04:00'),
(254, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-16 22:04:08'),
(255, 'c16a5320fa475530d9583c34fd356ef5', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.198', 'Android', '2020-11-17 00:08:56'),
(256, '6512bd43d9caa6e02c990b0a82652dca', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-17 21:59:05'),
(257, '45c48cce2e2d7fbdea1afc51c7c6ad26', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 86.0.4240.198', 'Android', '2020-11-17 23:00:08'),
(258, 'c20ad4d76fe97759aa27a0c99bff6710', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) G', 'Linux', 'Firefox - 82.0', 'Linux', '2020-11-18 02:05:13'),
(259, '34173cb38f07f89ddbebc2ac9128303f', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-18 12:32:37'),
(260, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-18 21:10:18'),
(261, 'c20ad4d76fe97759aa27a0c99bff6710', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-19 00:19:23'),
(262, '1c383cd30b7c298ab50293adfecb7b18', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-19 00:30:47'),
(263, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-19 11:01:21'),
(264, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 1, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-19 12:41:50'),
(265, '6ea9ab1baa0efb9e19094440c317e21b', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-19 12:43:45'),
(266, '02e74f10e0327ad868d138f2b4fdd6f0', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-19 20:18:31'),
(267, 'c51ce410c124a10e0db5e4b97fc2af39', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-20 00:09:13'),
(268, 'c74d97b01eae257e44aa9d5bade97baf', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-20 23:21:28'),
(269, '70efdf2ec9b086079795c442636b55fb', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-20 23:44:18'),
(270, '45c48cce2e2d7fbdea1afc51c7c6ad26', 4, '2804:214:81bb:2624:b3ca:4c4:66fb:8a64', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:214:81bb:2624:b3ca:4c4:66fb:8a64', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-21 09:44:04'),
(271, 'f457c545a9ded88f18ecee47145a72c0', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-21 23:21:49'),
(272, '8e296a067a37563370ded05f5a3bf3ec', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 01:21:36'),
(273, '6512bd43d9caa6e02c990b0a82652dca', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 01:22:07'),
(274, 'aab3238922bcc25a6f606eb525ffdc56', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 01:22:36'),
(275, '17e62166fc8586dfa4d1bc0e1742c08b', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 01:22:57'),
(276, 'c81e728d9d4c2f636f067f89cc14862c', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 17:06:00'),
(277, '3c59dc048e8850243be8079a5c74d079', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 18:53:31'),
(278, '1c383cd30b7c298ab50293adfecb7b18', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 19:56:56'),
(279, '1679091c5a880faf6fb5e6087eb1b2dc', 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 21:30:04'),
(280, '6f4922f45568161a8cdf4ad2299f6d23', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 21:30:18'),
(281, '8e296a067a37563370ded05f5a3bf3ec', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 21:33:45'),
(282, '37693cfc748049e45d87b8c7d8b9aacd', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 21:35:51'),
(283, 'aab3238922bcc25a6f606eb525ffdc56', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 22:35:14'),
(284, '45c48cce2e2d7fbdea1afc51c7c6ad26', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 22:46:10'),
(285, 'd3d9446802a44259755d38e6d163e820', 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 22:49:04'),
(286, 'c51ce410c124a10e0db5e4b97fc2af39', 17, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 22:52:28'),
(287, '6512bd43d9caa6e02c990b0a82652dca', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 23:22:05'),
(288, 'c74d97b01eae257e44aa9d5bade97baf', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 23:23:16'),
(289, '02e74f10e0327ad868d138f2b4fdd6f0', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-22 23:27:27'),
(290, '8e296a067a37563370ded05f5a3bf3ec', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 23:29:40'),
(291, '02e74f10e0327ad868d138f2b4fdd6f0', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 23:32:28'),
(292, '02e74f10e0327ad868d138f2b4fdd6f0', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-22 23:33:30'),
(293, '37693cfc748049e45d87b8c7d8b9aacd', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-23 00:29:24'),
(294, 'c0c7c76d30bd3dcaefc96f40275bdc0a', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-23 00:34:56'),
(295, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '0', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:82.0) ', 'Windows 7', 'Firefox - 82.0', 'Windows 7', '2020-11-23 10:41:41'),
(296, '6512bd43d9caa6e02c990b0a82652dca', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-23 18:50:09'),
(297, '642e92efb79421734881b53e1e1b18b6', 4, '177.74.61.129', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129', 0, '1', 'Mozilla/5.0 (Linux; Android 10; moto g(7) play) Ap', 'Android', 'Chrome - 87.0.4280.66', 'Android', '2020-11-24 16:38:55'),
(298, 'c9f0f895fb98ab9159f51fd0297e236d', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:44:06'),
(299, '3c59dc048e8850243be8079a5c74d079', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:44:27'),
(300, '02e74f10e0327ad868d138f2b4fdd6f0', 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:44:44'),
(301, '1f0e3dad99908345f7439f8ffabdffc4', 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:30:40'),
(302, '02e74f10e0327ad868d138f2b4fdd6f0', 17, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:36:39'),
(303, 'c74d97b01eae257e44aa9d5bade97baf', 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:55:18'),
(304, '4e732ced3463d06de0ca9a15b6153677', 17, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:55:32'),
(305, '6512bd43d9caa6e02c990b0a82652dca', 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:55:48'),
(306, 'cfcd208495d565ef66e7dff9f98764da', 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:00'),
(307, 'd67d8ab4f4c10bf22aa353e27879133c', 4, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, '0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:06:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

CREATE TABLE `financeiro` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `id_situacao` int(11) NOT NULL,
  `valor` varchar(100) DEFAULT NULL,
  `data` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_pedido`
--

CREATE TABLE `item_pedido` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `reparte` varchar(50) DEFAULT NULL,
  `encalhe` varchar(50) DEFAULT NULL,
  `edicao` varchar(50) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `peso` double NOT NULL,
  `data_reparte` datetime DEFAULT NULL,
  `data_encalhe` datetime DEFAULT NULL,
  `situacao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `item_pedido`
--

INSERT INTO `item_pedido` (`id`, `id_pedido`, `id_produto`, `reparte`, `encalhe`, `edicao`, `valor`, `peso`, `data_reparte`, `data_encalhe`, `situacao`) VALUES
(1, 1, 1, '20', '2', '13253', 1.25, 10, '2020-11-23 00:00:00', '2020-11-24 22:34:38', 2),
(2, 1, 1, '30', '2', '13254', 1.25, 5, '2020-11-24 00:00:00', '2020-11-24 22:34:40', 2),
(3, 1, 1, '20', '2', '13255', 1.25, 10, '2020-11-25 00:00:00', '2020-11-24 22:34:43', 2),
(4, 1, 1, '30', '2', '13256', 1.25, 5, '2020-11-26 00:00:00', '2020-11-24 22:34:45', 2),
(5, 1, 1, '20', '2', '13257', 1.25, 10, '2020-11-27 00:00:00', '2020-11-24 22:34:59', 2),
(6, 1, 1, '30', '2', '13258', 1.25, 5, '2020-11-28 00:00:00', '2020-11-24 22:35:03', 2),
(7, 1, 1, '20', '2', '13259', 3, 10, '2020-11-29 00:00:00', '2020-11-24 22:35:09', 2),
(8, 2, 1, '10', '1', '13253', 1.25, 5, '2020-11-23 00:00:00', '2020-11-24 22:57:33', 2),
(9, 2, 1, '20', '2', '13254', 1.25, 10, '2020-11-24 00:00:00', '2020-11-24 22:57:34', 2),
(10, 2, 1, '10', '1', '13255', 1.25, 5, '2020-11-25 00:00:00', '2020-11-24 22:57:36', 2),
(11, 2, 1, '20', '2', '13256', 1.25, 10, '2020-11-26 00:00:00', '2020-11-24 22:57:38', 2),
(12, 2, 1, '10', '1', '13257', 1.25, 5, '2020-11-27 00:00:00', '2020-11-24 22:57:39', 2),
(13, 2, 1, '20', '2', '13258', 1.25, 10, '2020-11-28 00:00:00', '2020-11-24 22:57:40', 2),
(14, 2, 1, '10', '1', '13259', 3, 5, '2020-11-29 00:00:00', '2020-11-24 22:57:42', 2),
(15, 3, 1, '20', '4', '13253', 1.25, 10, '2020-11-23 00:00:00', '2020-11-24 22:54:44', 2),
(16, 3, 1, '30', '5', '13254', 1.25, 20, '2020-11-24 00:00:00', '2020-11-24 22:54:45', 2),
(17, 3, 1, '20', '4', '13255', 1.25, 10, '2020-11-25 00:00:00', '2020-11-24 22:54:46', 2),
(18, 3, 1, '30', '5', '13256', 1.25, 20, '2020-11-26 00:00:00', '2020-11-24 22:54:48', 2),
(19, 3, 1, '20', '4', '13257', 1.25, 10, '2020-11-27 00:00:00', '2020-11-24 22:54:50', 2),
(20, 3, 1, '30', '5', '13258', 1.25, 20, '2020-11-28 00:00:00', '2020-11-24 22:54:51', 2),
(21, 3, 1, '20', '4', '13259', 3, 10, '2020-11-29 00:00:00', '2020-11-24 22:54:53', 2),
(22, 4, 1, '10', '1', '13253', 1.25, 5, '2020-11-23 00:00:00', '2020-11-24 23:03:26', 2),
(23, 4, 1, '20', '2', '13254', 1.25, 10, '2020-11-24 00:00:00', '2020-11-24 23:03:29', 2),
(24, 4, 1, '10', '1', '13255', 1.25, 5, '2020-11-25 00:00:00', '2020-11-24 23:03:32', 2),
(25, 4, 1, '20', '2', '13256', 1.25, 10, '2020-11-26 00:00:00', '2020-11-24 23:03:36', 2),
(26, 4, 1, '10', '1', '13257', 1.25, 5, '2020-11-27 00:00:00', '2020-11-24 23:03:39', 2),
(27, 4, 1, '20', '2', '13258', 1.25, 10, '2020-11-28 00:00:00', '2020-11-24 23:03:45', 2),
(28, 4, 1, '10', '1', '13259', 3, 5, '2020-11-29 00:00:00', '2020-11-24 23:03:56', 2),
(29, 5, 1, '20', '2', '13253', 1.25, 10, '2020-11-23 00:00:00', '2020-11-24 22:57:10', 2),
(30, 5, 1, '30', '3', '13254', 1.25, 20, '2020-11-24 00:00:00', '2020-11-24 22:57:11', 2),
(31, 5, 1, '20', '2', '13255', 1.25, 10, '2020-11-25 00:00:00', '2020-11-24 22:57:11', 2),
(32, 5, 1, '30', '3', '13256', 1.25, 20, '2020-11-26 00:00:00', '2020-11-24 22:57:13', 2),
(33, 5, 1, '20', '2', '13257', 1.25, 10, '2020-11-27 00:00:00', '2020-11-24 22:57:14', 2),
(34, 5, 1, '30', '3', '13258', 1.25, 20, '2020-11-28 00:00:00', '2020-11-24 22:57:17', 2),
(35, 5, 1, '20', '2', '13259', 3, 10, '2020-11-29 00:00:00', '2020-11-24 22:57:20', 2),
(36, 6, 1, '10', '1', '13253', 1.25, 5, '2020-11-23 00:00:00', '2020-11-24 23:02:14', 2),
(37, 6, 1, '20', '2', '13254', 1.25, 10, '2020-11-24 00:00:00', '2020-11-24 23:02:16', 2),
(38, 6, 1, '10', '1', '13255', 1.25, 5, '2020-11-25 00:00:00', '2020-11-24 23:02:18', 2),
(39, 6, 1, '20', '2', '13256', 1.25, 10, '2020-11-26 00:00:00', '2020-11-24 23:02:21', 2),
(40, 6, 1, '10', '1', '13257', 1.25, 5, '2020-11-27 00:00:00', '2020-11-24 23:02:29', 2),
(41, 6, 1, '20', '2', '13258', 1.25, 10, '2020-11-28 00:00:00', '2020-11-24 23:02:32', 2),
(42, 6, 1, '10', '1', '13259', 3, 5, '2020-11-29 00:00:00', '2020-11-24 23:02:36', 2),
(43, 7, 1, '20', '2', '13253', 1.25, 10, '2020-11-23 00:00:00', '2020-11-24 22:30:53', 2),
(44, 7, 1, '30', '2', '13254', 1.25, 20, '2020-11-24 00:00:00', '2020-11-24 22:30:57', 2),
(45, 7, 1, '20', '2', '13255', 1.25, 10, '2020-11-25 00:00:00', '2020-11-24 22:32:03', 2),
(46, 7, 1, '30', '2', '13256', 1.25, 20, '2020-11-26 00:00:00', '2020-11-24 22:32:07', 2),
(47, 7, 1, '20', '2', '13257', 1.25, 10, '2020-11-27 00:00:00', '2020-11-24 22:32:15', 2),
(48, 7, 1, '30', '2', '13258', 1.25, 20, '2020-11-28 00:00:00', '2020-11-24 22:32:27', 2),
(49, 7, 1, '20', '2', '13259', 3, 10, '2020-11-29 00:00:00', '2020-11-24 22:32:36', 2),
(50, 8, 1, '10', '1', '13253', 1.25, 5, '2020-11-23 00:00:00', '2020-11-24 22:56:37', 2),
(51, 8, 1, '20', '2', '13254', 1.25, 10, '2020-11-24 00:00:00', '2020-11-24 22:56:37', 2),
(52, 8, 1, '10', '1', '13255', 1.25, 5, '2020-11-25 00:00:00', '2020-11-24 22:56:41', 2),
(53, 8, 1, '20', '2', '13256', 1.25, 10, '2020-11-26 00:00:00', '2020-11-24 22:56:42', 2),
(54, 8, 1, '10', '1', '13257', 1.25, 5, '2020-11-27 00:00:00', '2020-11-24 22:56:44', 2),
(55, 8, 1, '20', '2', '13258', 1.25, 10, '2020-11-28 00:00:00', '2020-11-24 22:56:46', 2),
(56, 8, 1, '10', '1', '13259', 3, 5, '2020-11-29 00:00:00', '2020-11-24 22:56:50', 2),
(57, 9, 1, '20', '3', '13253', 1.25, 10, '2020-11-23 00:00:00', '2020-11-24 22:49:31', 2),
(58, 9, 1, '30', '4', '13254', 1.25, 20, '2020-11-24 00:00:00', '2020-11-24 22:49:32', 2),
(59, 9, 1, '20', '3', '13255', 1.25, 10, '2020-11-25 00:00:00', '2020-11-24 22:49:34', 2),
(60, 9, 1, '30', '4', '13256', 1.25, 20, '2020-11-26 00:00:00', '2020-11-24 22:49:34', 2),
(61, 9, 1, '20', '3', '13257', 1.25, 10, '2020-11-27 00:00:00', '2020-11-24 22:49:45', 2),
(62, 9, 1, '30', '4', '13258', 1.25, 20, '2020-11-28 00:00:00', '2020-11-24 22:49:46', 2),
(63, 9, 1, '20', '3', '13259', 3, 10, '2020-11-29 00:00:00', '2020-11-24 22:49:46', 2),
(64, 10, 1, '10', '1', '13253', 1.25, 5, '2020-11-23 00:00:00', '2020-11-24 22:56:06', 2),
(65, 10, 1, '20', '2', '13254', 1.25, 10, '2020-11-24 00:00:00', '2020-11-24 22:56:06', 2),
(66, 10, 1, '10', '1', '13255', 1.25, 5, '2020-11-25 00:00:00', '2020-11-24 22:56:08', 2),
(67, 10, 1, '20', '2', '13256', 1.25, 10, '2020-11-26 00:00:00', '2020-11-24 22:56:09', 2),
(68, 10, 1, '10', '1', '13257', 1.25, 5, '2020-11-27 00:00:00', '2020-11-24 22:56:11', 2),
(69, 10, 1, '20', '2', '13258', 1.25, 10, '2020-11-28 00:00:00', '2020-11-24 22:56:11', 2),
(70, 10, 1, '10', '1', '13259', 3, 5, '2020-11-29 00:00:00', '2020-11-24 22:56:13', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_produto`
--

CREATE TABLE `item_produto` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `item_produto`
--

INSERT INTO `item_produto` (`id`, `id_produto`, `nome`, `valor`) VALUES
(1, 1, 'Domingo', 3),
(2, 1, 'segunda-feira', 1.25),
(3, 1, 'terca-feita', 1.25),
(4, 1, 'quarta-feita', 1.25),
(5, 1, 'quinta-feita', 1.25),
(6, 1, 'sexta-feira', 1.25),
(7, 1, 'Sábado', 1.25);

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_item_pedido`
--

CREATE TABLE `log_item_pedido` (
  `id` int(11) NOT NULL,
  `id_item_pedido` int(11) NOT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_situacao` int(11) NOT NULL,
  `reparte` varchar(50) DEFAULT NULL,
  `encalhe` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `localizacao` varchar(200) DEFAULT NULL,
  `is_robot` tinyint(1) DEFAULT NULL,
  `is_mobile` tinyint(1) DEFAULT NULL,
  `agente_string` varchar(50) DEFAULT NULL,
  `plataforma` varchar(50) DEFAULT NULL,
  `navegador` varchar(50) DEFAULT NULL,
  `sistema` varchar(50) DEFAULT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `log_item_pedido`
--

INSERT INTO `log_item_pedido` (`id`, `id_item_pedido`, `id_produto`, `id_usuario`, `id_situacao`, `reparte`, `encalhe`, `ip`, `localizacao`, `is_robot`, `is_mobile`, `agente_string`, `plataforma`, `navegador`, `sistema`, `data`) VALUES
(1, 1, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:40'),
(2, 2, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:41'),
(3, 3, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:41'),
(4, 4, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:41'),
(5, 5, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:41'),
(6, 6, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:41'),
(7, 7, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:42'),
(8, 8, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:12'),
(9, 9, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:12'),
(10, 10, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:12'),
(11, 11, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:13'),
(12, 12, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:13'),
(13, 13, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:13'),
(14, 14, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:13'),
(15, 15, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:06'),
(16, 16, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:07'),
(17, 17, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:07'),
(18, 18, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:07'),
(19, 19, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:07'),
(20, 20, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:07'),
(21, 21, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:08'),
(22, 22, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:48'),
(23, 23, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:48'),
(24, 24, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:48'),
(25, 25, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:49'),
(26, 26, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:49'),
(27, 27, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:49'),
(28, 28, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:49'),
(29, 29, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:33'),
(30, 30, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:33'),
(31, 31, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:33'),
(32, 32, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:34'),
(33, 33, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:34'),
(34, 34, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:34'),
(35, 35, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:34'),
(36, 36, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:45'),
(37, 37, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:45'),
(38, 38, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:46'),
(39, 39, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:46'),
(40, 40, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:46'),
(41, 41, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:46'),
(42, 42, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:46'),
(43, 43, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:01'),
(44, 44, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:01'),
(45, 45, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:02'),
(46, 46, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:02'),
(47, 47, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:02'),
(48, 48, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:02'),
(49, 49, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:02'),
(50, 50, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:34'),
(51, 51, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:34'),
(52, 52, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:35'),
(53, 53, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:35'),
(54, 54, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:35'),
(55, 55, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:35'),
(56, 56, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:35'),
(57, 57, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:45'),
(58, 58, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:45'),
(59, 59, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:45'),
(60, 60, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:45'),
(61, 61, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:46'),
(62, 62, NULL, 1, 1, '30', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:46'),
(63, 63, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:46'),
(64, 64, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:47'),
(65, 65, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:48'),
(66, 66, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:48'),
(67, 67, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:48'),
(68, 68, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:48'),
(69, 69, NULL, 1, 1, '20', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:48'),
(70, 70, NULL, 1, 1, '10', NULL, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:49'),
(71, 43, NULL, 16, 2, '20', '18', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:30:53'),
(72, 44, NULL, 16, 2, '30', '28', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:30:57'),
(73, 45, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:04'),
(74, 46, NULL, 16, 2, '30', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:07'),
(75, 47, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:16'),
(76, 48, NULL, 16, 2, '30', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:27'),
(77, 49, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:36'),
(78, 1, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:34:39'),
(79, 2, NULL, 16, 2, '30', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:34:41'),
(80, 3, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:34:43'),
(81, 4, NULL, 16, 2, '30', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:34:51'),
(82, 5, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:35:00'),
(83, 6, NULL, 16, 2, '30', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:35:03'),
(84, 7, NULL, 16, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:35:15'),
(85, 57, NULL, 17, 2, '20', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:32'),
(86, 58, NULL, 17, 2, '30', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:32'),
(87, 59, NULL, 17, 2, '20', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:34'),
(88, 60, NULL, 17, 2, '30', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:34'),
(89, 61, NULL, 17, 2, '20', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:46'),
(90, 62, NULL, 17, 2, '30', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:46'),
(91, 63, NULL, 17, 2, '20', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:47'),
(92, 15, NULL, 17, 2, '20', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:44'),
(93, 16, NULL, 17, 2, '30', '5', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:46'),
(94, 17, NULL, 17, 2, '20', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:47'),
(95, 18, NULL, 17, 2, '30', '5', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:49'),
(96, 19, NULL, 17, 2, '20', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:50'),
(97, 20, NULL, 17, 2, '30', '5', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:51'),
(98, 21, NULL, 17, 2, '20', '4', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:54'),
(99, 64, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:06'),
(100, 65, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:07'),
(101, 66, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:08'),
(102, 67, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:09'),
(103, 68, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:11'),
(104, 69, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:12'),
(105, 70, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:13'),
(106, 50, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:37'),
(107, 51, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:38'),
(108, 52, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:41'),
(109, 53, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:43'),
(110, 54, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:45'),
(111, 55, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:46'),
(112, 56, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:50'),
(113, 29, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:10'),
(114, 30, NULL, 2, 2, '30', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:11'),
(115, 31, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:12'),
(116, 32, NULL, 2, 2, '30', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:13'),
(117, 33, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:14'),
(118, 34, NULL, 2, 2, '30', '3', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:17'),
(119, 35, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:20'),
(120, 8, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:34'),
(121, 9, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:34'),
(122, 10, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:37'),
(123, 11, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:38'),
(124, 12, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:40'),
(125, 13, NULL, 2, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:41'),
(126, 14, NULL, 2, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:42'),
(127, 36, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:14'),
(128, 37, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:16'),
(129, 38, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:19'),
(130, 39, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:22'),
(131, 40, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:29'),
(132, 41, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:32'),
(133, 42, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:37'),
(134, 22, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:27'),
(135, 23, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:29'),
(136, 24, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:33'),
(137, 25, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:37'),
(138, 26, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:40'),
(139, 27, NULL, 3, 2, '20', '2', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:45'),
(140, 28, NULL, 3, 2, '10', '1', '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_pedido`
--

CREATE TABLE `log_pedido` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_situacao` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `localizacao` varchar(200) DEFAULT NULL,
  `is_robot` tinyint(1) DEFAULT NULL,
  `is_mobile` tinyint(1) DEFAULT NULL,
  `agente_string` varchar(50) DEFAULT NULL,
  `plataforma` varchar(50) DEFAULT NULL,
  `navegador` varchar(50) DEFAULT NULL,
  `sistema` varchar(50) DEFAULT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `log_pedido`
--

INSERT INTO `log_pedido` (`id`, `id_pedido`, `id_situacao`, `id_usuario`, `ip`, `localizacao`, `is_robot`, `is_mobile`, `agente_string`, `plataforma`, `navegador`, `sistema`, `data`) VALUES
(1, 1, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:52:40'),
(2, 2, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 21:56:12'),
(3, 3, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:03:06'),
(4, 4, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:05:48'),
(5, 5, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:11:33'),
(6, 6, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:23:45'),
(7, 7, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:26:01'),
(8, 8, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:27:34'),
(9, 9, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:28:45'),
(10, 10, 1, 1, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:29:47'),
(11, 7, 2, 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:32:36'),
(12, 1, 2, 16, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:35:10'),
(13, 9, 2, 17, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:49:47'),
(14, 3, 2, 17, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:54:54'),
(15, 10, 2, 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:13'),
(16, 8, 2, 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:56:50'),
(17, 5, 2, 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:20'),
(18, 2, 2, 2, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 22:57:42'),
(19, 6, 2, 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:02:36'),
(20, 4, 2, 3, '177.65.177.181', 'Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.65.177.181', 0, 0, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) G', 'Linux', 'Firefox - 83.0', 'Linux', '2020-11-24 23:03:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_rota_senamal`
--

CREATE TABLE `log_rota_senamal` (
  `id` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL,
  `id_situacao` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `dia_avaliado` datetime NOT NULL,
  `reparte` varchar(50) DEFAULT NULL,
  `encalhe` varchar(50) DEFAULT NULL,
  `quantidade_fisica` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `localizacao` varchar(200) DEFAULT NULL,
  `is_robot` tinyint(1) DEFAULT NULL,
  `is_mobile` tinyint(1) DEFAULT NULL,
  `agente_string` varchar(50) DEFAULT NULL,
  `plataforma` varchar(50) DEFAULT NULL,
  `navegador` varchar(50) DEFAULT NULL,
  `sistema` varchar(50) DEFAULT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pdv`
--

CREATE TABLE `pdv` (
  `id` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL,
  `nome_pdv` varchar(250) DEFAULT NULL,
  `funcionario` varchar(150) DEFAULT NULL,
  `ddd` int(4) DEFAULT NULL,
  `telefone` bigint(14) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `endereco` varchar(250) DEFAULT NULL,
  `numero` varchar(100) NOT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `comissao` double DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_edicao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pdv`
--

INSERT INTO `pdv` (`id`, `id_rota`, `nome_pdv`, `funcionario`, `ddd`, `telefone`, `cep`, `endereco`, `numero`, `complemento`, `comissao`, `data_cadastro`, `data_edicao`) VALUES
(1, 1, 'Feira da Terra Firme', 'Sr. Ronaldo', 91, 988917980, '66077000', 'Avenida Celso Malcher', '712', '', 70, '2020-05-30 00:40:51', '2020-09-09 16:16:22'),
(2, 1, 'Terra Firme', 'Ronaldo', 91, 988917980, '66077000', 'Avenida Celso Malcher', '712', NULL, 70, '2020-05-30 00:42:52', NULL),
(3, 2, 'Mateus Supermercados Marambaia', 'Luiz', 91, 988617980, '66615005', 'Avenida Tavares Bastos', '1234', NULL, 70, '2020-06-13 16:59:36', '2020-06-13 18:32:11'),
(4, 2, 'IT CENTER', 'JR', 91, 999999999, '66120000', 'Avenida Senador Lemos', '3153', NULL, 70, '2020-07-28 13:59:56', NULL),
(5, 3, 'Panificadora 24h', 'Rivelino Cunha', 91, 988617980, '67113693', 'Travessa São Pedro', '01', 'Proximo ao colégio Logos', 70, '2020-07-30 15:41:43', '2020-09-09 21:00:55'),
(6, 4, 'Banpará Doca', 'Sr. Banpará', 91, 2147483647, '66050000', 'Avenida Senador Lemos', '321', 'Próximo a Doca de Souza Fanco', 70, '2020-08-03 22:13:04', '2020-08-03 22:24:35'),
(7, 5, 'Terminal Hidroviario de Icoaraci\r\n', 'Sr. Valdeci', 91, 909090909, ' 66812460 ', 'R. Siqueira Mendes', '1123', 'Terminal Hidroviario de Icoaraci\r\n', NULL, '2020-10-18 00:14:34', NULL),
(8, 3, 'Panificadora(2) 24h', 'Rivelino Cunha', 91, 988617980, '67113693', 'Travessa São Pedro', '01', 'Proximo ao colégio Logos', 70, '2020-07-30 15:41:43', '2020-09-09 21:00:55'),
(9, 6, 'Banpará Doca(2)', 'Sr. Banpará', 91, 2147483647, '66050000', 'Avenida Senador Lemos', '321', 'Próximo a Doca de Souza Fanco', 70, '2020-08-03 22:13:04', '2020-08-03 22:24:35'),
(10, 7, 'Terminal Hidroviario de Icoaraci(2)\r\n', 'Sr. Valdeci', 91, 909090909, ' 66812460 ', 'R. Siqueira Mendes', '1123', 'Terminal Hidroviario de Icoaraci\r\n', NULL, '2020-10-18 00:14:34', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `id_pdv` int(11) NOT NULL,
  `id_usuario_reparte` int(11) DEFAULT NULL,
  `id_usuario_encalhe` int(11) DEFAULT NULL,
  `total_peso` double DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `situacao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`id`, `id_pdv`, `id_usuario_reparte`, `id_usuario_encalhe`, `total_peso`, `data_cadastro`, `situacao`) VALUES
(1, 1, 16, 16, 55, '2020-11-24 21:52:40', 2),
(2, 2, 2, 2, 50, '2020-11-24 21:56:12', 2),
(3, 3, 17, 17, 100, '2020-11-24 22:03:06', 2),
(4, 4, 3, 3, 50, '2020-11-24 22:05:48', 2),
(5, 5, 2, 2, 100, '2020-11-24 22:11:33', 2),
(6, 6, 3, 3, 50, '2020-11-24 22:23:45', 2),
(7, 7, 16, 16, 100, '2020-11-24 22:26:01', 2),
(8, 8, 2, 2, 50, '2020-11-24 22:27:34', 2),
(9, 9, 17, 17, 100, '2020-11-24 22:28:44', 2),
(10, 10, 2, 2, 50, '2020-11-24 22:29:47', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `percentual_semanal_pdv`
--

CREATE TABLE `percentual_semanal_pdv` (
  `id` int(11) NOT NULL,
  `id_pdv` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `percentual` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `percentual_semanal_pdv`
--

INSERT INTO `percentual_semanal_pdv` (`id`, `id_pdv`, `nome`, `percentual`) VALUES
(1, 5, 'segunda-feira', 30),
(2, 5, 'terca-feira', 30),
(3, 5, 'quarta-feira', 30),
(4, 5, 'quinta-feira', 30),
(5, 5, 'sexta-feira', 30),
(6, 5, 'sabado', 30),
(7, 5, 'domingo', 20),
(8, 1, 'segunda-feira', 30),
(9, 1, 'terca-feira', 30),
(10, 1, 'quarta-feira', 30),
(11, 1, 'quinta-feira', 30),
(12, 1, 'sexta-feira', 30),
(13, 1, 'sabado', 30),
(14, 1, 'domingo', 20),
(15, 2, 'segunda-feira', 30),
(16, 2, 'terca-feira', 30),
(17, 2, 'quarta-feira', 30),
(18, 2, 'quinta-feira', 30),
(19, 2, 'sexta-feira', 30),
(20, 2, 'sabado', 30),
(21, 2, 'domingo', 20),
(22, 3, 'segunda-feira', 30),
(23, 3, 'terca-feira', 30),
(24, 3, 'quarta-feira', 30),
(25, 3, 'quinta-feira', 30),
(26, 3, 'sexta-feira', 30),
(27, 3, 'sabado', 30),
(28, 3, 'domingo', 20),
(29, 4, 'segunda-feira', 30),
(30, 4, 'terca-feira', 30),
(31, 4, 'quarta-feira', 30),
(32, 4, 'quinta-feira', 30),
(33, 4, 'sexta-feira', 30),
(34, 4, 'sabado', 30),
(35, 4, 'domingo', 20),
(36, 6, 'segunda-feira', 30),
(37, 6, 'terca-feira', 30),
(38, 6, 'quarta-feira', 30),
(39, 6, 'quinta-feira', 30),
(40, 6, 'sexta-feira', 30),
(41, 6, 'sabado', 30),
(42, 6, 'domingo', 20),
(43, 7, 'segunda-feira', 30),
(44, 7, 'terca-feira', 30),
(45, 7, 'quarta-feira', 30),
(46, 7, 'quinta-feira', 30),
(47, 7, 'sexta-feira', 30),
(48, 7, 'sabado', 30),
(49, 7, 'domingo', 20),
(50, 8, 'segunda-feira', 30),
(51, 8, 'terca-feira', 30),
(52, 8, 'quarta-feira', 30),
(53, 8, 'quinta-feira', 30),
(54, 8, 'sexta-feira', 30),
(55, 8, 'sabado', 30),
(56, 8, 'domingo', 20);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil_usuario`
--

CREATE TABLE `perfil_usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id`, `nome`) VALUES
(1, 'administrador'),
(2, 'entregador'),
(3, 'pdv'),
(4, 'Supervisor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(500) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  `valor` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `descricao`, `valor`) VALUES
(1, 'DIÁRIO DO PARÁ', NULL, NULL),
(2, 'DIÁRIO OFICIAL', NULL, NULL),
(3, 'DIÁRIO DO PARÁ - ANANINDEUA', NULL, NULL),
(4, 'DIÁRIO DO PARÁ-DIST. MOTTIVA', NULL, NULL),
(5, 'MD ENTREGAS-DIÁRIO DO PARÁ', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `rota`
--

CREATE TABLE `rota` (
  `id` int(11) NOT NULL,
  `nome_rota` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `rota`
--

INSERT INTO `rota` (`id`, `nome_rota`) VALUES
(1, 'Belem-1'),
(2, 'Belem-2'),
(3, 'AM-1'),
(4, 'AM-2'),
(5, 'Icoaraci-1'),
(6, 'Icoaraci-2'),
(7, 'Belém Alimentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `rota_usuario_supervisor`
--

CREATE TABLE `rota_usuario_supervisor` (
  `id` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `rota_usuario_supervisor`
--

INSERT INTO `rota_usuario_supervisor` (`id`, `id_rota`, `id_usuario`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 3, 4),
(4, 5, 4),
(5, 4, 4),
(6, 6, 4),
(7, 7, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_financeiro`
--

CREATE TABLE `situacao_financeiro` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_item_pedido`
--

CREATE TABLE `situacao_item_pedido` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `situacao_item_pedido`
--

INSERT INTO `situacao_item_pedido` (`id`, `nome`) VALUES
(1, 'cadastrado'),
(2, 'avaliado_entregador'),
(3, 'avaliado_supervidor'),
(4, 'Homologado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_pedido`
--

CREATE TABLE `situacao_pedido` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `situacao_pedido`
--

INSERT INTO `situacao_pedido` (`id`, `nome`) VALUES
(1, 'cadastrado'),
(2, 'avaliado_entregador'),
(3, 'avaliado_supervidor'),
(4, 'Homologado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacao_rota_senamal`
--

CREATE TABLE `situacao_rota_senamal` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `situacao_rota_senamal`
--

INSERT INTO `situacao_rota_senamal` (`id`, `nome`) VALUES
(1, 'avaliado_supervidor'),
(2, 'Homologado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `perfil_id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `ddd` int(4) DEFAULT NULL,
  `celular` int(4) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` text DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `perfil_id`, `nome`, `ddd`, `celular`, `email`, `senha`, `data_cadastro`) VALUES
(1, 1, 'Eraldo', NULL, NULL, 'eraldoctc@gmail.com', '6750727f47ed932117fa2e6cfa3e6b486640de574e78325bab51298d42b79d06406aed48c060d64c66d34d8ecee97f6a9cdef3cd0897ccd385804cae7ad5bc56+1IjAWsZ+7VON1ny8XUHOlh4/KLzekaPFbXUM/0+99Y=', '2020-05-27 21:35:46'),
(2, 2, 'Luiz Danin', 92, 980873514, 'otaviodanin@gmail.com', '553fba362d79d7200a29696fda78205e712e658390366b6e0cc6ce91e3c97da188b383e0ebcd6990ad4286d8f8e30eb863b03baf50c555b80ac038b153febf079KPeXgC4lslQu6hSa92NVYYF/Y2cTF4GNCpc6dLF4vM=', '2020-05-27 21:35:46'),
(3, 2, 'Carlos Portela', 91, 987878787, 'carlos@email.com', '553fba362d79d7200a29696fda78205e712e658390366b6e0cc6ce91e3c97da188b383e0ebcd6990ad4286d8f8e30eb863b03baf50c555b80ac038b153febf079KPeXgC4lslQu6hSa92NVYYF/Y2cTF4GNCpc6dLF4vM=', NULL),
(4, 4, 'Supervisor', NULL, NULL, 'supervisor@email.com', '553fba362d79d7200a29696fda78205e712e658390366b6e0cc6ce91e3c97da188b383e0ebcd6990ad4286d8f8e30eb863b03baf50c555b80ac038b153febf079KPeXgC4lslQu6hSa92NVYYF/Y2cTF4GNCpc6dLF4vM=', '2020-10-13 00:38:19'),
(16, 2, 'Jr Entregas', 91, 997979797, 'jr@email.com', '8504b363cbda1a20b0c1f9cb203f6202bc3d09d07c54267c3ed6a065fd41b1c79c09a5b276440e74a22627d08ac02c543165f45627df1416d85eb107fefe088ccRvVybA5yvrUFnIwGjQaG3jVWQgnpZEgdXqE/sBNy80=', '2020-11-16 00:26:36'),
(17, 2, 'JP Entregas', 91, 994949494, 'jp@email.com', '0446220ee73ea6346035137517451ec8c1dfa52d1130ca3c27f6616e69fcfad6e1647564fbf4a66a966acdfa9ae9fe86031d32e60b8dcee766e7d199cb379d7cNkcNVoHuQshIlvANRBq6drvkhb0bVUoWcmSejFtzyV4=', '2020-11-16 00:33:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_rota`
--

CREATE TABLE `usuario_rota` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario_rota`
--

INSERT INTO `usuario_rota` (`id`, `id_usuario`, `id_rota`) VALUES
(1, 16, 7),
(2, 17, 7),
(3, 16, 1),
(4, 17, 2),
(5, 16, 3),
(6, 17, 4),
(7, 16, 5),
(8, 17, 6),
(9, 2, 1),
(10, 3, 2),
(11, 2, 3),
(12, 3, 4),
(13, 2, 5),
(14, 3, 6),
(15, 2, 7);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `area_rota`
--
ALTER TABLE `area_rota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_area_entrega_has_ponto_entrega_ponto_entrega1_idx` (`id_rota`),
  ADD KEY `fk_area_entrega_has_ponto_entrega_area_entrega_idx` (`id_area`);

--
-- Índices para tabela `autoria_acesso`
--
ALTER TABLE `autoria_acesso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_id` (`session_id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices para tabela `financeiro`
--
ALTER TABLE `financeiro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_financeiro_tipo_financeiro1_idx` (`id_situacao`),
  ADD KEY `fk_financeiro_item_entregra1_idx` (`id_item`);

--
-- Índices para tabela `item_pedido`
--
ALTER TABLE `item_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_item_entregra_produto1_idx` (`id_produto`),
  ADD KEY `id_pedido` (`id_pedido`);

--
-- Índices para tabela `item_produto`
--
ALTER TABLE `item_produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produto` (`id_produto`);

--
-- Índices para tabela `log_item_pedido`
--
ALTER TABLE `log_item_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_situacao` (`id_situacao`);

--
-- Índices para tabela `log_pedido`
--
ALTER TABLE `log_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pedido` (`id_pedido`),
  ADD KEY `id_situacao` (`id_situacao`);

--
-- Índices para tabela `log_rota_senamal`
--
ALTER TABLE `log_rota_senamal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rota` (`id_rota`),
  ADD KEY `id_situacao` (`id_situacao`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices para tabela `pdv`
--
ALTER TABLE `pdv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rota_ponto1_idx` (`id_rota`);

--
-- Índices para tabela `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_entrega_rota_entrega1_idx` (`id_pdv`);

--
-- Índices para tabela `percentual_semanal_pdv`
--
ALTER TABLE `percentual_semanal_pdv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pdv` (`id_pdv`);

--
-- Índices para tabela `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `rota`
--
ALTER TABLE `rota`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `rota_usuario_supervisor`
--
ALTER TABLE `rota_usuario_supervisor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rota` (`id_rota`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices para tabela `situacao_financeiro`
--
ALTER TABLE `situacao_financeiro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `situacao_item_pedido`
--
ALTER TABLE `situacao_item_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `situacao_pedido`
--
ALTER TABLE `situacao_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `situacao_rota_senamal`
--
ALTER TABLE `situacao_rota_senamal`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_usuario_perfil1_idx` (`perfil_id`);

--
-- Índices para tabela `usuario_rota`
--
ALTER TABLE `usuario_rota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_rota` (`id_rota`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `area_rota`
--
ALTER TABLE `area_rota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `autoria_acesso`
--
ALTER TABLE `autoria_acesso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT de tabela `financeiro`
--
ALTER TABLE `financeiro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `item_pedido`
--
ALTER TABLE `item_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de tabela `item_produto`
--
ALTER TABLE `item_produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `log_item_pedido`
--
ALTER TABLE `log_item_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT de tabela `log_pedido`
--
ALTER TABLE `log_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de tabela `log_rota_senamal`
--
ALTER TABLE `log_rota_senamal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pdv`
--
ALTER TABLE `pdv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `percentual_semanal_pdv`
--
ALTER TABLE `percentual_semanal_pdv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de tabela `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `rota`
--
ALTER TABLE `rota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `rota_usuario_supervisor`
--
ALTER TABLE `rota_usuario_supervisor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `situacao_financeiro`
--
ALTER TABLE `situacao_financeiro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `situacao_item_pedido`
--
ALTER TABLE `situacao_item_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `situacao_pedido`
--
ALTER TABLE `situacao_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `situacao_rota_senamal`
--
ALTER TABLE `situacao_rota_senamal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `usuario_rota`
--
ALTER TABLE `usuario_rota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `area_rota`
--
ALTER TABLE `area_rota`
  ADD CONSTRAINT `fk_area_entrega_has_ponto_entrega_area_entrega` FOREIGN KEY (`id_area`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_area_entrega_has_ponto_entrega_ponto_entrega1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `autoria_acesso`
--
ALTER TABLE `autoria_acesso`
  ADD CONSTRAINT `autoria_acesso_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Limitadores para a tabela `financeiro`
--
ALTER TABLE `financeiro`
  ADD CONSTRAINT `fk_financeiro_item_entregra1` FOREIGN KEY (`id_item`) REFERENCES `item_pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_financeiro_tipo_financeiro1` FOREIGN KEY (`id_situacao`) REFERENCES `situacao_financeiro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `item_pedido`
--
ALTER TABLE `item_pedido`
  ADD CONSTRAINT `fk_item_entregra_produto1` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `item_pedido_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`);

--
-- Limitadores para a tabela `item_produto`
--
ALTER TABLE `item_produto`
  ADD CONSTRAINT `item_produto_ibfk_1` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id`);

--
-- Limitadores para a tabela `log_item_pedido`
--
ALTER TABLE `log_item_pedido`
  ADD CONSTRAINT `log_item_pedido_ibfk_1` FOREIGN KEY (`id_situacao`) REFERENCES `situacao_item_pedido` (`id`);

--
-- Limitadores para a tabela `log_pedido`
--
ALTER TABLE `log_pedido`
  ADD CONSTRAINT `log_pedido_ibfk_1` FOREIGN KEY (`id_situacao`) REFERENCES `situacao_pedido` (`id`);

--
-- Limitadores para a tabela `log_rota_senamal`
--
ALTER TABLE `log_rota_senamal`
  ADD CONSTRAINT `log_rota_senamal_ibfk_1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`),
  ADD CONSTRAINT `log_rota_senamal_ibfk_2` FOREIGN KEY (`id_situacao`) REFERENCES `situacao_rota_senamal` (`id`),
  ADD CONSTRAINT `log_rota_senamal_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Limitadores para a tabela `pdv`
--
ALTER TABLE `pdv`
  ADD CONSTRAINT `fk_rota_ponto1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_entrega_rota_entrega1` FOREIGN KEY (`id_pdv`) REFERENCES `pdv` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `percentual_semanal_pdv`
--
ALTER TABLE `percentual_semanal_pdv`
  ADD CONSTRAINT `percentual_semanal_pdv_ibfk_1` FOREIGN KEY (`id_pdv`) REFERENCES `pdv` (`id`);

--
-- Limitadores para a tabela `rota_usuario_supervisor`
--
ALTER TABLE `rota_usuario_supervisor`
  ADD CONSTRAINT `rota_usuario_supervisor_ibfk_1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`),
  ADD CONSTRAINT `rota_usuario_supervisor_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_perfil1` FOREIGN KEY (`perfil_id`) REFERENCES `perfil_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario_rota`
--
ALTER TABLE `usuario_rota`
  ADD CONSTRAINT `usuario_rota_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `usuario_rota_ibfk_2` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
