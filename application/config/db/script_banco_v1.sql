CREATE TABLE IF NOT EXISTS `perfil` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `ponto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_ponto` VARCHAR(150) NULL,
  `funcionario` VARCHAR(150) NULL,
  `telefone` VARCHAR(150) NULL,
  `endereco` VARCHAR(45) NULL,
  `data_cadastro` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `area` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_area` VARCHAR(150) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `rota` (
  `id` INT NOT NULL,
  `nome_rota` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `entrega` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_ponto` INT NOT NULL,
  `id_rota` INT NOT NULL,
  `data_entrega` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_entrega_rota_entrega1_idx` (`id_rota` ASC),
  INDEX `fk_entrega_ponto1_idx` (`id_ponto` ASC),
  CONSTRAINT `fk_entrega_rota_entrega1`
    FOREIGN KEY (`id_rota`)
    REFERENCES `rota` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entrega_ponto1`
    FOREIGN KEY (`id_ponto`)
    REFERENCES `ponto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `area_ponto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `area_entrega_id` INT NOT NULL,
  `ponto_entrega_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_area_entrega_has_ponto_entrega_ponto_entrega1_idx` (`ponto_entrega_id` ASC),
  INDEX `fk_area_entrega_has_ponto_entrega_area_entrega_idx` (`area_entrega_id` ASC),
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_area_entrega`
    FOREIGN KEY (`area_entrega_id`)
    REFERENCES `area` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_ponto_entrega1`
    FOREIGN KEY (`ponto_entrega_id`)
    REFERENCES `ponto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(500) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `situacao_financeiro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `item_entregra` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_entrega` INT NOT NULL,
  `id_produto` INT NOT NULL,
  `valor` VARCHAR(100) NULL,
  `encalhe` VARCHAR(500) NULL,
  `reparte` VARCHAR(500) NULL,
  `data_` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_item_entregra_produto1_idx` (`id_produto` ASC),
  INDEX `fk_item_entregra_entrega1_idx` (`id_entrega` ASC),
  CONSTRAINT `fk_item_entregra_produto1`
    FOREIGN KEY (`id_produto`)
    REFERENCES `produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_entregra_entrega1`
    FOREIGN KEY (`id_entrega`)
    REFERENCES `entrega` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `financeiro` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_item` INT NOT NULL,
  `id_situacao` INT NOT NULL,
  `valor` VARCHAR(100) NULL,
  `data` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_financeiro_tipo_financeiro1_idx` (`id_situacao` ASC) ,
  INDEX `fk_financeiro_item_entregra1_idx` (`id_item` ASC) ,
  CONSTRAINT `fk_financeiro_tipo_financeiro1`
    FOREIGN KEY (`id_situacao`)
    REFERENCES `situacao_financeiro` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_financeiro_item_entregra1`
    FOREIGN KEY (`id_item`)
    REFERENCES `item_entregra` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;