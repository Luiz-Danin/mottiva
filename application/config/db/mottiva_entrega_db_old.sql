-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for FreeBSD12.1 (amd64)
--
-- Host: localhost    Database: mottiva_entrega_db
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_area` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Belém'),(2,'Ananindeua/Marituba'),(3,'Icoaraci');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area_rota`
--

DROP TABLE IF EXISTS `area_rota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_rota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_area` int(11) NOT NULL,
  `id_rota` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_area_entrega_has_ponto_entrega_ponto_entrega1_idx` (`id_rota`),
  KEY `fk_area_entrega_has_ponto_entrega_area_entrega_idx` (`id_area`),
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_area_entrega` FOREIGN KEY (`id_area`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_area_entrega_has_ponto_entrega_ponto_entrega1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_rota`
--

LOCK TABLES `area_rota` WRITE;
/*!40000 ALTER TABLE `area_rota` DISABLE KEYS */;
INSERT INTO `area_rota` VALUES (7,1,1),(8,1,2),(9,2,3),(10,2,4),(11,3,5),(12,3,6);
/*!40000 ALTER TABLE `area_rota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autoria_acesso`
--

DROP TABLE IF EXISTS `autoria_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoria_acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `localizacao` varchar(200) NOT NULL,
  `is_robot` tinyint(1) NOT NULL,
  `is_mobile` varchar(200) NOT NULL,
  `agent_string` varchar(50) NOT NULL,
  `plataforma` varchar(50) NOT NULL,
  `navegador` varchar(50) NOT NULL,
  `sistema` varchar(50) NOT NULL,
  `data_acesso` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoria_acesso`
--

LOCK TABLES `autoria_acesso` WRITE;
/*!40000 ALTER TABLE `autoria_acesso` DISABLE KEYS */;
INSERT INTO `autoria_acesso` VALUES (1,'19ca14e7ea6328a42e0eb13d585e4c22',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-27 21:40:51'),(2,'a1d0c6e83f027327d8461063f4ac58a6',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-27 22:43:43'),(3,'c81e728d9d4c2f636f067f89cc14862c',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-28 00:16:02'),(4,'e4da3b7fbbce2345d7772b0674a318d5',1,'45.235.222.117','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:45.235.222.117',0,'1','Mozilla/5.0 (Linux; Android 6.0; LG-X230) AppleWeb','Android','Chrome - 81.0.4044.138','Android','2020-05-28 18:37:04'),(5,'98f13708210194c475687be6106a3b84',1,'45.235.222.117','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:45.235.222.117',0,'0','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ','Windows 7','Firefox - 76.0','Windows 7','2020-05-28 18:39:54'),(6,'45c48cce2e2d7fbdea1afc51c7c6ad26',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-28 22:20:32'),(7,'6512bd43d9caa6e02c990b0a82652dca',1,'177.74.61.129','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129',0,'0','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ','Windows 7','Firefox - 76.0','Windows 7','2020-05-29 09:13:18'),(8,'d9d4f495e875a2e075a1a4a6e1b9770f',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-29 19:33:59'),(9,'e4da3b7fbbce2345d7772b0674a318d5',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'1','Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple','Android','Chrome - 83.0.4103.83','Android','2020-05-29 21:27:06'),(10,'e4da3b7fbbce2345d7772b0674a318d5',1,'2804:14c:5988:93a2:3055:dca4:141d:6072','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:3055:dca4:141d:6072',0,'1','Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple','Android','Chrome - 83.0.4103.83','Android','2020-05-30 13:17:14'),(11,'e4da3b7fbbce2345d7772b0674a318d5',1,'186.207.197.131','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:186.207.197.131',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-30 17:23:03'),(12,'e4da3b7fbbce2345d7772b0674a318d5',1,'2804:14c:5988:93a2:4d08:66c1:4eea:5be7','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:4d08:66c1:4eea:5be7',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-30 21:19:16'),(13,'e4da3b7fbbce2345d7772b0674a318d5',1,'2804:14c:5988:93a2:d8da:77b7:45f8:8dde','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:d8da:77b7:45f8:8dde',0,'1','Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple','Android','Chrome - 83.0.4103.83','Android','2020-05-31 15:20:05'),(14,'45c48cce2e2d7fbdea1afc51c7c6ad26',1,'2804:14c:5988:93a2:14c7:85f9:4a67:632c','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:14c7:85f9:4a67:632c',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-05-31 20:53:34'),(15,'a87ff679a2f3e71d9181a67b7542122c',1,'177.74.61.129','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129',0,'0','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:76.0) ','Windows 7','Firefox - 76.0','Windows 7','2020-06-01 14:58:02'),(16,'c74d97b01eae257e44aa9d5bade97baf',1,'2804:14c:5988:93a2:254f:b20c:3967:541e','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:254f:b20c:3967:541e',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.61','Linux','2020-06-13 15:48:48'),(17,'45c48cce2e2d7fbdea1afc51c7c6ad26',1,'2804:14c:5988:93a2:85ce:3814:d599:d1ed','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:85ce:3814:d599:d1ed',0,'0','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36','Linux','Chrome - 83.0.4103.97','Linux','2020-06-14 17:44:36'),(18,'aab3238922bcc25a6f606eb525ffdc56',1,'2804:14c:5988:93a2:bdb1:d17:3363:df1e','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:2804:14c:5988:93a2:bdb1:d17:3363:df1e',0,'1','Mozilla/5.0 (Linux; Android 8.1.0; SM-J260M) Apple','Android','Chrome - 83.0.4103.101','Android','2020-06-14 22:20:37'),(19,'8f14e45fceea167a5a36dedd4bea2543',1,'177.74.61.129','Cidade: Belém/ Regiao: Pará/ Pais: BR/ Coordenadas Aprox: -1.4558,-48.5044/ CEP: 66000-000/ IP:177.74.61.129',0,'0','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:77.0) ','Windows 7','Firefox - 77.0','Windows 7','2020-06-15 14:14:12');
/*!40000 ALTER TABLE `autoria_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrega`
--

DROP TABLE IF EXISTS `entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pdv` int(11) NOT NULL,
  `data_entrega` datetime DEFAULT NULL,
  `data_entrrega_final` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entrega_rota_entrega1_idx` (`id_pdv`),
  CONSTRAINT `fk_entrega_rota_entrega1` FOREIGN KEY (`id_pdv`) REFERENCES `pdv` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrega`
--

LOCK TABLES `entrega` WRITE;
/*!40000 ALTER TABLE `entrega` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financeiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_situacao` int(11) NOT NULL,
  `valor` varchar(100) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_financeiro_tipo_financeiro1_idx` (`id_situacao`),
  KEY `fk_financeiro_item_entregra1_idx` (`id_item`),
  CONSTRAINT `fk_financeiro_item_entregra1` FOREIGN KEY (`id_item`) REFERENCES `item_produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_financeiro_tipo_financeiro1` FOREIGN KEY (`id_situacao`) REFERENCES `situacao_financeiro` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `financeiro`
--

LOCK TABLES `financeiro` WRITE;
/*!40000 ALTER TABLE `financeiro` DISABLE KEYS */;
/*!40000 ALTER TABLE `financeiro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_produto`
--

DROP TABLE IF EXISTS `item_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_entrega` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `encalhe` varchar(500) DEFAULT NULL,
  `reparte` varchar(500) DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_devolucao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_entregra_produto1_idx` (`id_produto`),
  KEY `fk_item_entregra_entrega1_idx` (`id_entrega`),
  CONSTRAINT `fk_item_entregra_entrega1` FOREIGN KEY (`id_entrega`) REFERENCES `entrega` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_item_entregra_produto1` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_produto`
--

LOCK TABLES `item_produto` WRITE;
/*!40000 ALTER TABLE `item_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdv`
--

DROP TABLE IF EXISTS `pdv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rota` int(11) NOT NULL,
  `nome_pdv` varchar(250) DEFAULT NULL,
  `funcionario` varchar(150) DEFAULT NULL,
  `ddd` int(4) DEFAULT NULL,
  `telefone` int(10) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `endereco` varchar(250) DEFAULT NULL,
  `numero` varchar(100) NOT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_edicao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rota_ponto1_idx` (`id_rota`),
  CONSTRAINT `fk_rota_ponto1` FOREIGN KEY (`id_rota`) REFERENCES `rota` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdv`
--

LOCK TABLES `pdv` WRITE;
/*!40000 ALTER TABLE `pdv` DISABLE KEYS */;
INSERT INTO `pdv` VALUES (1,1,'Feira da Terra Firme','Sr. Ronaldo',91,988917980,'66077000','Avenida Celso Malcher','712','2020-05-30 00:40:51',NULL),(2,1,'Terra Firme','Ronaldo',91,988917980,'66077000','Avenida Celso Malcher','712','2020-05-30 00:42:52',NULL),(3,2,'Mateus Supermercados Marambaia','Luiz',91,988617980,'66615005','Avenida Tavares Bastos','1234','2020-06-13 16:59:36','2020-06-13 18:32:11');
/*!40000 ALTER TABLE `pdv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_usuario`
--

LOCK TABLES `perfil_usuario` WRITE;
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` VALUES (1,'administrador'),(2,'entregador'),(3,'pdv');
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edicao` varchar(100) DEFAULT NULL,
  `nome` varchar(500) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  `peso` double DEFAULT NULL,
  `valor` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rota`
--

DROP TABLE IF EXISTS `rota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_rota` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rota`
--

LOCK TABLES `rota` WRITE;
/*!40000 ALTER TABLE `rota` DISABLE KEYS */;
INSERT INTO `rota` VALUES (1,'Belem-1'),(2,'Belem-2'),(3,'AM-1'),(4,'AM-2'),(5,'Icoaraci-1'),(6,'Icoaraci-2');
/*!40000 ALTER TABLE `rota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `situacao_financeiro`
--

DROP TABLE IF EXISTS `situacao_financeiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `situacao_financeiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `situacao_financeiro`
--

LOCK TABLES `situacao_financeiro` WRITE;
/*!40000 ALTER TABLE `situacao_financeiro` DISABLE KEYS */;
/*!40000 ALTER TABLE `situacao_financeiro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `ddd` int(4) DEFAULT NULL,
  `celular` int(4) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` text DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_usuario_perfil1_idx` (`perfil_id`),
  CONSTRAINT `fk_usuario_perfil1` FOREIGN KEY (`perfil_id`) REFERENCES `perfil_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'Eraldo',NULL,NULL,'eraldoctc@gmail.com','6750727f47ed932117fa2e6cfa3e6b486640de574e78325bab51298d42b79d06406aed48c060d64c66d34d8ecee97f6a9cdef3cd0897ccd385804cae7ad5bc56+1IjAWsZ+7VON1ny8XUHOlh4/KLzekaPFbXUM/0+99Y=','2020-05-27 21:35:46'),(2,2,'Luiz',92,980873514,'otaviodanin@gmail.com','553fba362d79d7200a29696fda78205e712e658390366b6e0cc6ce91e3c97da188b383e0ebcd6990ad4286d8f8e30eb863b03baf50c555b80ac038b153febf079KPeXgC4lslQu6hSa92NVYYF/Y2cTF4GNCpc6dLF4vM=','2020-05-27 21:35:46');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-13 21:36:24
