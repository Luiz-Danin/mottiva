<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Site
$route['login'] = 'Site/pagina_login';
$route['do_login'] = 'Site/do_login';
$route['logout'] = 'Entrega/sair';

//Administrador
$route['default_controller'] = 'Site/';
$route['pdv'] = 'Entrega/pagina_pdv';
$route['do_ponto'] = 'Entrega/add_ponto';
$route['form_pdv'] = 'Entrega/form_pdv';
$route['form_edit_pdv/(:any)'] = 'Entrega/pagina_edit_pdv/$1';
$route['do_edit_pdv'] = 'Entrega/edit_ponto';
$route['pagina_entrega/(:any)'] = 'Entrega/pagina_entrega/$1';
$route['detalhe_pdv/(:any)'] = 'Entrega/detalhe_pdv/$1';
$route['do_entrega'] = 'Entrega/do_entrega';

$route['lista_rota'] = 'Entrega/lista_rota';

$route['week'] = 'Entrega/get_value_week';
$route['build_week'] = 'Entrega/build_week';
$route['pedido'] = 'Entrega/do_pedido';
$route['load_modal'] = 'Entrega/load_modal';

$route['add_area'] = 'Entrega/form_area';
$route['do_area'] = 'Entrega/do_area';

$route['add_rota'] = 'Entrega/form_rota';
$route['do_rota'] = 'Entrega/do_rota';

$route['add_entregador'] = 'Entrega/form_entregador';
$route['do_entregador'] = 'Entrega/do_entregador';
$route['modal_percentual'] = 'Entrega/load_modal_percentual';
$route['save_percentual'] = 'Entrega/save_percentual';
//Entregador
$route['entrega'] = 'Entregador/home';
$route['modal_entregador'] = 'Entregador/load_modal_entregador';
$route['save_encalhe'] = 'Entregador/do_encalhe';

//Supervisor
$route['supervisor'] = 'Supervisor/home';
$route['modal_supervidor']  = 'Supervisor/load_modal_supervidor';
$route['valida_encalhe'] = 'Supervisor/do_valida';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;