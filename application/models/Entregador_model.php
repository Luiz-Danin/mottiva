<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entregador_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function selec_itens_pedido($id_pedido)
    {
        $this->db->select('id, id_pedido,reparte, encalhe, edicao,data_reparte');
        $this->db->from('item_pedido');
        $this->db->where('id_pedido', $id_pedido);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    public function get_pedido_entregador($id)
    {
        $this->db->select('pdv.nome_pdv, pedido.id AS id_pedido, pedido.data_cadastro');
        $this->db->from('pdv');
        $this->db->join('pedido','pdv.id = pedido.id_pdv');
        $this->db->join('usuario','pedido.id_usuario_reparte = usuario.id');
        $this->db->where('pedido.id_usuario_reparte',$id);
        $this->db->where('usuario.perfil_id', 2);
        $this->db->order_by('pedido.id', 'DESC');
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
    public function select_situacao_log_pedido($id_pedido)
    {
        $sql = "SELECT * FROM log_pedido WHERE id_pedido=:id_pedido ORDER BY id DESC";

        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->bindValue(":id_pedido", $id_pedido);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function select_encalhe_pedido_null($id_pedido)
    {
        $sql = "SELECT * FROM item_pedido WHERE id_pedido=:id_pedido AND encalhe IS NULL";

        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->bindValue(":id_pedido", $id_pedido);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function select_item_pedido($id)
    {
        $sql = "SELECT * FROM item_pedido WHERE id=:id";

        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function update_encalhe($id, $encalhe, $situacao)
    {
        $sql = "UPDATE item_pedido SET encalhe=:encalhe, data_encalhe=:data_encalhe, situacao=:situacao  WHERE id=:id";
        $stmt = $this->db->conn_id->prepare($sql);
        
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':encalhe', $encalhe, PDO::PARAM_STR);
        $stmt->bindValue(':situacao', $situacao, PDO::PARAM_INT);
        $stmt->bindValue(':data_encalhe', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        if( $stmt->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function add_log_item_pedido($id_item_pedido, $id_situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agente_string, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_item_pedido (id_item_pedido, id_usuario, id_situacao, reparte ,encalhe, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_item_pedido, :id_usuario, :id_situacao, :reparte, :encalhe, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_item_pedido', $id_item_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':reparte', $reparte, PDO::PARAM_STR);
        $sth->bindValue(':encalhe', $encalhe, PDO::PARAM_STR);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agente_string, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function add_log_pedido($id_pedido, $id_situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_pedido (id_pedido, id_situacao, id_usuario, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_pedido, :id_situacao, :id_usuario, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_pedido', $id_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agent, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        return $sth->execute();
    }
    
    public function update_usuario_encalhe($id, $id_usuario_encalhe, $situacao)
    {
        $sql = "UPDATE pedido SET id_usuario_encalhe=:id_usuario_encalhe, situacao=:situacao WHERE id=:id";
        
        $stmt = $this->db->conn_id->prepare($sql);
        
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':situacao', $situacao, PDO::PARAM_INT);
        $stmt->bindValue(':id_usuario_encalhe', $id_usuario_encalhe, PDO::PARAM_STR);
        
        $stmt->execute();
    }

    public function select_situacao_log($id_pedido, $id_situacao, $id_usuario)
    {
        $this->db->select();
        $this->db->from('log_pedido');
        $this->db->where('id_pedido', $id_pedido);
        $this->db->where('id_situacao', $id_situacao);
        $this->db->where('id_usuario', $id_usuario);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

}
