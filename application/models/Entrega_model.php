<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entrega_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function verifica_login($email)
    {
        $this->db->select();
        $this->db->from('usuario');
        $this->db->where('email',$email);
        
        $query = $this->db->get();
        $usuario = $query->result_array();
        
        return $usuario;
    }
    
    public function add_usuario($id_perfil, $nome, $ddd, $celular, $email, $senha, $data_cadastro)
    {
        $sql = "INSERT INTO usuario (perfil_id, nome, ddd, celular, email, senha, data_cadastro) VALUES (:perfil_id, :nome, :ddd, :celular, :email, :senha, :data_cadastro)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':perfil_id', $id_perfil, PDO::PARAM_INT);
        $sth->bindValue(':nome', $nome, PDO::PARAM_STR);
        $sth->bindValue(':ddd', $ddd, PDO::PARAM_INT);
        $sth->bindValue(':celular', $celular, PDO::PARAM_INT);
        $sth->bindValue(':email', $email, PDO::PARAM_STR);
        $sth->bindValue(':senha', $senha, PDO::PARAM_STR);
        $sth->bindValue(':data_cadastro', $data_cadastro, PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return $this->db->conn_id->lastInsertId();
        }
        
        return FALSE;
    }
    
    public function add_usuario_rota($id_usuario, $id_rota)
    {
        $sql = "INSERT INTO usuario_rota (id_usuario, id_rota) VALUES (:id_usuario, :id_rota)";
        
        $sth = $this->db->conn_id->prepare($sql);
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':id_rota', $id_rota, PDO::PARAM_INT);
        
        return $sth->execute();
    }

    public function add_rota($nome)
    {
        $sql = "INSERT INTO rota (nome_rota) VALUES (:nome_rota)";
        
        $sth = $this->db->conn_id->prepare($sql);
        $sth->bindValue(':nome_rota', $nome, PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return $this->db->conn_id->lastInsertId();
        }
        
        return FALSE;
    }
    
    public function add_area_rota($id_area, $id_rota)
    {
        $sql = "INSERT INTO area_rota (id_area, id_rota) VALUES (:id_area, :id_rota)";
        
        $sth = $this->db->conn_id->prepare($sql);
        $sth->bindValue(':id_area', $id_area, PDO::PARAM_INT);
        $sth->bindValue(':id_rota', $id_rota, PDO::PARAM_INT);
        
        return $sth->execute();
    }

    public function add_area($nome)
    {
        $sql = "INSERT INTO area (nome_area) VALUES (:nome_area)";
        
        $sth = $this->db->conn_id->prepare($sql);
        $sth->bindValue(':nome_area', $nome, PDO::PARAM_STR);
        
        return $sth->execute();
    }
    
    public function lista_rotas()
    {
        $this->db->select();
        $this->db->from('rota');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function lista_rota($id_rota)
    {
        $this->db->select();
        $this->db->from('rota');
        $this->db->where('id',$id_rota);
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function lista_area_rota()
    {
        $this->db->select();
        $this->db->from('area_rota');
        $this->db->join("area","area_rota.id_area = area.id");
        $this->db->join("rota","area_rota.id_rota = rota.id");
        $this->db->order_by('area.id ASC');
        $this->db->order_by('rota.nome_rota ASC');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_data_produtos()
    {
        $this->db->select();
        $this->db->from('produto');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_usuario_rota_id($id_rota)
    {
        $this->db->select('id_usuario');
        $this->db->from('usuario_rota');
        $this->db->where('id_rota', $id_rota);
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_user_entregador($id_usuario)
    {
        $this->db->select('id, nome');
        $this->db->from('usuario');
        $this->db->where('id',$id_usuario);
        $this->db->where('perfil_id','2');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_ponto($pdv)
    {
        $this->db->insert('pdv', $pdv);
        
        return $this->db->insert_id();
    }
    
    public function area_ponto($area_ponto)
    {
        $this->db->insert('area_ponto', $area_ponto);
    }
    
    public function add_pedido($dados_pedido)
    {
        $sql = "INSERT INTO pedido (id_pdv, id_usuario_reparte, total_peso,data_cadastro, situacao) VALUES(:id_pdv, :id_usuario_reparte, :total_peso,:data_cadastro, :situacao)";
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_pdv', $dados_pedido['id_pdv'], PDO::PARAM_INT); 
        $sth->bindValue(':id_usuario_reparte', $dados_pedido['id_usuario_reparte'], PDO::PARAM_INT);
        $sth->bindValue(':total_peso', $dados_pedido['total_peso'], PDO::PARAM_INT);
        $sth->bindValue(':data_cadastro', $dados_pedido['data_cadastro'], PDO::PARAM_STR);
        $sth->bindValue(':situacao', $dados_pedido['situacao'], PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return $this->db->conn_id->lastInsertId();
        }
        return FALSE;
    }
    
    public function add_log_pedido($id_pedido, $id_situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_pedido (id_pedido, id_situacao, id_usuario, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_pedido, :id_situacao, :id_usuario, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_pedido', $id_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agent, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        $sth->execute();
    }
    
    public function add_log_item_pedido($id_item_pedido, $id_situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agente_string, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_item_pedido (id_item_pedido, id_usuario, id_situacao, reparte ,encalhe, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_item_pedido, :id_usuario, :id_situacao, :reparte, :encalhe, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_item_pedido', $id_item_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':reparte', $reparte, PDO::PARAM_STR);
        $sth->bindValue(':encalhe', $encalhe, PDO::PARAM_STR);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agente_string, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        $sth->execute();
    }

    public function add_item_entrega($id_pedido, $produto, $reparte, $edicao, $valor, $peso, $data_reparte, $situacao)
    {
        $sql = "INSERT INTO item_pedido (id_pedido, id_produto, reparte, edicao, valor, peso, data_reparte, situacao) VALUES(:id_pedido, :id_produto, :reparte, :edicao, :valor, :peso, :data_reparte, :situacao)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_pedido', $id_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_produto', $produto, PDO::PARAM_INT);
        $sth->bindValue(':reparte', $reparte, PDO::PARAM_INT);
        $sth->bindValue(':edicao', $edicao, PDO::PARAM_STR);
        $sth->bindValue(':valor', $valor, PDO::PARAM_STR);
        $sth->bindValue(':peso', $peso, PDO::PARAM_STR);
        $sth->bindValue(':data_reparte', $data_reparte, PDO::PARAM_STR);
        $sth->bindValue(':situacao', $situacao, PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return $this->db->conn_id->lastInsertId();
        }
        return FALSE;
    }

    public function aditoria_acesso($audit)
    {
        $auditoria_acesso = ['session_id'=>$audit['session_id'],'id_usuario'=>$audit['id_usuario'],'ip'=>$audit['ip'],
                   'localizacao'=>$audit['localizacao'],'is_robot'=>$audit['is_robot'],'is_mobile'=>$audit['is_mobile'],
                   'agent_string'=>$audit['agent_string'],'plataforma'=>$audit['plataforma'],
                   'navegador'=>$audit['navegador'], 'sistema'=>$audit['sistema'],'data_acesso'=>$audit['data_acesso']
                 ];
        $this->db->insert('autoria_acesso', $auditoria_acesso);
        
        return $this->db->insert_id();
    }
    
    public function edit_ponto($dados)
    {
        $this->db->set('id_rota', $dados['id_rota']);
        $this->db->set('nome_pdv', $dados['nome_pdv']);
        $this->db->set('funcionario', $dados['funcionario']);
        $this->db->set('ddd', $dados['ddd']);
        $this->db->set('telefone', $dados['telefone']);
        $this->db->set('cep', $dados['cep']);
        $this->db->set('endereco', $dados['endereco']);
        $this->db->set('numero', $dados['numero']);
        $this->db->set('complemento', $dados['complemento']);
        $this->db->set('data_edicao', $dados['data_edicao']);
        
        $this->db->where('id', $dados['id_pdv']);
        
        return $this->db->update('pdv');
    }
    
    public function edit_area_ponto($id_area, $id_ponto)
    {
        $this->db->set('area_entrega_id', $id_area);
        $this->db->where('ponto_entrega_id', $id_ponto );
        
        return $this->db->update('area_ponto');
    }
    
    public function get_data_pedido($id_pdv)
    {
        $this->db->select('pedido.id as pedido_id, pedido.id_pdv, pedido.id_usuario_reparte, '
                . 'pedido.id_usuario_encalhe, '
                . 'item_pedido.id,	item_pedido.id_pedido, item_pedido.id_produto,'
                . 'item_pedido.reparte,	item_pedido.encalhe, item_pedido.edicao, item_pedido.valor, '
                . 'item_pedido.peso, item_pedido.data_reparte');
        $this->db->from('pedido');
        $this->db->join('item_pedido', 'pedido.id = item_pedido.id_pedido');
        $this->db->where('id_pdv',$id_pdv);
        $this->db->group_by("pedido.id");
        $this->db->order_by("pedido.id");
        
        $query = $this->db->get();
        $pedido = $query->result_array();
        
        return $pedido;
    }
    
    public function get_modal_percentual_pdv($id_pdv)
    {
        $this->db->select();
        $this->db->from('percentual_semanal_pdv');
        $this->db->where('id_pdv',$id_pdv);
        $this->db->order_by("id");
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function update_percentual_pdv($id_pdv, $nome, $percentual)
    {
        $sql = "UPDATE percentual_semanal_pdv SET percentual=:percentual WHERE id_pdv=:id_pdv AND nome=:nome";
        $stmt = $this->db->conn_id->prepare($sql);
        
        $stmt->bindValue(':percentual', $percentual, PDO::PARAM_INT);
        $stmt->bindValue(':id_pdv', $id_pdv, PDO::PARAM_INT);
        $stmt->bindValue(':nome', $nome, PDO::PARAM_STR);
        
        if( $stmt->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function get_data_item_pedido($id_pedido)
    {
        $this->db->select();
        $this->db->from('item_pedido');
        $this->db->join('pedido', 'pedido.id = item_pedido.id_pedido');
        $this->db->where('id_pedido',$id_pedido);
        $this->db->group_by("item_pedido.id");
        $this->db->order_by("item_pedido.id");
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_data_usuario($id_usuario)
    {
        $this->db->select("nome");
        $this->db->from('usuario');
        $this->db->where('id', $id_usuario);
        
        $query = $this->db->get();
        $usuario = $query->result_array();
        
        return $usuario;
    }

    public function get_entrega_idponto($id_ponto)
    {
        $this->db->select();
        $this->db->from('entrega');
        $this->db->where('id_ponto',$id_ponto);
        
        $query = $this->db->get();
        $entrega = $query->result_array();
        
        return $entrega;
    }

    public function get_ponto_id($id)
    {
        $this->db->select();
        $this->db->from('pdv');
        $this->db->where('id',$id);
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_produto_by_id($id)
    {
        $this->db->select("nome");
        $this->db->from('produto');
        $this->db->where("id", $id);
        
        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function get_area_ponto_idponto($id_ponto)
    {
        $this->db->select();
        $this->db->from('area_ponto');
        $this->db->where('ponto_entrega_id',$id_ponto);
        
        $query = $this->db->get();
        $area_ponto = $query->result_array();
        
        return $area_ponto;
    }

    public function get_pontos()
    {
        $this->db->select();
        $this->db->from('pdv');
        
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_areas()
    {
        $this->db->select();
        $this->db->from('area');
        
        $query = $this->db->get();
        $areas = $query->result_array();
        
        return $areas;
    }
}