<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function select_pedidos_supervisor($id_usuario)
    {
        $this->db->select(' rota.id,rota.nome_rota, pedido.situacao');
        $this->db->from('pdv');
        
        $this->db->join('rota_usuario_supervisor','pdv.id_rota = rota_usuario_supervisor.id_rota');
        $this->db->join('rota','rota.id = rota_usuario_supervisor.id_rota');
        $this->db->join('usuario','rota_usuario_supervisor.id_usuario = usuario.id');
        $this->db->join('pedido','pdv.id = pedido.id_pdv');
        
        $this->db->where('rota_usuario_supervisor.id_usuario',$id_usuario);
        $this->db->where('usuario.perfil_id', 4);
        
        $this->db->order_by('rota.nome_rota', 'ASC');
        
        $this->db->group_by('rota.id');

        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function select_usuario($id_usuario)
    {
        try
        {
            $sql = "SELECT nome FROM usuario WHERE id=:id";

            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id", (int)$id_usuario);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function select_pedido_rota_entregador($id_rota)
    {
        try
        {
            $sql = "SELECT rota_usuario_supervisor.id_rota, pdv.id AS id_pdv, pdv.nome_pdv,
                    pedido.id AS id_pedido, pedido.id_usuario_encalhe usuario, pedido.situacao AS situacao_pedido,
                    item_pedido.id id_itempedido, item_pedido.reparte, item_pedido.encalhe, item_pedido.data_reparte, item_pedido.situacao AS situacao_item_pedido
                    FROM rota_usuario_supervisor
                    JOIN pdv ON (pdv.id_rota = rota_usuario_supervisor.id_rota) 
                    JOIN pedido ON (pdv.id = pedido.id_pdv) 
                    JOIN item_pedido ON (pedido.id = item_pedido.id_pedido)
                    WHERE pdv.id_rota =:id_rota
                    ORDER BY item_pedido.data_reparte ASC, item_pedido.id";

            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_rota", (int)$id_rota);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function select_pdv_by_rota($id_rota)
    {
        try
        {
            $sql = "SELECT rota_usuario_supervisor.id_rota, pdv.id AS id_pdv, pdv.nome_pdv, "
                    . "pedido.id AS id_pedido, "
                    . "item_pedido.id id_itempedido, item_pedido.reparte, item_pedido.encalhe, item_pedido.data_reparte"
                    . " FROM rota_usuario_supervisor "
                    . "JOIN pdv ON (pdv.id_rota = rota_usuario_supervisor.id_rota) "
                    . "JOIN pedido ON (pdv.id = pedido.id_pdv) "
                    . "JOIN item_pedido ON (pedido.id = item_pedido.id_pedido)"
                    . "WHERE pdv.id_rota = :id_rota "
                    . "ORDER BY item_pedido.data_reparte ASC";

            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_rota", (int)$id_rota);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function check_log_item($id_item_pedido)
    {
        try
        {
            $sql = "SELECT * FROM log_item_pedido WHERE id_item_pedido=:id_item_pedido ORDER BY id DESC";

            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_item_pedido", (int)$id_item_pedido);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function check_log_pedido($id_pedido)
    {
        try
        {
            $sql = "SELECT * FROM log_pedido WHERE id_pedido=:id_pedido ORDER BY id DESC";

            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_pedido", (int)$id_pedido);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function select_pedido_rota($id_rota, $date)
    {
        try
        {
            $sql = "SELECT pedido.id AS id_pedido FROM rota 
                    JOIN pdv ON (rota.id = pdv.id_rota) 
                    JOIN pedido ON (pdv.id = pedido.id_pdv)
                    JOIN item_pedido ON (pedido.id = item_pedido.id_pedido)
                    WHERE rota.id =:id_rota AND item_pedido.data_reparte=:data_reparte ";
            
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_rota", $id_rota, PDO::PARAM_INT);
            $stmt->bindValue(":data_reparte", $date, PDO::PARAM_STR);
            $stmt->execute();
        
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function check_log_item_pedido($id, $id_situacao)
    {
        $sql = "SELECT * FROM log_item_pedido WHERE id_item_pedido=:id_item_pedido AND AND id_situacao=:id_situacao";
            
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->bindValue(":id_item_pedido", $id, PDO::PARAM_INT);
        $stmt->bindValue(":id_situacao", $id_situacao, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function select_items_pedido_situacao($id_pedido, $situacao)
    {
        try
        {
            $sql = "SELECT id, situacao FROM item_pedido WHERE id_pedido=:id_pedido AND situacao=:situacao";
            
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_pedido", $id_pedido);
            $stmt->bindValue(":situacao", $situacao);
            $stmt->execute();
            
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function select_items_pedido($id_pedido)
    {
        try
        {
            $sql = "SELECT id, encalhe, reparte, data_reparte, situacao FROM item_pedido WHERE id_pedido=:id_pedido";
            
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_pedido", $id_pedido);
            $stmt->execute();
            
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function select_item_pedido($id_pedido, $date)
    {
        try
        {
            $sql = "SELECT id, reparte, encalhe FROM item_pedido WHERE id_pedido=:id_pedido AND data_reparte=:data_reparte";
            
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_pedido", (int)$id_pedido);
            $stmt->bindValue(":data_reparte", $date, PDO::PARAM_STR);
            $stmt->execute();
            
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function select_log_rota_senamal($id_rota, $id_situacao, $id_usuario, $dia_avaliado)
    {
        try
        {
            $sql = "SELECT * FROM log_rota_senamal "
                    . "WHERE id_rota=:id_rota "
                    . "AND id_situacao=:id_situacao "
                    . "AND id_usuario=:id_usuario "
                    . "AND dia_avaliado=:dia_avaliado";
            
            $stmt = $this->db->conn_id->prepare($sql);
            $stmt->bindValue(":id_rota", $id_rota, PDO::PARAM_INT);
            $stmt->bindValue(":id_situacao", $id_situacao, PDO::PARAM_INT);
            $stmt->bindValue(":id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->bindValue(":dia_avaliado", $dia_avaliado, PDO::PARAM_STR);
            $stmt->execute();
            
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function select_pedidos_rota_semanal($id_rota, $data_reparte, $situacao)
    {
        $sql = 'SELECT  pedido.id FROM log_rota_senamal 
                JOIN rota ON (log_rota_senamal.id_rota  =rota.id)
                JOIN pdv ON (rota.id = pdv.id_rota)
                JOIN pedido ON (pdv.id = pedido.id_pdv)
                JOIN item_pedido ON (pedido.id = item_pedido.id_pedido)
                WHERE log_rota_senamal.id_rota=:id_rota 
                    AND item_pedido.data_reparte=:data_reparte 
                    AND item_pedido.situacao=:situacao';
        
        $stmt = $this->db->conn_id->prepare($sql);
        $stmt->bindValue(":id_rota", $id_rota, PDO::PARAM_INT);
        $stmt->bindValue(":data_reparte", $data_reparte, PDO::PARAM_STR);
        $stmt->bindValue(":situacao", $situacao, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function add_log_rota_senamal($id_rota, $id_situacao, $id_usuario, $dia_avaliado, $reparte, $encalhe, $quantidade_fisica, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema, $data)
    {
        $sql = "INSERT INTO log_rota_senamal (id_rota, id_situacao, id_usuario, dia_avaliado,reparte, encalhe, quantidade_fisica, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES( :id_rota, :id_situacao, :id_usuario, :dia_avaliado, :reparte, :encalhe, :quantidade_fisica, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_rota', $id_rota, PDO::PARAM_INT); 
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':dia_avaliado', $dia_avaliado, PDO::PARAM_STR);
        $sth->bindValue(':reparte', $reparte, PDO::PARAM_STR);
        $sth->bindValue(':encalhe', $encalhe, PDO::PARAM_STR);
        $sth->bindValue(':quantidade_fisica', $quantidade_fisica, PDO::PARAM_STR);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agent, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', $data, PDO::PARAM_STR);
        
        return $sth->execute();
    }

    public function add_log_pedido($id_pedido, $id_situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_pedido (id_pedido, id_situacao, id_usuario, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_pedido, :id_situacao, :id_usuario, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_pedido', $id_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agent, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        return $sth->execute();
    }
    
    public function update_situacao_pedido($id, $situacao)
    {
        $sql = "UPDATE pedido SET situacao=:situacao  WHERE id=:id";
        $stmt = $this->db->conn_id->prepare($sql);
        
        $stmt->bindValue(':situacao', $situacao, PDO::PARAM_INT);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        
        if( $stmt->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function update_situacao_item_pedido($id, $situacao)
    {
        $sql = "UPDATE item_pedido SET situacao=:situacao  WHERE id=:id";
        $stmt = $this->db->conn_id->prepare($sql);
        
        $stmt->bindValue(':situacao', $situacao, PDO::PARAM_INT);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        
        if( $stmt->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }
    
    public function add_log_item_pedido($id_item_pedido, $id_usuario, $id_situacao , $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agente_string, $plataforma, $navegador, $sistema)
    {
        $sql = "INSERT INTO log_item_pedido (id_item_pedido, id_usuario, id_situacao, reparte ,encalhe, ip, localizacao, is_robot, is_mobile, agente_string, plataforma, navegador, sistema, data) "
                . "VALUES(:id_item_pedido, :id_usuario, :id_situacao, :reparte, :encalhe, :ip, :localizacao, :is_robot, :is_mobile, :agente_string, :plataforma, :navegador, :sistema, :data)";
        $sth = $this->db->conn_id->prepare($sql);
        
        $sth->bindValue(':id_item_pedido', $id_item_pedido, PDO::PARAM_INT); 
        $sth->bindValue(':id_usuario', $id_usuario, PDO::PARAM_INT);
        $sth->bindValue(':id_situacao', $id_situacao, PDO::PARAM_INT);
        $sth->bindValue(':reparte', $reparte, PDO::PARAM_STR);
        $sth->bindValue(':encalhe', $encalhe, PDO::PARAM_STR);
        $sth->bindValue(':ip', $ip, PDO::PARAM_STR);
        $sth->bindValue(':localizacao', $localizacao, PDO::PARAM_STR);
        $sth->bindValue(':is_robot', $is_robot, PDO::PARAM_BOOL);
        $sth->bindValue(':is_mobile', $is_mobile, PDO::PARAM_BOOL);
        $sth->bindValue(':agente_string', $agente_string, PDO::PARAM_STR);
        $sth->bindValue(':plataforma', $plataforma, PDO::PARAM_STR);
        $sth->bindValue(':navegador', $navegador, PDO::PARAM_STR);
        $sth->bindValue(':sistema', $sistema, PDO::PARAM_STR);
        $sth->bindValue(':data', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        
        if( $sth->execute() )
        {
            return TRUE;
        }
        return FALSE;
    }

}
