<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->seguranca();
        $this->is_supervisor();
    }
    
    private function seguranca()
    {
        if( !$this->session->has_userdata('logado') || !$this->session->userdata('logado') )
        {
            redirect('login');
        }
    }
    
    private function is_supervisor()
    {
        if($this->session->userdata("perfil")!='4')
        {
            $this->session->sess_destroy();
            redirect('pdv');
        }
     }
     
     public function home()
     {
        $id_usuario = $this->session->userdata('id_usuario');
        
        $this->load->model('Supervisor_model');
        //$select_log_rota = $this->Supervisor_model->select_log_rota($id_rota, $id_situacao, $id_usuario);
        //echo '<pre>';
        //var_dump($select_log_rota);
        //die;
        $pedidos = $this->Supervisor_model->select_pedidos_supervisor($id_usuario);
        
        $data = ['pedidos'=>$pedidos];
        
        $this->load->view('supervisor/include/head.php');
        $this->load->view('supervisor/pagina/home.php', $data);
        $this->load->view('supervisor/include/footer.php');
     }
     
     private function ajuste_dados($dados)
     {
         if (count($dados)>0 )
         {
             $firstKey = array_key_first($dados);
             $data_reparte = $dados[$firstKey]['data_reparte'];
             $data_reparte = strtotime($data_reparte);
             $data_reparte_indice = '';
             
             $encalhe = 0;
             $reparte = 0;
             
             foreach ($dados as $dado)
             {
                 $data_reparte_atual = strtotime($dado['data_reparte']);
                 
                 if($data_reparte_atual == $data_reparte)
                 {
                     $data_reparte_indice = date("Y-m-d", $data_reparte_atual);
                     
                     $reparte += (int)$dado['reparte'];
                     $encalhe += (int)$dado['encalhe'];
                     $ajuste_dados[$data_reparte_indice] = ['situacao_item_pedido'=>$dado['situacao_item_pedido'],'id_rota'=>$dado['id_rota'], 'id_item'=>$dado['id_itempedido'],'usuario'=>$dado['usuario'], 'data'=>$data_reparte_indice, 'encalhe'=>$encalhe, 'reparte'=>$reparte];
                 }
                 elseif($data_reparte_atual != $data_reparte)
                 {
                     $reparte = 0;
                     $encalhe = 0;
                     
                     $reparte += (int)$dado['reparte'];
                     $encalhe += (int)$dado['encalhe'];
                     $data_reparte_indice = date("Y-m-d", $data_reparte_atual);
                     
                     $ajuste_dados[$data_reparte_indice] = ['situacao_item_pedido'=>$dado['situacao_item_pedido'], 'id_rota'=>$dado['id_rota'], 'id_item'=>$dado['id_itempedido'], 'usuario'=>$dado['usuario'], 'data'=>$data_reparte_indice, 'encalhe'=>$encalhe,'reparte'=>$reparte];
                 }
                 
                 $data_reparte = $data_reparte_atual ;
             }
             return $ajuste_dados;
         }
     }
     
     private function pedidos_validos_rota($pedidos)
     {
         if ( count($pedidos)>0 )
         {
             $i = 0;
             $pedidos_validos = [];
             
             foreach ($pedidos as $pedido)
             {
                 //echo '<pre>';var_dump($pedido);die;
                 //$log_pedido = $this->Supervisor_model->check_log_pedido( $pedido['id_pedido'] );
                 //$log_pedido = $this->Supervisor_model->check_log_item( $pedido['id_itempedido'] );
                 
                 if( $pedido['situacao_item_pedido']==2 )
                 {
                     $pedidos_validos[$i] = $pedido;
                 }
                 
                 $i++;
             }
         }
         
         return $pedidos_validos;
     }
     
     private function get_value_week($data_week)
    {
        $data_week = explode("-", $data_week);
        
        $ano = (int)$data_week[0];
        $mes = (int)$data_week[1];
        $dia = (int)$data_week[2];
        
        $jd = gregoriantojd( $mes, $dia, $ano);
        $dia_semana = jddayofweek($jd,1);
        
        switch ($dia_semana)
        {
        case 'Sunday':
            return "Dom";
        case 'Monday':
            return "Seg";
        case 'Tuesday':
            return "Ter";
        case 'Wednesday':
            return "Quar";
        case 'Thursday':
            return "Qui";
        case 'Friday':
            return "Sex";
        case 'Saturday':
            return "Sáb";
        }
    }

     public function load_modal_supervidor()
     {
        if( $this->input->method(TRUE) )
        {
            
            $id = $this->input->post("id", TRUE);
            $name = $this->input->post("name", TRUE);
            
            $this->load->model('Supervisor_model');
            //$pdv_rota = $this->Supervisor_model->select_pdv_by_rota($id);
            //TO DO: adicionar coluna situacao nas tabelas pedido e item_pedido
            $pdv_rota = $this->Supervisor_model->select_pedido_rota_entregador($id);
            //$pdv_rota = $this->pedidos_validos_rota($pdv_rota);
            $response = '';
            
            if( count($pdv_rota)>0 )
            {
                $ajuste_dados = $this->ajuste_dados($pdv_rota);
                
                $response = '';
                
                $response .= '<div class="modal-header">
                                <h4 class="modal-title w-100 text-center">Rota '.$name.'</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="feedback"></div>
                                    </div>
                                </div>
                                </div>
                                <div class="modal-body">';
                $response .= '<div class="table-responsive">';
                $response .= "<table class='table'>";
                $response .= '<thead>
                                  <tr>
                                  <th scope="col">Data</th>
                                  <th scope="col">Encalhe</th>
                                  <th scope="col">Físico</th>
                                  <th scope="col">Validar</th>
                                </tr>
                              </thead>';
                foreach ($ajuste_dados as $item)
                {
                    $data_reparte = $item['data'];
                    $data_reparte_str = strtotime($data_reparte);
                    $data_reparte = date("d/m",$data_reparte_str);
                    
                    $week = $this->get_value_week($item['data']);

                    //$nome = $this->Supervisor_model->select_usuario($item['usuario']);
                    
                    $response .= "<tr>";
                         $response .= "<td><strong>(".$week.")</strong>"."<br>".$data_reparte."</td>";
                         $response .= "<td>".$item['encalhe']."</td>";
                         //$response .= "<td>-</td>";
                         
                         if($item['situacao_item_pedido']==3)
                         {
                             $id_usuario = (int)$this->session->userdata('id_usuario');
                             //var_dump(date("Y-m-d",$data_reparte_str));die;
                             //date("Y-m-d",$data_reparte_str)
                             $log_rota_senamal = $this->Supervisor_model->select_log_rota_senamal((int)$item['id_rota'], 1, $id_usuario, date("Y-m-d",$data_reparte_str));
                             //echo '<pre>';var_dump($log_rota_senamal[0]);die;
                             if(isset($log_rota_senamal[0]['quantidade_fisica']))
                             {
                                $response .= "<td><input type='tel' value='".$log_rota_senamal[0]['quantidade_fisica']."' class='form-control' disabled=''></td>";
                                $response .= '<td><div class="btn-group"><div class="btn-save-encalhe-36"><button type="button" class="btn btn-success"><i class="fa fa-check-circle" aria-hidden="true"></i></button></div></div></td>';
                             }
                             else
                             {
                                $response .= "<td><input type='tel' value='' class='form-control' disabled=''></td>";
                                $response .= '<td><div class="btn-group"><div class="btn-save-encalhe-36"><button type="button" class="btn btn-success"><i class="fa fa-check-circle" aria-hidden="true"></i></button></div></div></td>';
                             }
                         }
                         else
                         {
                             //$response .= "<td><div class='btn-group'><div class='btn-save-encalhe-".$item['id_item']."'><button data-id='' value='' type='button' class='btn btn-danger btn-sm btn-alert-super' >NÃO</button><button data-id='".strtotime($item['data'])."' value='".$item['id_rota']."' type='button' class='btn btn-success btn-sweet-sim btn-sm'>SIM</button></div></div></td>";
                             
                             $response .= "<td><input id='btn-save-encalhe-".$item['id_item']."' type='tel' placeholder='Quantidade do Fisica' class='input-encalhe form-control'></td>";
                             $response .= "<td><div class='btn-save-encalhe-".$item['id_item']."'><button data-reparte='".$item['reparte']."' data-encalhe='".$item['encalhe']."' data-id='".strtotime($item['data'])."' value='".$item['id_rota']."' type='button' class='btn btn-primary btn-sweet-sim'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></div></td>";
                         }

                         /*if( isset($item['encalhe']) )
                         {
                             $response .= "<td><input type='tel' value='".$item['encalhe']."' class='form-control' disabled=''></td>";
                             $response .= "<td><button style='width:100%' type='button' class='btn btn-success' ><i class='fa fa-check-circle' aria-hidden='true'></i></button></td>";
                         }
                         else
                         {
                             $response .= "<td><input id='encalhe-".$item['id']."' type='tel' placeholder='Quantidade do Encalhe' class='input-encalhe form-control'></td>";
                             $response .= "<td><div class='btn-save-encalhe-".$item['id']."'><button style='width:100%' onclick='get_value_encalhe(".'"'.$item['id']." ".'"'.")' id='id-encalhe-".$item['id']."' type='button' class='btn btn-primary'>Salvar</button></div></td>";
                         }*/
                    $response .= "</tr>";
                }

                $response .= "</table>";
                $response .= "</div>";
                $response .= '</div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-modal-super" data-dismiss="modal">Fechar</button>
                                </div>';
            }
            echo $response;
        }
     }
     
     private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    private function location()
    {
        
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN' )
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("https://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                $loc = $json['loc'];
                $cep = $json['postal'];
                $ip = $json['ip'];
                
                return 'Cidade: '.$city.'/ Regiao: '.$region.'/ Pais: '.$country.'/ Coordenadas Aprox: '.$loc.'/ CEP: '.$cep.'/ IP:'.$ip;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
     
    private function valida_pedido($id_pedido)
    {
        $itens = $this->Supervisor_model->select_validacao_item_pedido($id_pedido);
        $log = 0;
        $i = 0;
        foreach ($itens as $iten)
        {
            $id = (int)$iten['id'];
            $item_pedido = $this->Supervisor_model->check_log_item_pedido($id, 3);
            
            if( isset($item_pedido[$i]) && $item_pedido[$i]['id_situacao']==3)
            {
                $log++;
            }
            $i++;
        }
        
        if($log==count($itens) )
        {
            return TRUE;
        }
        
        return FALSE;
    }
    
    private function update_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $itens = $this->Supervisor_model->select_items_pedido_situacao($id_pedido, 2);
        $quantidade_itens = count($itens);

        if($quantidade_itens==0)
        {
            $this->Supervisor_model->update_situacao_pedido($id_pedido, $situacao);
            $this->Supervisor_model->add_log_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema);
        }
    }

    public function do_valida()
    {
         $id_rota = (int)$this->input->post("id_rota", TRUE);
         $str_data = $this->input->post("str_data", TRUE);
         $input = $this->input->post("input", TRUE);
         $reparte = $this->input->post("reparte", TRUE);
         $encalhe = $this->input->post("encalhe", TRUE);
         
         $date = date("Y-m-d",$str_data);
         
         $this->load->model('Supervisor_model');
         //$pedidos = $this->Supervisor_model->select_pedido_rota($id_rota, $date);
         
            $ip = $this->get_client_ip();
            $id_usuario = (int)$this->session->userdata('id_usuario');

            $navegador = $this->agent->browser() . ' - ' . $this->agent->version();
            $sistema = $this->agent->platform();

            $robot = $this->agent->is_robot();
            $mobile = $this->agent->is_mobile();
            $agent = $this->agent->agent_string();
            $platform = $this->agent->platform();
            
            $add_log_rota = $this->Supervisor_model->add_log_rota_senamal($id_rota, 1, $id_usuario, $date, $reparte, $encalhe, $input, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema, date("Y-m-d H:i:s") );
            
            if($add_log_rota)
            {
                $pedidos_rota_semanal = $this->Supervisor_model->select_pedidos_rota_semanal($id_rota, $date, 2);
                if( isset($pedidos_rota_semanal[0]))
                {
                    $itens = [];
                    $pedidos = [];
                    foreach ($pedidos_rota_semanal as $pedidos_rota)
                    {
                        $pedidos[] = (int)$pedidos_rota['id'];
                        $id_pedido = (int)$pedidos_rota['id'];
                        $itens[] = $this->Supervisor_model->select_items_pedido($id_pedido);
                    }
                    
                    //TO DO: CORRIGIR O FOREACH PARA QUE EEL GEERE A QUANTIDADE CORRETA DE LOG_ITEM_PEDIDO
                    //($itens);die;
                    $i = 0;
                    if( isset($itens[0]) )
                    {
                        foreach ($itens as $item)
                        {
                            foreach ($item as $iten)
                            {
                                if( isset($iten['data_reparte']) && ( date("Y-m-d", strtotime($iten['data_reparte']) )==$date) && ($iten['situacao']=='2') )
                                {
                                    $this->Supervisor_model->update_situacao_item_pedido($iten['id'], 3);
                                    $this->Supervisor_model->add_log_item_pedido($iten['id'], $id_usuario, 3, $iten['reparte'], $iten['encalhe'], $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                                    $i++;
                                }
                            }
                        }
                    }
                    
                    echo json_encode(['status'=>1,'msg'=>'Encalhe validado com Sucesso:'.$i]);
                    foreach ($pedidos as $id_pedido)
                    {
                        $this->update_pedido($id_pedido, 3, $id_usuario, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                    }
                }
                else
                {
                    echo json_encode(['status'=>2,'msg'=>'Falha na validação do Encalhe']);
                }
            }
     }
     
}