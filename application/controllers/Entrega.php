<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entrega extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->seguranca();
        $this->is_admin();
    }
    
    private function seguranca()
    {
        if( !$this->session->has_userdata('logado') || !$this->session->userdata('logado') )
        {
            redirect('login');
        }
    }
    
    private function is_admin()
    {
        if($this->session->userdata("perfil")!='1')
        {
            $this->session->sess_destroy();
            redirect('pdv');
        }
    }
    
    public function sair()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function pagina_pdv()
    {
        
        $this->load->view('entrega/include/head.php');
        
        $this->load->model('Entrega_model');
        $pontos = $this->Entrega_model->get_pontos();
        
        $data = ['pontos'=>$pontos];
        
        $this->load->view('entrega/pagina/home.php', $data);
        $this->load->view('entrega/include/footer.php');
    }
    
    public function form_entregador()
    {
        $this->load->view('entrega/include/head.php');
        
        $this->load->model('Entrega_model');
        $rotas = $this->Entrega_model->lista_rotas();
        $data = ['rotas'=>$rotas];
        
        $this->load->view('entrega/pagina/add_entregador.php',$data);
        $this->load->view('entrega/include/footer.php');
    }
    
    public function do_entregador()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[2]');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            //$this->form_validation->set_rules('ddd', 'ddd', 'trim|required|integer|min_length[2]|max_length[2]');
            //$this->form_validation->set_rules('celular', 'celular', 'trim|required|integer|min_length[8]|max_length[9]');
            $this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('rota', 'rota', 'trim|required|integer');
            
            
            if ( $this->form_validation->run() )
            {
                $nome = $this->input->post("nome", TRUE);
                $nome_entregador = filter_var($nome, FILTER_SANITIZE_STRING);
                
                $email = $this->input->post("email", TRUE);
                $email = filter_var($email, FILTER_SANITIZE_STRING);
                
                $ddd = $this->input->post("ddd", TRUE);
                $ddd = filter_var($ddd, FILTER_SANITIZE_STRING);
                
                $celular = $this->input->post("celular", TRUE);
                $celular = filter_var($celular, FILTER_SANITIZE_STRING);
                
                $senha = $this->input->post("senha", TRUE);
                $senha = filter_var($senha, FILTER_SANITIZE_STRING);
                
                $rota = $this->input->post("rota", TRUE);
                $id_rota = filter_var($rota, FILTER_SANITIZE_STRING);
                
                $this->load->library('encryption');
                $this->encryption->initialize(array('driver' => 'openssl'));
                    
                $encrypt_senha = $this->encryption->encrypt($senha);
                
                $this->load->model('Entrega_model');
                $id_usuario = $this->Entrega_model->add_usuario(2, $nome_entregador, $ddd, $celular, $email, $encrypt_senha, date("Y-m-d H:i:s") );
                //echo '<pre>';var_dump($id_usuario);die;
                if( isset($id_usuario) )
                {
                     $is_add = $this->Entrega_model->add_usuario_rota($id_usuario, $id_rota);
                     
                     if($is_add)
                     {
                         $this->session->set_flashdata('sucesso_cad_entregador','<div class="alert alert-success" alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <strong>Parabéns!</strong><br> Entregador Cadastrado com Sucesso.
                                    </div>');

                            redirect("pdv");
                     }
                }
            }
            $this->session->set_flashdata('cad_entregador_erro','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Falha no Cadastro!</strong><br> verifique as Informações digitadas.
                            </div>');
            redirect('add_entregador');
        
        }
    }

    public function form_rota()
    {
        $this->load->view('entrega/include/head.php');
        
        $this->load->model('Entrega_model');
        $areas = $this->Entrega_model->get_areas();
        $data = ['areas'=>$areas];
        
        $this->load->view('entrega/pagina/add_rota.php',$data);
        $this->load->view('entrega/include/footer.php');
    }
    
    public function do_rota()
    {
        
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('rota', 'rota', 'trim|required|min_length[2]');
            $this->form_validation->set_rules('area', 'area', 'trim|required|integer');
            
            if ( $this->form_validation->run() )
            {
                
                $rota = $this->input->post("rota", TRUE);
                $nome_rota = filter_var($rota, FILTER_SANITIZE_STRING);
                
                $area = $this->input->post("area", TRUE);
                $id_area = filter_var($area, FILTER_SANITIZE_STRING);
                
                $this->load->model('Entrega_model');
                $id_rota = $this->Entrega_model->add_rota($nome_rota);
                
                if( isset($id_area) && isset($id_rota) )
                {
                    $is_add = $this->Entrega_model->add_area_rota($id_area, $id_rota);
                    
                    if($is_add)
                    {
                        $this->session->set_flashdata('sucesso_cad_rota','<div class="alert alert-success" alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <strong>Parabéns!</strong><br> Área Cadastrada com Sucesso.
                                    </div>');

                            redirect("pdv");
                    }
                }
            }
            $this->session->set_flashdata('cad_rota_erro','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Falha no Cadastro!</strong><br> verifique o Nome digitado.
                            </div>');
            redirect('area'); 
        
        }
    }

    public function form_area()
    {
        $this->load->view('entrega/include/head.php');
        $this->load->view('entrega/pagina/add_area.php');
        $this->load->view('entrega/include/footer.php');
    }
    
    public function do_area()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('area', 'area', 'trim|required|min_length[2]');
            
            if ( $this->form_validation->run() )
            {
                $area = $this->input->post("area", TRUE);
                $nome_area = filter_var($area, FILTER_SANITIZE_STRING);
                
                $this->load->model('Entrega_model');
                $is_add = $this->Entrega_model->add_area($nome_area);
                
                if($is_add)
                {
                    $this->session->set_flashdata('sucesso_cad_area','<div class="alert alert-success" alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <strong>Parabéns!</strong><br> Área Cadastrada com Sucesso.
                                </div>');

                        redirect("pdv");
                }
            }
            $this->session->set_flashdata('cad_area_erro','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Falha no Cadastro!</strong><br> verifique o Nome digitado.
                            </div>');
            redirect('area');
        
        }
    }

    public function form_pdv()
    {
        if($this->session->userdata("perfil")!='1')
        {
            redirect('entrega');
        }
        
        $this->load->view('entrega/include/head.php');
        
        $this->load->model('Entrega_model');
        $areas = $this->Entrega_model->get_areas();
        $area_rota = $this->lista_area_rota();
        
        $data = ['areas'=>$areas,'area_rota'=>$area_rota];
        
        $this->load->view('entrega/pagina/add_ponto.php', $data);
        $this->load->view('entrega/include/footer.php');
    }
    
    public function add_ponto()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('ponto', 'ponto', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[2]');
            $this->form_validation->set_rules('ddd', 'ddd', 'trim|required|integer|min_length[2]|max_length[2]');
            $this->form_validation->set_rules('celular', 'celular', 'trim|required|integer|min_length[9]|max_length[11]');
            $this->form_validation->set_rules('cep', 'cep', 'trim|required|integer|min_length[8]|max_length[8]');
            $this->form_validation->set_rules('endereco', 'endereco', 'trim|required');
            $this->form_validation->set_rules('numero', 'numero', 'trim|required');
            $this->form_validation->set_rules('complemento', 'complemento', 'trim|required');
            $this->form_validation->set_rules('id_rota', 'id_rota', 'trim|required|integer');
            
            if ( $this->form_validation->run() )
            {
                if($this->session->userdata("perfil")=='1')
                {
                    
                    $ponto = $this->input->post("ponto", TRUE);
                    $ponto = filter_var($ponto, FILTER_SANITIZE_STRING);
                    
                    $nome = $this->input->post("nome", TRUE);
                    $nome = filter_var($nome, FILTER_SANITIZE_STRING);
                    
                    $ddd = $this->input->post("ddd", TRUE);
                    $ddd = filter_var($ddd, FILTER_SANITIZE_STRING);
                    
                    $celular = $this->input->post("celular", TRUE);
                    $celular = filter_var($celular, FILTER_SANITIZE_STRING);
                    
                    $cep = $this->input->post("cep", TRUE);
                    $cep = filter_var($cep, FILTER_SANITIZE_STRING);
                    
                    $endereco = $this->input->post("endereco", TRUE);
                    $endereco = filter_var($endereco, FILTER_SANITIZE_STRING);
                    
                    $numero = $this->input->post("numero", TRUE);
                    $numero = filter_var($numero, FILTER_SANITIZE_STRING);
                    
                    $complemento = $this->input->post("complemento", TRUE);
                    $complemento = filter_var($complemento, FILTER_SANITIZE_STRING);
                    
                    $id_rota = $this->input->post("id_rota", TRUE);
                    $id_rota = filter_var($id_rota, FILTER_SANITIZE_STRING);
                    
                    $dados_pdv = [
                                'id_rota'=>(int)$id_rota,
                                'nome_pdv'=>$ponto,
                                'funcionario'=>$nome,
                                'ddd'=>(int)$ddd,
                                'telefone'=>(int)$celular,
                                'cep'=>$cep,
                                'endereco'=>$endereco,
                                'numero'=>$numero,
                                'complemento'=>$complemento,
                                'data_cadastro'=>date("Y-m-d H:i:s")
                             ];
                    
                    $this->load->model('Entrega_model');
                    $id_pdv = $this->Entrega_model->add_ponto($dados_pdv);
                    
                    if( isset($id_pdv) )
                    {
                        $this->session->set_flashdata('sucesso_cad_ponto','<div class="alert alert-success" alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <strong>Parabéns!</strong><br> PDV Cadastrado com Sucesso.
                                </div>');

                        redirect("pdv");
                    }
                }
            }
        
        }
        $this->session->set_flashdata('cad_ponto_erro','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Falha no Cadastro!</strong><br> verifique seus dados novamente.
                            </div>');
        redirect('form_pdv');
    }
    
    public function pagina_edit_pdv($id_ponto=NULL)
    {
        if( isset($id_ponto)  )
        {
            $this->is_admin();
        
            $this->load->model('Entrega_model');
            $ponto = $this->Entrega_model->get_ponto_id($id_ponto);
            //$area_ponto = $this->Entrega_model->get_area_ponto_idponto($id_ponto);
            $this->load->model('Entrega_model');
            //$areas = $this->Entrega_model->get_areas();
            $area_rota = $this->lista_area_rota();
            $data = ['ponto'=>$ponto[0], 'area_rota'=>$area_rota];
            
            $this->session->set_flashdata('id_ponto_session',$id_ponto);
            
            $this->load->view('entrega/include/head.php');
            $this->load->view('entrega/pagina/edit_ponto.php', $data);
            $this->load->view('entrega/include/footer.php');
        }
        else
        {
            redirect("entrega");
        }
    }
    
    public function edit_ponto()
    {
        if ($this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'] . '/' === base_url() )) 
        {
            if($this->session->userdata("perfil")!='1')
            {
                redirect('entrega');
            }
            $this->load->library("form_validation");
            $this->form_validation->set_rules('ponto', 'ponto', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('nome', 'nome', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('ddd', 'ddd', 'trim|required|min_length[2]|max_length[2]');
            $this->form_validation->set_rules('celular', 'celular', 'trim|required|integer|min_length[9]|max_length[10]');
            $this->form_validation->set_rules('cep', 'cep', 'trim|required|integer|min_length[8]|max_length[8]');
            $this->form_validation->set_rules('endereco', 'endereco', 'trim|required');
            $this->form_validation->set_rules('numero', 'numero', 'trim|required');
            $this->form_validation->set_rules('id_rota', 'id_rota', 'trim|required|integer');
            
            if ($this->form_validation->run())
            {
                if ($this->session->userdata("perfil") == '1') 
                {
                    //echo '<pre>';var_dump($this->input->post());die;
                    $ponto = $this->input->post("ponto", TRUE);
                    $ponto = filter_var($ponto, FILTER_SANITIZE_STRING);

                    $nome = $this->input->post("nome", TRUE);
                    $nome = filter_var($nome, FILTER_SANITIZE_STRING);
                    
                    $ddd = $this->input->post("ddd", TRUE);
                    $ddd = filter_var($ddd, FILTER_SANITIZE_STRING);
                    
                    $celular = $this->input->post("celular", TRUE);
                    $celular = filter_var($celular, FILTER_SANITIZE_STRING);
                    
                    $cep = $this->input->post("cep", TRUE);
                    $cep = filter_var($cep, FILTER_SANITIZE_STRING);

                    $endereco = $this->input->post("endereco", TRUE);
                    $endereco = filter_var($endereco, FILTER_SANITIZE_STRING);

                    $numero = $this->input->post("numero", TRUE);
                    $numero = filter_var($numero, FILTER_SANITIZE_STRING);
                    
                    $complemento = $this->input->post("complemento", TRUE);
                    $complemento = filter_var($complemento, FILTER_SANITIZE_STRING);

                    $id_rota = $this->input->post("id_rota", TRUE);
                    $id_rota = filter_var($id_rota, FILTER_SANITIZE_STRING);
                    
                    $id_pdv = $this->input->post("id_pdv", TRUE);
                    $id_pdv = filter_var($id_pdv, FILTER_SANITIZE_STRING);
                    
                    $dados_pdv = [
                                'id_pdv'=> $id_pdv,
                                'id_rota'=>(int)$id_rota,
                                'nome_pdv'=>$ponto,
                                'funcionario'=>$nome,
                                'ddd'=>(int)$ddd,
                                'telefone'=>(int)$celular,
                                'cep'=>$cep,
                                'endereco'=>$endereco,
                                'numero'=>$numero,
                                'complemento'=>$complemento,
                                'data_edicao'=>date("Y-m-d H:i:s")
                             ];
                    
                    $this->load->model('Entrega_model');
                    $is_update = $this->Entrega_model->edit_ponto($dados_pdv);
                    
                    if($is_update)
                    {
                        unset($_SESSION['id_ponto_session']);

                        $this->session->set_flashdata('sucesso_edit_ponto','<div class="alert alert-success" alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <strong>Parabéns!</strong><br> Ponto de Entrega Editado com Sucesso.
                                </div>');
                        redirect("pdv");
                    }
                }
            }
            $this->session->set_flashdata('erro_edit_ponto','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Verifique os Dados Preenchidos!</strong><br> Não foi possível realizar a Edição do Ponto de Entrega.
                            </div>');
            redirect("pagina_edit_ponto");
        }
        
    }
    

    public function pagina_entrega($id_ponto = NULL)
    {
        if( isset($id_ponto)  )
        {
            $this->is_admin();

            $this->load->model('Entrega_model');
            $usuarios = $this->Entrega_model->get_user_entregador();

            $produtos = $this->Entrega_model->get_data_produtos();
            
            $data_ponto = $this->Entrega_model->get_ponto_id($id_ponto);
            
            $data = ['usuarios'=>$usuarios,'produtos'=>$produtos, 'id_ponto'=>$id_ponto,'nome_ponto'=>$data_ponto[0]['nome_ponto']];

            $this->load->view('entrega/include/head.php');
            $this->load->view('entrega/pagina/pagina_entrega.php', $data);
            $this->load->view('entrega/include/footer.php');
        }
        else
        {
            redirect("entrega");
        }
    }
    
    public function do_entrega()
    {
        if ($this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'] . '/' === base_url() )) 
        {
            $this->is_admin();
            
            $this->load->library("form_validation");
            $this->form_validation->set_rules('data_inicio', 'data_inicio', 'trim|required');
            $this->form_validation->set_rules('data_fim', 'data_fim', 'trim|required');
            $this->form_validation->set_rules('entregador', 'entregador', 'trim|required|integer');
            
            if ( $this->form_validation->run() )
            {
                $data_inicio = $this->input->post("data_inicio", TRUE);
                $data_inicio = filter_var($data_inicio, FILTER_SANITIZE_STRING);
                
                $data_fim = $this->input->post("data_fim", TRUE);
                $data_fim = filter_var($data_fim, FILTER_SANITIZE_STRING);
                
                $id_entregador = $this->input->post("entregador", TRUE);
                $id_entregador = filter_var($id_entregador, FILTER_SANITIZE_STRING);
                
                $data_inicio = strtotime($data_inicio);
                $data_fim = strtotime($data_fim);
                
                $dados_entrega = [
                                    'id_ponto'=>$_POST['id_ponto'],
                                    'id_rota'=>1,
                                    'data_entrega'=>date('Y-m-d', $data_inicio),
                                    'data_entrega_final'=>date('Y-m-d', $data_fim)
                                 ];
                                 
                $this->load->model('Entrega_model');
                $id_add_entrega = $this->Entrega_model->add_entrega($dados_entrega);
                
                if( isset($id_add_entrega) )
                {
                    
                    $data_produtos = $this->Entrega_model->get_data_produtos();
                    
                    $i = 0;
                    foreach ($_POST['produtos'] as $products)
                    {
                        $products = filter_var($products, FILTER_SANITIZE_STRING);

                        if( $data_produtos[$i]['nome']== $products)
                        {
                            $this->Entrega_model->add_item_entrega($id_add_entrega, $data_produtos[0]['id']);
                        }
                        $i++;
                    }
                     
                }
                
            }
            redirect('entrega');
        }
        redirect( $_SERVER['HTTP_REFERER'] );
        
    }
    
    private function lista_area_rota()
    {
        $this->is_admin();

        $this->load->model('Entrega_model');
        $area_rota = $this->Entrega_model->lista_area_rota();
        return $area_rota;
    }
    
    private function ajuste_data_pedido($pedidos)
    {
        $i = 0;
        foreach ($pedidos as $pedido)
        {
            $usuario = $this->Entrega_model->get_data_usuario($pedido['id_usuario_reparte']);
            $produto = $this->Entrega_model->get_produto_by_id($pedido['id_produto']);
            
            $pedidos[$i]['id_usuario_reparte'] = $usuario[0]['nome'];
            $pedidos[$i]['id_produto'] = $produto[0]['nome'];
            $i++;
        }
        
        return $pedidos;
    }
    
    public function save_percentual()
    {
        $id_pdv = $this->input->post("id_pdv", TRUE);
        $dias = $this->input->post("dias", TRUE);
        
        $this->load->model('Entrega_model');
        
        $is_update = [];
        foreach ($dias as $dia)
        {
            $id_pdv = (int)$id_pdv;
            $percentual = (int)$dia;
            $dia_semana = key($dias);
            
            $is_update[] = $this->Entrega_model->update_percentual_pdv($id_pdv, $dia_semana, $percentual);
            next($dias);
        }
        
        var_dump($is_update);
    }

    public function load_modal_percentual()
    {
        $id_pdv = $this->input->post("id_pdv", TRUE);
        
        $this->load->model('Entrega_model');
        $data_percentual = $this->Entrega_model->get_modal_percentual_pdv($id_pdv);
        
        if ( isset($data_percentual[0]))
        {
            echo '<div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Dia</th>
                                    <th scope="col">Percentual(%)</th>
                                </tr>
                            </thead>
                            <tbody>';
            foreach ($data_percentual as $data)
            {
                echo '<tr>
                        <td>'.$data['nome'].'</td>
                        <td><input requiredd="" value="'.$data['percentual'].'" minlength="1" maxlength="2" class="form-control" type="tel" name="" id="'.$data['nome'].'"></td>
                      </tr>';
            }
            echo '</tbody>
                        </table>
                    </div>';
        }
    }

    public function load_modal()
    {
        $id = $this->input->post("id", TRUE);
        $this->load->model('Entrega_model');
        $item_pedido = $this->Entrega_model->get_data_item_pedido($id);
        
        $response = '';
        $response .= "<table class='table'>";
        $response .= '<thead>
                          <tr>
                          <th scope="col">Data</th>
                          <th scope="col">Edição</th>
                          <th scope="col">Reparte</th>
                          <th scope="col">Valor</th>
                        </tr>
                      </thead>';
        foreach ($item_pedido as $item)
        {
            $data_reparte = $item['data_reparte'];
            $data_reparte = strtotime($data_reparte);
            $data_reparte = date("d/m/Y",$data_reparte);
            
            $valor = str_replace(".", ",", $item['valor']);
            if($item['valor']==3)
            {
                $valor = '3,00';
            }
            
            $response .= "<tr>";
            $response .= "<td>".$data_reparte." </td><td>".$item['edicao']." </td><td>".$item['reparte']."</td><td>R$".$valor."</td>";
            
            $response .= "</tr>";
        }
        
        $response .= "</table>";
        echo $response;

    }

    public function detalhe_pdv($id_pdv)
    {
        $this->is_admin();
        
        $this->load->model('Entrega_model');
        
        $data_pdv = $this->Entrega_model->get_ponto_id($id_pdv);
        $usuario_rota = $this->Entrega_model->get_usuario_rota_id($data_pdv[0]['id_rota']);
        
        $data_entregador = [];
        foreach ($usuario_rota as $usuario)
        {
            $data_entregador[] = $this->Entrega_model->get_user_entregador($usuario['id_usuario']);
        }
        //echo '<pre>';var_dump($data_entregador);die;
        
        $produtos = $this->Entrega_model->get_data_produtos();
        
        $pedidos = $this->Entrega_model->get_data_pedido($id_pdv);
        $ajuste = $this->ajuste_data_pedido($pedidos);
        
        $data = ['data_pdv'=>$data_pdv, 'produtos'=>$produtos,'id_pdv'=>$id_pdv,'entregador'=>$data_entregador, 'pedidos'=>$ajuste];
        
        $this->load->view('entrega/include/head.php');
        $this->load->view('entrega/pagina/detalhe_pdv.php', $data);
        $this->load->view('entrega/include/footer.php');
    }
    
    public function get_value_week()
    {
        $data_week = $this->input->post('data_week');
        $data_week = explode("/", $data_week);
        
        $dia = (int)$data_week[0];
        $mes = (int)$data_week[1];
        $ano = (int)$data_week[2];
        
        $jd = gregoriantojd( $mes, $dia, $ano);
        $dia_semana = jddayofweek($jd,1);
        //var_dump($dia_semana);die;
        if($dia_semana=='Sunday')
        {
           echo 3;
        }
        else
        {
           echo 1.25;
        }
    }
    public function build_week()
    {
        $this->is_admin();
        
        $startDate = $this->input->post('startDate', TRUE);
        $endDate = $this->input->post('endDate', TRUE);
        
        $dateStart = implode('-', array_reverse(explode('/', substr($startDate, 0, 10)))).substr($startDate, 10);
        $dateStart = new DateTime($dateStart);
        
        $dateEnd = implode('-', array_reverse(explode('/', substr($endDate, 0, 10)))).substr($endDate, 10);
        $dateEnd = new DateTime($dateEnd);

        //$startDate = explode("/", $startDate);
        $endDate = explode("/", $endDate);
        
        $week = '';
        
        echo '<div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2">
                            <div class="form-group">
                                <label for="dia"><strong>Data</strong></label>
                            </div> 
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="edicao"><strong>Edição</strong></label>
                            </div> 
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                           <div class="form-group">
                                <label for="quantidade"><strong>Reparte</strong></label>
                            </div> 
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2">
                           <div class="form-group">
                                <label for="peso"><strong>Peso(KG)</strong></label>
                            </div> 
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2">
                          <div class="form-group">
                                <label for="preco"><strong>Preço(R$)</strong></label>
                            </div>  
                        </div>
                    </div>';
        
        $increment_name_input = 0;
        //for($i=$dia_startDate; $i<=$dia_endDate; $i++)
        $dateRange = array();
        while($dateStart <= $dateEnd){
            
            
            $data = $dateStart->format('Y-m-d');
            $data = explode("-", $data);
            
            $dia = (int)$data[2];;
            $mes = (int)$data[1];
            $ano = (int)$data[0];

            $jd = gregoriantojd( $mes, $dia, $ano);
            $dia_semana = jddayofweek($jd,1);
            
            if( ($dia_semana=='Sunday') )
            {
                $preco = 3;
            }
            else
            {
                $preco = 1.25;
                
            }
        
        $week .= '<div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2">
                            <div class="form-group">
                                <label for="dia">'.$dateStart->format('d/m/Y').'</label>
                                <input type="hidden"  name="data['.$increment_name_input.']" value="'.$dateStart->format('Y-m-d').'">
                            </div> 
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <input required="" id="edicao" type="text" class="form-control" id="email" placeholder="Digite a Edição" name="edicao['.$increment_name_input.']">
                            </div> 
                        </div>
                        <div class="col-sm-3 col-md-3 col-lg-3">
                           <div class="form-group">
                                <input type="tel" required="" class="form-control" id="reparte" placeholder="Quantidade" name="quantidade['.$increment_name_input.']">
                            </div> 
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2">
                           <div class="form-group">
                                <input required=""  onchange="get_peso('."'".'peso'.$increment_name_input."'".')" type="text" class="form-control" id="peso'.$increment_name_input.'" placeholder="Peso(KG)" name="peso['.$increment_name_input.']">
                            </div> 
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2">
                          <div class="form-group">
                                <input type="tel" class="form-control" id="preco" placeholder="Digite o Preço" name="preco['.$increment_name_input.']" value="'.$preco.'">
                            </div>  
                        </div>
                    </div>'; 
        
            $increment_name_input++;
            $dateStart = $dateStart->modify('+1day');
        }
        //$week .=  '<div class="form-group">
        //                <label for="total_peso">Peso Total(KG):</label>
        //                <input type="tel" class="form-control" required="" id="total_peso" placeholder="Digite o Peso" name="total_peso">
        //            </div>';
        echo $week;
    
    }
    
    private function location()
    {
        
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN' )
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("https://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                $loc = $json['loc'];
                $cep = $json['postal'];
                $ip = $json['ip'];
                
                return 'Cidade: '.$city.'/ Regiao: '.$region.'/ Pais: '.$country.'/ Coordenadas Aprox: '.$loc.'/ CEP: '.$cep.'/ IP:'.$ip;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
    
    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    private function add_log_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $this->Entrega_model->add_log_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema);
    }
    
    private function add_log_item_pedido($id_item_pedido, $situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $this->Entrega_model->add_log_item_pedido($id_item_pedido, $situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema );
    }
    
    public function do_pedido()
    {
        if ($this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'] . '/' === base_url() )) 
        {
            $this->is_admin();
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules('id_pdv', 'id_pdv', 'trim|required');
            $this->form_validation->set_rules('entregador', 'entregador', 'trim|required');
            $this->form_validation->set_rules('produto', 'produto', 'trim|required');
            
            if ( $this->form_validation->run() )
            {
                $id_pdv = $this->input->post("id_pdv", TRUE);
                $id_pdv = filter_var($id_pdv, FILTER_SANITIZE_STRING);
                
                $entregador = $this->input->post("entregador", TRUE);
                $entregador = filter_var($entregador, FILTER_SANITIZE_STRING);
                
                $produto = $this->input->post("produto", TRUE);
                $produto = filter_var($produto, FILTER_SANITIZE_STRING);
                
                $total_peso = 0;
                $data = $this->input->post();
                for ($i = 0; $i<count($data['peso']); $i++)
                {
                    $total_peso += (int)$data['peso'][$i];
                }
                
                $dados_pedido = [
                                    'id_pdv'=>$id_pdv,
                                    'id_usuario_reparte'=>$entregador,
                                    'total_peso'=>$total_peso,
                                    'data_cadastro'=>date('Y-m-d H:i:s'),
                                    'situacao'=>1
                                 ];
                                 
                $this->load->model('Entrega_model');
                $id_pedido = $this->Entrega_model->add_pedido($dados_pedido);
                
                $id_usuario = $this->session->userdata('id_usuario');
                $ip = $this->get_client_ip();
                $navegador = $this->agent->browser() . ' - ' . $this->agent->version();
                $sistema = $this->agent->platform();

                $robot = $this->agent->is_robot();
                $mobile = $this->agent->is_mobile();
                $agent = $this->agent->agent_string();
                $platform = $this->agent->platform();
                
                $this->add_log_pedido($id_pedido, 1, $id_usuario, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                
                if( isset($id_pedido) )
                {
                    for ($i = 0; $i<count($data['edicao']); $i++)
                    {
                        $id_item_pedido = $this->Entrega_model->add_item_entrega((int)$id_pedido, (int)$produto, $data['quantidade'][$i], $data['edicao'][$i], doubleval($data['preco'][$i]), doubleval($data['peso'][$i]), $data['data'][$i], 1 );
                        $this->add_log_item_pedido($id_item_pedido, 1, $id_usuario, $data['quantidade'][$i], NULL, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                    }
                }
                redirect( $_SERVER['HTTP_REFERER'] );
            }
            else
            {
                redirect( $_SERVER['HTTP_REFERER'] );
            }
            
        }
    }
}
