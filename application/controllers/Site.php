<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->helper('url');
    }
    
    public function index()
    {
        $this->load->view('site/include/head.php');
        $this->load->view('site/pagina/home');
        $this->load->view('site/include/footer.php');
    }
    
    public function pagina_login()
    {
        $this->load->view('site/include/head.php');
        $this->load->view('site/pagina/login.php');
        $this->load->view('site/include/footer.php');
    }
    
    private function verifica_login($email)
    {
        $this->load->model('Entrega_model');
        
        return $this->Entrega_model->verifica_login($email);
    }
    
    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    private function location()
    {
        
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN' )
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("https://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            //var_dump($json);die;
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                $loc = $json['loc'];
                $cep = $json['postal'];
                $ip = $json['ip'];
                
                return 'Cidade: '.$city.'/ Regiao: '.$region.'/ Pais: '.$country.'/ Coordenadas Aprox: '.$loc.'/ CEP: '.$cep.'/ IP:'.$ip;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
    
    private function autoria_acesso($session_id, $id_usuario)
    {
        $ip = $this->get_client_ip();

        $audit = array(
                'session_id' => $session_id, 
                'id_usuario' => $id_usuario,  
                'ip' => ($ip!='UNKNOWN') ? $ip : getenv("REMOTE_ADDR"), 
                'localizacao'=> $this->location(),
                'is_robot'=> $this->agent->is_robot(),
                'is_mobile'=> $this->agent->is_mobile(),
                'agent_string'=> $this->agent->agent_string(),
                'plataforma'=>$this->agent->platform(),
                'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 
                'sistema' => $this->agent->platform(),
                'data_acesso' => date("Y-m-d H:i:s")
                );
        
        return $audit;
    }
    
    public function do_login()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[4]');
        
            if ( $this->form_validation->run() )
            {
                $email = $this->input->post("email", TRUE);
                $email = trim($email);
                $email = strip_tags($email);
                $email = filter_var($email, FILTER_SANITIZE_STRING);
                $email = str_replace(['""','WHERE','1=1', '%', '`','´', '*',';','/'], '', $email);

                $senha = $this->input->post('senha', TRUE);
                $senha = trim($senha);
                $senha = strip_tags($senha);
                $senha = filter_var($senha, FILTER_SANITIZE_STRING);
                $senha = str_replace(['""','WHERE','1=1', '%', '`','´', '*',';','/'], '', $senha);
                
                $user = $this->verifica_login($email);
                if( isset($user[0]) && count($user[0])>0 )
                {
                    $this->load->library('encryption');
                    $this->encryption->initialize(array('driver' => 'openssl'));
                    
                    $decrypt_senha = $this->encryption->decrypt($user[0]['senha']);
                    
                    if($decrypt_senha===$senha)
                    {
                        if($user[0]['perfil_id']==1)
                        {
                        
                            $session_id = md5(rand(date('m'), date('s'))) ;

                            $audit = $this->autoria_acesso($session_id, $user[0]['id']);
                            $id_acesso = $this->Entrega_model->aditoria_acesso($audit);

                            $session = [
                                        'id_usuario' => $user[0]['id'],
                                        'perfil'=>$user[0]['perfil_id'],
                                        'email' => $user[0]['email'],
                                        'id_autoria_acesso'=>$id_acesso,
                                        'logado'=>TRUE
                                        ];

                            $this->session->set_userdata($session);
                            redirect("pdv");
                        }
                        elseif($user[0]['perfil_id']==2)
                        {
                            $session_id = md5(rand(date('m'), date('s'))) ;

                            $audit = $this->autoria_acesso($session_id, $user[0]['id']);
                            $id_acesso = $this->Entrega_model->aditoria_acesso($audit);

                            $session = [
                                        'id_usuario' => $user[0]['id'],
                                        'perfil'=>$user[0]['perfil_id'],
                                        'email' => $user[0]['email'],
                                        'id_autoria_acesso'=>$id_acesso,
                                        'logado'=>TRUE
                                        ];

                            $this->session->set_userdata($session);
                            redirect("entrega");
                        }
                        elseif($user[0]['perfil_id']==4)
                        {
                            $session_id = md5(rand(date('m'), date('s'))) ;

                            $audit = $this->autoria_acesso($session_id, $user[0]['id']);
                            $id_acesso = $this->Entrega_model->aditoria_acesso($audit);

                            $session = [
                                        'id_usuario' => $user[0]['id'],
                                        'perfil'=>$user[0]['perfil_id'],
                                        'email' => $user[0]['email'],
                                        'id_autoria_acesso'=>$id_acesso,
                                        'logado'=>TRUE
                                        ];

                            $this->session->set_userdata($session);
                            redirect("supervisor");
                        }
                    }
                }
            }
        }
        $this->session->set_flashdata('login_erro','<div class="alert alert-warning" alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <strong>Falha no Login!</strong><br> Dados incorretos, digite seu login e senha novamente.
                            </div>');
        redirect( $_SERVER['HTTP_REFERER'] );

    }
    
}
?>