<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entregador extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->seguranca();
        $this->is_entregador();
    }
    
    private function seguranca()
    {
        if( !$this->session->has_userdata('logado') || !$this->session->userdata('logado') )
        {
            redirect('login');
        }
    }
    
    private function is_entregador()
    {
        if($this->session->userdata("perfil")!='2')
        {
            $this->session->sess_destroy();
            redirect('pdv');
        }
    }

    private function ajuste_pedido_entregador($pedidos)
    {
        $i = 0;
        foreach ($pedidos as $pedido)
        {
            $itens = $this->Entregador_model->selec_itens_pedido($pedido['id_pedido']);
            
            $pedidos[$i]['total_itens_reparte'] = 0;
            foreach ($itens as $item)
            {
                $pedidos[$i]['total_itens_reparte'] += (int)$item['reparte'];
                
                $log_pedido = $this->Entregador_model->select_situacao_log_pedido( $item['id_pedido'] );
                
                $id_situacao = (int)$log_pedido[0]['id_situacao'];
                
                $pedidos[$i]['log_situacao'] = FALSE;
                if($id_situacao==2)
                {
                    $pedidos[$i]['log_situacao'] = TRUE;
                }
            }
            
            $pedidos[$i]['itens'] = $itens;
            
            $i++;
        }
        
        return $pedidos;
    }
    
    private function get_value_week($data_week)
    {
        $data_week = explode("-", $data_week);
        
        $ano = (int)$data_week[0];
        $mes = (int)$data_week[1];
        $dia = (int)$data_week[2];
        
        $jd = gregoriantojd( $mes, $dia, $ano);
        $dia_semana = jddayofweek($jd,1);
        
        switch ($dia_semana)
        {
        case 'Sunday':
            return "Dom";
        case 'Monday':
            return "Seg";
        case 'Tuesday':
            return "Ter";
        case 'Wednesday':
            return "Quar";
        case 'Thursday':
            return "Qui";
        case 'Friday':
            return "Sex";
        case 'Saturday':
            return "Sáb";
        }
    }
    
    public function load_modal_entregador()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            
            $id = $this->input->post("id", TRUE);
            $name = $this->input->post("name", TRUE);
            
            $this->load->model('Entregador_model');

            $itens = $this->Entregador_model->selec_itens_pedido($id);
            $response = '';
            $response .='<div class="modal-header">
                            <h4 class="modal-title w-100 text-center"> '.$name.'</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="feedback"></div>
                            </div>
                        </div>
                        </div>
                        <div class="modal-body">';

            $response .= '<div class="table-responsive">';
            $response .= "<table class='table'>";
            $response .= '<thead>
                              <tr>
                              <th scope="col">Data</th>
                              <th scope="col">Reparte</th>
                              <th scope="col">Encalhe</th>
                              <th scope="col">Ação</th>
                            </tr>
                          </thead>';
            foreach ($itens as $item)
            {
                $data_reparte = $item['data_reparte'];
                $data_reparte = strtotime($data_reparte);
                $data_reparte = date("d/m",$data_reparte);

                //$dia_semana = $this->get_value_week($item['data_reparte']);

                $response .= "<tr>";
                     $response .= "<td>".$data_reparte."</td>";
                     $response .= "<td id>".$item['reparte']."</td>";
                     if( isset($item['encalhe']) )
                     {
                         $response .= "<td><input type='tel' value='".$item['encalhe']."' class='form-control' disabled=''></td>";
                         $response .= "<td><button style='width:100%' type='button' class='btn btn-success' ><i class='fa fa-check-circle' aria-hidden='true'></i></button></td>";
                     }
                     else
                     {
                         $response .= "<td><input id='encalhe-".$item['id']."' type='tel' placeholder='Quantidade do Encalhe' class='input-encalhe form-control'></td>";
                         $response .= "<td><div class='btn-save-encalhe-".$item['id']."'><button style='width:100%' onclick='get_value_encalhe(".'"'.$item['id']." ".'"'.")' id='id-encalhe-".$item['id']."' type='button' class='btn btn-primary'>Salvar</button></div></td>";
                     }
                $response .= "</tr>";
            }

            $response .= "</table>";
            $response .= "</div>";
            
            $response .='</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-modal-entregador" data-dismiss="modal">Fechar</button>
                        </div>';
            
            echo $response;
        }
    }
    
    private function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
    private function location()
    {
        
        $ip = $this->get_client_ip(); 
        if($ip=='UNKNOWN' )
        {
            return $ip;
        }
        else
        {
            $json  = file_get_contents("https://ipinfo.io/$ip/geo");
            $json  =  json_decode($json ,true);
            
            if( isset($json['country']) && isset($json['region']) && isset($json['city']) )
            {
                $country =  $json['country'];
                $region= $json['region'];
                $city = $json['city'];
                $loc = $json['loc'];
                $cep = $json['postal'];
                $ip = $json['ip'];
                
                return 'Cidade: '.$city.'/ Regiao: '.$region.'/ Pais: '.$country.'/ Coordenadas Aprox: '.$loc.'/ CEP: '.$cep.'/ IP:'.$ip;
            }
            else
            {
                return "UNKNOWN";
            }
        }
    }
    
    private function add_log_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        $pedido_null = $this->Entregador_model->select_encalhe_pedido_null($id_pedido);
        $quantidade_null = count($pedido_null);

        if($quantidade_null==0)
        {
            $this->Entregador_model->update_usuario_encalhe($id_pedido, $id_usuario, $situacao);
            return $this->Entregador_model->add_log_pedido($id_pedido, $situacao, $id_usuario, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema);
        }
    }
    
    private function add_log_item_pedido($id_item_pedido, $situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema)
    {
        return $this->Entregador_model->add_log_item_pedido($id_item_pedido, $situacao, $id_usuario, $reparte, $encalhe, $ip, $localizacao, $is_robot, $is_mobile, $agent, $plataforma, $navegador, $sistema );
    }

    public function do_encalhe()
    {
        if( $this->input->method(TRUE) && ( $_SERVER['HTTP_ORIGIN'].'/'=== base_url() ) )
        {
            $id = $this->input->post("id", TRUE);
            $encalhe = $this->input->post("encalhe", TRUE);

            if( !empty( trim($id) ) && !empty(trim($encalhe) ) )
            {
                $this->load->model('Entregador_model');
                
                $item_pedido = $this->Entregador_model->select_item_pedido($id);
                
                if( (int)$item_pedido['reparte']>=(int)$encalhe)
                {
                    $is_update = $this->Entregador_model->update_encalhe($id, $encalhe, 2);  
                    if ($is_update)
                    {
                        $id_usuario = (int)$this->session->userdata('id_usuario');
                        $ip = $this->get_client_ip();

                        $navegador = $this->agent->browser() . ' - ' . $this->agent->version();
                        $sistema = $this->agent->platform();
                        $id_item_pedido = (int)$id;

                        $robot = $this->agent->is_robot();
                        $mobile = $this->agent->is_mobile();
                        $agent = $this->agent->agent_string();
                        $platform = $this->agent->platform();
                        
                        $add_log_pedido = $this->add_log_pedido( (int)$item_pedido['id_pedido'], 2, $id_usuario, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                        $is_log = $this->add_log_item_pedido($id_item_pedido, 2, $id_usuario, $item_pedido['reparte'], $encalhe, $ip, $this->location(), $robot, $mobile, $agent, $platform, $navegador, $sistema);
                        
                        if($add_log_pedido)
                        {
                            echo json_encode( ['status'=>4, 'msg'=>''] );
                        }
                        else
                        {
                            if($is_log)
                            {
                                echo json_encode( ['status'=>1, 'msg'=>'<strong>Parabéns!</strong> Encalhe cadastrado com sucesso.'] );
                            }
                            else
                            {
                                echo json_encode( ['status'=>0, 'msg'=>'Problemas ao Salvar o Log.'] );
                            }
                        }
                    }
                    else
                    {
                        echo json_encode( ['status'=>2, 'msg'=>'Problemas ao Salvar o Encalhe.'] );
                    }
                }
                else
                {
                    echo json_encode( ['status'=>3, 'msg'=>'<strong>Atenção!</strong> A quantidade da Devolução não pode ser maior do que a quantidade da Entrega'] );
                }
            }
        }
        
    }

    public function home()
    {
        
        $id_usuario = $this->session->userdata('id_usuario');
        
        $this->load->model('Entregador_model');
        $pedidos = $this->Entregador_model->get_pedido_entregador($id_usuario);
        $pedidos = $this->ajuste_pedido_entregador($pedidos);
        
        $data = ['pedidos'=>$pedidos];
        
        $this->load->view('entregador/include/head.php');
        $this->load->view('entregador/pagina/pedido_entregador.php', $data);
        $this->load->view('entregador/include/footer.php');
    }
    
}
