<?php
?>
<style>
     .bs-example{
    	margin: 20px;
    }
    
    .btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  background: red;
  cursor: inherit;
  display: block;
}
.file-input-label {
	padding: 0px 10px;
	display: table-cell;
	vertical-align: middle;
  border: 1px solid #ddd;
  border-radius: 4px;
}
input[readonly] {
  background-color: white !important;
  cursor: text !important;
}

@media only screen and (max-width: 768px) {
/* For mobile phones: */
.btn-modal-entregador{
    width: 100%
}
}
</style>
    <br>
<main role="main" class="container-fluid">
        
    <h1 class="mt-5">Pedidos de Entregas</h1>
    <?php echo '<div class="alert alert-warning" role="alert">
                                <h4 class="alert-heading">Bem vindo Colaborador!</h4>
                                <p> Aqui você visualiza o seus pedidos de entrega solicitados pela Empresa Mottiva Entregas.</p>
                              </div>';
    ?>
    <hr class="mb-4">
    
                <?php
                    if( isset($pedidos[0]) )
                    {
                        echo '<div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>PDV</th>
                    <th>Reparte Total</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>';
                        
                        $td = '';
                        foreach ($pedidos as $pedido)
                        {
                            $td .=  '<tr>
                                    <td>'.$pedido['nome_pdv'].'</td>
                                    <td>'.$pedido['total_itens_reparte'].'</td>';
                            if($pedido['log_situacao'])
                            {
                                $td .= '<td><button data-name="'.$pedido['nome_pdv'].'" data-id="'.$pedido['id_pedido'].'" class="pedido_id btn btn-success btn-sm"><i class="fa fa-check-circle" aria-hidden="true"></i> Ok</button></td>';
                            }
                            else
                            {
                                $td .= '<td><button data-name="'.$pedido['nome_pdv'].'" data-id="'.$pedido['id_pedido'].'" class="pedido_id btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Realizar Encalhe</button></td>';
                            }
                            
                            $td .=     '</tr>';
                            
                        }
                        echo $td;
                        echo '</tbody>
        </table>
    </div>
</main>';    
                    
                    }
                    else
                    {
                        echo '<div class="alert alert-danger" role="alert">
                                Não há solicitações de pedido Cadastrado
                              </div>';
                    }
                ?>
            
</main>
<!-- Modal -->
    <!-- Modal -->
    <div class="modal fade" tabindex="-1" id="empModal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <!-- Modal content-->
            <div class="modal-content">
            </div>
        </div>
    </div>

    <br/>
<script>
$(document).ready(function() {
    $(document).on("input", ".input-encalhe", function() {
    this.value = this.value.replace(/\D/g,'');
});
    
});
    
function get_value_encalhe(valor) {
    
    var encalhe = $("input#encalhe-"+valor).val();
    
    if(encalhe.length!=0){
        
        $.ajax({
        url: 'save_encalhe',
        beforeSend: function( xhr ) {
              $('#encalhe-'+valor).prop('disabled', true);
              $('#id-encalhe-'+valor).hide();
              
              $(".btn-save-encalhe-"+valor).html('<button class="btn-load-encalhe btn btn-primary" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> </button>');
        },
        type: 'post',
        data: {id: valor, encalhe:encalhe},
        success: function(response){ 
            var resposta = JSON.parse(response);
            
            if(resposta.status==1){
                $(".btn-load-encalhe").hide();
                $(".btn-save-encalhe-"+valor).html('<button type="button" class="btn btn-success" ><i class="fa fa-check-circle" aria-hidden="true"></i></button>');
                $(".feedback").html('<div id="feed-sucesso" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>'+resposta.msg+'</div>');
                
                setTimeout(function() {
                     $("#feed-sucesso").remove();
                }, 10000);
            }
            else if(resposta.status==3){
                $('#encalhe-'+valor).prop('disabled', false);
                $('#id-encalhe-'+valor).show();
                
                $(".btn-save-encalhe-"+valor).html('<button onclick="get_value_encalhe('+valor+')" id="id-encalhe-'+valor+'" type="button" class="btn btn-primary">Salvar</button>');
                
                $(".feedback").html('<div id="feed-sucesso" class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button>'+resposta.msg+'</div>');
                
                setTimeout(function() {
                     $("#feed-sucesso").remove();
                }, 10000);
            }
            else if(resposta.status==4){
                document.location.reload(true);
            }
            else{
                $('#encalhe-'+valor).prop('disabled', false);
                $('#id-encalhe-'+valor).show();
                
                $(".btn-save-encalhe-"+valor).html('<button onclick="get_value_encalhe('+valor+')" id="id-encalhe-'+valor+'" type="button" class="btn btn-primary">Salvar</button>');
                
                $(".feedback").html('<div id="feed-sucesso" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>'+resposta.msg+'</div>');
                setTimeout(function() {
                     $("#feed-sucesso").remove();
                }, 10000);
            }
            
        }
      });
    }
}
</script>
<script>
$(document).ready(function(){

 $('.pedido_id').click(function(){
   
   var id = $(this).data('id');
   var name = $(this).data('name');
   // AJAX request
   $.ajax({
    url: 'modal_entregador',
    type: 'post',
    data: {id: id, name:name},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-content').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});    
</script>