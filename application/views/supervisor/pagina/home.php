<style>
    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
    /*
    */
.bs-example{
    margin: 20px;
}
    
    .btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  background: red;
  cursor: inherit;
  display: block;
}
.file-input-label {
	padding: 0px 10px;
	display: table-cell;
	vertical-align: middle;
  border: 1px solid #ddd;
  border-radius: 4px;
}
input[readonly] {
  background-color: white !important;
  cursor: text !important;
}

@media only screen and (max-width: 768px) {
/* For mobile phones: */
.btn-modal-super{
    width: 100%
}
}
</style>
    <br>
<main role="main" class="container">
        
    <h1 class="mt-5">Pedidos de Entregas</h1>
    <?php echo '<div class="alert alert-warning" role="alert">
                                <h4 class="alert-heading">Bem vindo Supervisor!</h4>
                              </div>';
    ?>
    <hr class="mb-4">
    
                <?php
                    if( isset($pedidos[0]) )
                    {
                        echo '<div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr class="row m-0"">
                    <th  class="col-6">ROTA</th>
                    <th  class="col-6">Ações</th>
                </tr>
            </thead>
            <tbody>';
                        $rotas = '';
                        foreach ($pedidos as $pedido)
                        {
                            $rotas .= '<tr class="row m-0"">
                                    <td  class="col-6">'.$pedido['nome_rota'].'</td>';
                            if($pedido['situacao']==3)
                            {
                                $rotas .= '<td  class="col-6"><button data-name="'.$pedido['nome_rota'].'" data-id="'.$pedido['id'].'" class="pedido_id btn btn-success btn-sm"><i class="fa fa-check-circle" aria-hidden="true"></i> Ok</button></td>';
                            }
                            else
                            {
                                $rotas .= '<td  class="col-6"><button data-name="'.$pedido['nome_rota'].'" data-id="'.$pedido['id'].'" class="pedido_id btn btn-primary btn-sm"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Avaliar</button></td>';
                            }
                            $rotas .= '</tr>';
                        }
                        echo $rotas;
            echo '</tbody>
        </table>
    </div>
</main>';    
                    
                    }
                    else
                    {
                        echo '<div class="alert alert-warning" role="alert">
                                Não há Rota para Avaliar
                              </div>';
                    }
                ?>
            
</main>
<!-- Modal -->
<div class="modal fade" tabindex="-1" id="empModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>

<br/>
<!--<script src="public/js/sweet/sweetalert.min.js"></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.min.css">-->
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>-->
<!--<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>-->
<script>
$(document).ready(function(){

 $('.pedido_id').click(function(){
   
   var id = $(this).data('id');
   var name = $(this).data('name');
   // AJAX request
   $.ajax({
    url: 'modal_supervidor',
    type: 'post',
    data: {id: id,name:name},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-content').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});    
</script>
<script>
$(document).on('click', '.btn-sweet-sim', function(e) {
//$(document).off('focusin.modal');

      var parentTag = $( this ).parent().prop('className');
      
      var input= $("#"+parentTag).val();
      var input_trim = input.trim();
      
      if(input_trim.length!=0){
           
      var str_data = $(this).data('id');
      var id_rota = $(this).val();
      var reparte = $(this).data('reparte');
      var encalhe = $(this).data('encalhe');
      
      $.ajax({
        url: 'valida_encalhe',
        beforeSend: function( xhr ) {
            
            $("#"+parentTag).prop('disabled', true);
            $("."+parentTag).children().hide(); 
            $("."+parentTag).html('<button class="btn btn-success"><span class="spinner-border spinner-border-sm"></span></button>');
        },
        type: 'post',
        data: {id_rota: id_rota, str_data:str_data, input:input_trim, reparte:reparte, encalhe:encalhe},
        success: function(response){ 
            
            var resposta = JSON.parse(response);
          if(resposta.status==1)
          {
              //$(".btn-not-encalhe-").hide();
              //<button type="button" class="btn btn-success"><i class="fa fa-check-circle" aria-hidden="true"></i></button>
              $("."+parentTag).children().hide(); 
              $("."+parentTag).html('<button type="button" class="btn btn-success"><i class="fa fa-check-circle" aria-hidden="true"></i></button>');
              
                Swal.fire(
                  'Sucesso!',
                  'Encale validado com Sucesso.',
                  'success'
                )
              //document.location.reload(true);
          }
          else
          {
              
          }
          
          
        }
      });

   }
   else{
    Swal.fire(
                  'Ops...',
                  'O valor digitado não pode ser vazio.',
                  'error'
                )
   }
});
</script>