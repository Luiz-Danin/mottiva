<!DOCTYPE html>
<html lang="pt-br">
    <head><title>Mottiva Home | Entregas Expresso</title>
        <base href="<?= base_url()?>">

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="public/img/site/favicon.ico" />
        <link href="public/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
        <link href="public/css/site/font-awesome.min.css" rel="stylesheet">
        <script src="public/js/jquery-3.5.1.min.js"></script>
        <script src="public/js/popper.min.js"></script>
        <script src="public/js/bootstrap/bootstrap.min.js"></script>
        <!--<script src="public/js/sweet/sweetalert2.all.min.js"></script>-->
        <link href="public/css/site/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <!-- Masthead-->
        <header>
            <!-- Fixed navbar -->
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <a href="<?= base_url()?>supervisor" class="navbar-brand">
                    <img src="public/img/site/logo.png" style="height: 40px" title="Mottiva">
                  </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse collapse" id="navbarCollapse" style="">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="supervisor"><i class="fa fa-home"></i> Inicio <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                    <p></p>
                    <p></p>
                    <a href="logout" class="btn btn-primary" role="button">Sair <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </div>
            </nav>
        </header>