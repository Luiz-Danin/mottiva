<!DOCTYPE html>
<html lang="pt-br">
    <head><title>Mottiva Home | Entregas Expresso</title>
        <base href="<?= base_url()?>">

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="public/img/site/favicon.ico" />
        <link href="public/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
        <link href="public/css/site/font-awesome.min.css" rel="stylesheet">
        <script src="public/js/jquery-3.5.1.min.js"></script>
        <script src="public/js/popper.min.js"></script>
        <script src="public/js/bootstrap/bootstrap.min.js"></script>
        <link href="public/css/site/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <!-- Masthead-->
        <header>
            <!-- Fixed navbar -->
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <!--<a class="navbar-brand" href="<?= base_url()?>entrega">Mottiva</a>-->
                <a href="<?= base_url()?>pdv" class="navbar-brand">
                    <img src="public/img/site/logo.png" style="height: 40px" title="Mottiva">
                  </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse collapse" id="navbarCollapse" style="">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="pdv"><i class="fa fa-home"></i> PDVs <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i> Cadastrar
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="add_area"><i class="fa fa-map" aria-hidden="true"></i> Areas</a>
                                <a class="dropdown-item" href=add_rota><i class="fa fa-location-arrow" aria-hidden="true"></i> Rotas</a>
                                <a class="dropdown-item" href="form_pdv"><i class="fa fa-map-marker" aria-hidden="true"></i> PDV</a>
                                <a class="dropdown-item" href="add_entregador"><i class="fa fa-user-plus" aria-hidden="true"></i> Entregador</a>
                            </div>
                        </li>
<!--                        <li class="nav-item">
                            <a class="nav-link" href="form_pdv"><i class="fa fa-map-marker"></i> Adicionar PDV <span class="sr-only">(current)</span></a>
                        </li>-->
                    </ul>
<!--                    <form class="form-inline mt-2 mt-md-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        <button class="btn btn-outline-success ml-2" type="submit"><i class="fa fa-search"></i> Buscar</button>
                    </form>-->
                    <br>
                    <a href="logout" class="btn btn-primary" role="button">Sair <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </div>
            </nav>
<!--            <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="https://www.e-palestras.com/transparente/inicio"><img style="width: 65px;" class="logo_palestra" src="https://www.e-palestras.com/transparente/public/img/logo_palestra.png"></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse collapse" id="navbarSupportedContent" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.e-palestras.com/transparente/inicio"><i class="fa fa-home"></i>
                            Minhas Palestras<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.e-palestras.com/transparente/pagina_cadastro_palestra"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Cadastrar Palestra</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.e-palestras.com/transparente/palestra_disponivel"><i class="fa fa-play-circle"></i>
                            Palestras Disponiveis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.e-palestras.com/transparente/palestra_matriculada"><i class="fa fa-graduation-cap"></i>Palestras Matriculadas</a>
                    </li>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Dropdown
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                          </li>
                </ul>
                <form method="post" action="https://www.e-palestras.com/transparente/busca_curso" class="form-inline my-2 my-lg-0">
                <div class="input-group">
                    <input id="id_busca" name="busca" required="" autocomplete="off" type="text" class="form-control" placeholder="Buscar Palestra" aria-label="Buscar Palestras" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </div>
                </form>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Buscar Palestras" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
        </nav>-->
        </header>