<?php
?>
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.pt-br.js" type="text/javascript"></script>
    <script src="public/js/bundle.min.js"></script>
    
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
    container {
        max-width: 960px;
    }

    .border-top { border-top: 1px solid #e5e5e5; }
    .border-bottom { border-bottom: 1px solid #e5e5e5; }
    .border-top-gray { border-top-color: #adb5bd; }

    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

    .lh-condensed { line-height: 1.25; }
</style>
<style>
  body {
    font-family: "Roboto", sans-serif;
  }

  .select-wrapper {
    margin: auto;
/*    max-width: 600px;
    width: calc(100% - 40px);*/
  }

  .select-pure__select {
    align-items: center;
    background: #f9f9f8;
    border-radius: 4px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);
    box-sizing: border-box;
    color: #363b3e;
    cursor: pointer;
    display: flex;
    font-size: 16px;
    font-weight: 500;
    justify-content: left;
    min-height: 44px;
    padding: 5px 10px;
    position: relative;
    transition: 0.2s;
    width: 100%;
  }

  .select-pure__options {
    border-radius: 4px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);
    box-sizing: border-box;
    color: #363b3e;
    display: none;
    left: 0;
    max-height: 221px;
    overflow-y: scroll;
    position: absolute;
    top: 50px;
    width: 100%;
    z-index: 5;
  }

  .select-pure__select--opened .select-pure__options {
    display: block;
  }

  .select-pure__option {
    background: #fff;
    border-bottom: 1px solid #e4e4e4;
    box-sizing: border-box;
    height: 44px;
    line-height: 25px;
    padding: 10px;
  }

  .select-pure__option--disabled {
    color: #e4e4e4;
  }

  .select-pure__option--selected {
    color: #e4e4e4;
    cursor: initial;
    pointer-events: none;
  }

  .select-pure__option--hidden {
    display: none;
  }

  .select-pure__selected-label {
    align-items: 'center';
    background: #5e6264;
    border-radius: 4px;
    color: #fff;
    cursor: initial;
    display: inline-flex;
    justify-content: 'center';
    margin: 5px 10px 5px 0;
    padding: 3px 7px;
  }

  .select-pure__selected-label:last-of-type {
    margin-right: 0;
  }

  .select-pure__selected-label i {
    cursor: pointer;
    display: inline-block;
    margin-left: 7px;
  }

  .select-pure__selected-label img {
    cursor: pointer;
    display: inline-block;
    height: 18px;
    margin-left: 7px;
    width: 14px;
  }

  .select-pure__selected-label i:hover {
    color: #e4e4e4;
  }

  .select-pure__autocomplete {
    background: #f9f9f8;
    border-bottom: 1px solid #e4e4e4;
    border-left: none;
    border-right: none;
    border-top: none;
    box-sizing: border-box;
    font-size: 16px;
    outline: none;
    padding: 10px;
    width: 100%;
  }

  .select-pure__placeholder--hidden {
    display: none;
  }
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5 text-center">Cadastrar Entrega</h1>
    <blockquote class="blockquote text-center">
        <p class="mb-0"><strong><?=$nome_ponto?></strong></p>
    </blockquote>

    
    <?php if( isset($_SESSION['cad_ponto_erro']) )
                    {
                        echo $_SESSION['cad_ponto_erro'];
                    }
              ?>
    <div class="row">
        <div class="col-md-2 order-md-2 mb-4">

        </div>
        <div class="col-md-8 order-md-2 mb-4">
            <form method="POST" action="do_entrega">
                
                <div class="row">
                    <div class="col-md-6 mb-3">
                        Data de Início: <input name="data_inicio" id="startDate" />
                    </div>
                    <div class="col-md-6 mb-3">
                        Data de Fim: <input name="data_fim" id="endDate" />
                    </div>
                </div>
                <input name="id_ponto" value="<?php echo $id_ponto;?>" type="hidden">
                <div class="produtos">
                    <input name="produtos[jn]" value="jn" type="hidden">
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="select-wrapper">
                            <h3>Produtos:</h3>
                            <span name="produto" class="autocomplete-select"></span>
                            <!--<button onclick="resetAutocomplete()">Limpar...</button>-->
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="country">Entregador:</label>
                        <select name="entregador" class="custom-select d-block w-100" id="country" required="">
                            <option value="">Selecione o Entregador...</option>
                            <?php 
                            if( isset($usuarios[0]) )
                            {
                                foreach ($usuarios as $usuario)
                                {
                                    echo '<option value="'.$usuario['id'].'">'.$usuario['nome'].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Cadastrar</button>
            </form>
        </div>
        <div class="col-md-2 order-md-2 mb-4">

        </div>
    </div>
</main>
<!--https://gijgo.com/LiveEdit/Index/daterangepicker.html?component=datepicker#-->
<script>
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#startDate').datepicker({
       locale: 'pt-br',
       format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        maxDate: function () {
            return $('#endDate').val();
        }
    });
    $('#endDate').datepicker({
        locale: 'pt-br',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function () {
            return $('#startDate').val();
        }
    });
</script>
<script>
//      var single = new SelectPure(".single-select", {
//        options: [
//          {
//            label: "New York",
//            value: 1,
//            disabled: true,
//          },
//          {
//            label: "San Francisco",
//            value: 2,
//          },
//          {
//            label: "Los Angeles",
//            value: 3,
//          },
//        ],
//        placeholder: "-Please select-",
//        onChange: value => { console.log(value); },
//        classNames: {
//          select: "select-pure__select",
//          dropdownShown: "select-pure__select--opened",
//          multiselect: "select-pure__select--multiple",
//          label: "select-pure__label",
//          placeholder: "select-pure__placeholder",
//          dropdown: "select-pure__options",
//          option: "select-pure__option",
//          autocompleteInput: "select-pure__autocomplete",
//          selectedLabel: "select-pure__selected-label",
//          selectedOption: "select-pure__option--selected",
//          placeholderHidden: "select-pure__placeholder--hidden",
//          optionHidden: "select-pure__option--hidden",
//        }
//      });
//      var resetSingle = function() {
//        single.reset();
//      };

//      var multi = new SelectPure(".multi-select", {
//        options: [
//          {
//            label: "New York",
//            value: "NY",
//          },
//          {
//            label: "Washington",
//            value: "WA",
//          },
//          {
//            label: "California",
//            value: "CA",
//          },
//          {
//            label: "New Jersey",
//            value: "NJ",
//          },
//          {
//            label: "North Carolina",
//            value: "NC",
//          },
//        ],
//        multiple: true,
//        icon: "fa fa-times",
//        placeholder: "-Please select-",
//        onChange: value => { console.log(value); },
//        classNames: {
//          select: "select-pure__select",
//          dropdownShown: "select-pure__select--opened",
//          multiselect: "select-pure__select--multiple",
//          label: "select-pure__label",
//          placeholder: "select-pure__placeholder",
//          dropdown: "select-pure__options",
//          option: "select-pure__option",
//          autocompleteInput: "select-pure__autocomplete",
//          selectedLabel: "select-pure__selected-label",
//          selectedOption: "select-pure__option--selected",
//          placeholderHidden: "select-pure__placeholder--hidden",
//          optionHidden: "select-pure__option--hidden",
//        }
//      });
//      var resetMulti = function() {
//        multi.reset();
//      };
//
//      var customIcon = document.createElement('img');
//      customIcon.src = './icon.svg';
//      var customIconMulti = new SelectPure(".multi-select-custom", {
//        options: [
//          {
//            label: "New York",
//            value: "NY",
//          },
//          {
//            label: "Washington",
//            value: "WA",
//          },
//          {
//            label: "California",
//            value: "CA",
//          },
//          {
//            label: "New Jersey",
//            value: "NJ",
//          },
//          {
//            label: "North Carolina",
//            value: "NC",
//          },
//        ],
//        value: ["NY", "CA"],
//        multiple: true,
//        inlineIcon: customIcon,
//        onChange: value => { console.log(value); },
//        classNames: {
//          select: "select-pure__select",
//          dropdownShown: "select-pure__select--opened",
//          multiselect: "select-pure__select--multiple",
//          label: "select-pure__label",
//          placeholder: "select-pure__placeholder",
//          dropdown: "select-pure__options",
//          option: "select-pure__option",
//          autocompleteInput: "select-pure__autocomplete",
//          selectedLabel: "select-pure__selected-label",
//          selectedOption: "select-pure__option--selected",
//          placeholderHidden: "select-pure__placeholder--hidden",
//          optionHidden: "select-pure__option--hidden",
//        }
//      });
//      var resetCustomMulti = function() {
//        customIconMulti.reset();
//      };

      var autocomplete = new SelectPure(".autocomplete-select", {
        options: [
          {
            label: "Jornal Semanal(R$1,50)",
            value: "jn",
          },
          {
            label: "Jornal fim de Semana(R$2,50)",
            value: "jnf",
          },
          {
            label: "Cesta Básica (Tipo 1)",
            value: "cb1",
          },
          {
            label: "Cesta Básica (Tipo 2)",
            value: "cb2",
          },
          {
            label: "Cesta Básica (Tipo 3)",
            value: "cb3",
          },
          {
            label: "Cesta Básica (Tipo 4)",
            value: "cb4",
          },
          {
            label: "Cesta Básica (Tipo 5)",
            value: "cb5",
          },
          {
            label: "Cesta Básica (Tipo 6)",
            value: "cb6",
          },
          {
            label: "Cesta Básica (Tipo 7)",
            value: "cb7",
          },
        ],
        value: ["jn"],
        multiple: true,
        autocomplete: true,
        icon: "fa fa-times",
        onChange: value => { console.log(value); },
        classNames: {
          select: "select-pure__select",
          dropdownShown: "select-pure__select--opened",
          multiselect: "select-pure__select--multiple",
          label: "select-pure__label",
          placeholder: "select-pure__placeholder",
          dropdown: "select-pure__options",
          option: "select-pure__option",
          autocompleteInput: "select-pure__autocomplete",
          selectedLabel: "select-pure__selected-label",
          selectedOption: "select-pure__option--selected",
          placeholderHidden: "select-pure__placeholder--hidden",
          optionHidden: "select-pure__option--hidden",
        }
      });
      var resetAutocomplete = function() {
        autocomplete.reset();
      };
    </script>
<script>
//$(document).ready(function () {
//    $(document).on("click", "i.fa.fa-times", function(e) {
//       alert( $( this ).attr('data-value') );
//    });
//$( "i.fa.fa-times" ).trigger( "click" );
//});

$(document).ready(function () {
        $( "i.fa.fa-times" ).on( "click", function(e) {
             e.preventDefault();
            alert( $( this ).attr('data-value') );
        //$( "i.fa.fa-times" ).trigger( "click" );
        });
        //$('i.fa.fa-times').click();
});

$( ".select-pure__option" ).on( "click", function() {
    $(".produtos").append('<input name="produtos['+$( this ).attr('data-value')+']" value="'+$( this ).attr('data-value')+'" type="hidden" >');
  //$("input.select-pure__autocomplete").attr("name","jn");
});
//$( "span.select-pure__selected-label" ).on( "click", function() {
//    alert("ddd");
//});

//$("input.select-pure__autocomplete").attr("name","jn");
    /*
var base_url = "<?php //echo base_url();?>";
$.ajax({
    url: base_url+"script.php",
    type: "POST",
    data: {},
    dataType: "html"

}).done(function(resposta) {
    console.log(resposta);

}).fail(function(jqXHR, textStatus ) {
    console.log("Request failed: " + textStatus);

}).always(function() {
    console.log("completou");
});*/
</script>