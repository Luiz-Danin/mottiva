<style>
    container {
        max-width: 960px;
    }

    .border-top { border-top: 1px solid #e5e5e5; }
    .border-bottom { border-bottom: 1px solid #e5e5e5; }
    .border-top-gray { border-top-color: #adb5bd; }

    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

    .lh-condensed { line-height: 1.25; }
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5 text-center">Cadastrar Entregador   </h1>
    
    <?php if( isset($_SESSION['cad_entregador_erro']) )
                    {
                        echo '<div class="row">';
                         echo '<div class="col-md-2 order-md-2 mb-4"></div>';
                            echo '<div class="col-md-8 order-md-2 mb-4">'.$_SESSION['cad_entregador_erro'].'</div>';
                         echo '<div class="col-md-2 order-md-2 mb-4"></div>';
                        echo '</div>';
                    }
              ?>
    <div class="row">
        <div class="col-md-2 order-md-2 mb-4">

        </div>
        <div class="col-md-8 order-md-2 mb-4">
            <form method="POST" action="do_entregador">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="nome">Nome:</label>
                        <input name="nome" type="text" class="form-control" id="nome" placeholder="Digite o Nome" required="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="email">E-mail:</label>
                        <input name="email" type="email" class="form-control" id="email" placeholder="Digite o E-mail" required="">
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-4">
                            <label for="ddd">DDD: <span class="text-muted">(Opcional)</span></label>
                            <input name="ddd" minlength="2" maxlength="2" type="tel" class="form-control" id="ddd" placeholder="Número do DDD">
                        </div>
                        <div class="col-md-8">
                            <label for="celular">Celular/Telefone: <span class="text-muted">(Opcional)</span></label>
                            <input name="celular" type="tel" class="form-control" id="celular" placeholder="Número do Celular">
                        </div>    
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="senha">Senha:</label>
                        <input name="senha" type="password" class="form-control" id="senha" placeholder="Digite a Senha" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="area">Selecione a Rota</label>
                    <select class="form-control" id="rota" required="" name="rota">
                        <option value="" selected>Selecione...</option>
                        <?php
                          foreach ($rotas as $rota)
                          {
                              echo '<option value="'.$rota['id'].'">'.$rota['nome_rota'].'</option>';
                          }
                        ?>
                    </select>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <div class="col-md-2 order-md-2 mb-4">

        </div>
    </div>
</main>
<?php unset($_SESSION['cad_entregador_erro']); ?>