<?php 
?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.pt-br.js" type="text/javascript"></script>
<style>
    @media only screen and (max-width: 768px) {
  /* For mobile phones: */
  }
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5 text-center">PDV: <?php echo $data_pdv[0]['nome_pdv'];?></h1>
    <button type="button" class="btn btn-success percentual" data-id="<?php echo $id_pdv;?>" data-toggle="modal" data-target="#myModal">
        Percentual <i class="fa fa-percent" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Cadastrar Pedido <i class="fa fa-chevron-right" aria-hidden="true"></i>
    </button>
    <a href="pdv" class="btn btn-info" role="button"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Voltar</a>
<!--    <a href="form_edit_pdv/<?php echo $data_pdv[0]['id'];?>" class="btn btn-success" role="button">
        Editar PDV <i class="fa fa-pencil fa-fw"></i>
    </a>-->
    <br><br>
    <div class="row">
        <div class="col-sm-3 col-md-2 col-5">
            <label style="font-weight:bold;">Responável:</label>
        </div>
        <div class="col-md-8 col-6">
            <?php echo $data_pdv[0]['funcionario'];?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-3 col-md-2 col-5">
            <label style="font-weight:bold;">Endereço:</label>
        </div>
        <div class="col-md-8 col-6">
            <?php echo $data_pdv[0]['endereco'];?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-3 col-md-2 col-5">
            <label style="font-weight:bold;">Número:</label>
        </div>
        <div class="col-md-8 col-6">
            <?php echo $data_pdv[0]['numero'];?>
        </div>
    </div>
    <hr />
    
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <!--<th>Nº</th>-->
                    <!--<th>Entregador</th>-->
                    <th>Produto</th>
                    <th>Ação</th
                </tr>
            </thead>
            <tbody>
                <?php
                    if( isset($pedidos[0]) )
                    {
                        //$increment = 1;
                        foreach ($pedidos as $pedido)
                        {
                            echo '<tr>
                                    <td>'.$pedido['id_produto'].'</td>
                                    <td><button data-id="'.$pedido['pedido_id'].'" class="pedido_id btn btn-success btn-sm"> <i class="fa fa-eye"> Itens</i></button> | <button disabled="" class="btn btn-danger btn-sm"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Homologar</button></td>
                                </tr>';
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>

</main>

<div class="container-fluid" >
   <!-- Modal -->
   <div class="modal fade" id="empModal" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100 text-center"> Itens do Pedido</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body-2">
 
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
      </div>
     </div>
    </div>
   </div>

   <br/>
  </div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
          <div class="container">
              <div class="row justify-content-center">
                  <h4>Percentual Semanal</h4>
              </div>
          </div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body-3">
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
          <button type="button" class="btn btn-success percents" style="width:100%" data-dismiss="modal">Salvar Todos</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cadastrar Pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="mt_pedido" method="POST" action="pedido">
                    <input type="hidden" name="id_pdv" value="<?php echo $id_pdv;?>">
                    <div class="form-group">
                        <label for="single" class="control-label">Selecione o Entregador</label><br>
                        <?php
                        echo '<select required="" class="js-example-basic-single js-states form-control"  id ="entregador" name="entregador">';
                        echo '<option></option>'; 
                        foreach ($entregador as $entrega)
                         {
                            foreach ($entrega as $ent)
                            {
                                echo '<option value="'.$ent['id'].'">'.$ent['nome'].'</option>';
                            }
                         }
                        
                        echo ' </select>';
                        
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Seleciono o Produto:</label>
                        <select class="form-control" name="produto" required="">
                            <?php
                                if( isset($produtos[0]))
                                {
                                    echo '<option selected value="">Selecione uma Opção...</option>';
                                    foreach ($produtos as $produto)
                                    {
                                        echo '<option value="'.$produto['id'].'">'.$produto['nome'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="dtinicio">Data Inicial:</label>
                                <input required="" autocomplete="off" id="startDate" width="auto" />
                                <!--<input type="text" class="form-control" id="dtinicio" placeholder="Data de Início" name="dtinicio">-->
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="dtfim">Data Final:</label>
                                <input required="" autocomplete="off" id="endDate" width="auto" />
                                <!--<input type="text" class="form-control" id="dtfim" placeholder="Data de Fim" name="dtfim">-->
                            </div>
                        </div>
                    </div>
                    <div class="build_week"></div>
                    
<!--                    <div class="form-group">
                        <label for="total_peso">Peso Total(KG):</label>
                        <input type="tel" class="form-control" required="" id="total_peso" placeholder="Digite o Peso" name="total_peso">
                    </div>-->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button id="mt_login" type="submit" class="btn btn-primary">Savar Pedido <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                        <button style="display:none;" id="loading" class="btn btn-primary" disabled="">
                            <span class="spinner-border spinner-border-sm"></span>
                            Aguarde...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>
<script>
    $('.percents').click(function(){
        var segunda = $("#segunda-feira").val();
        var terca = $("#terca-feira").val();
        var quarta = $("#quarta-feira").val();
        var quinta = $("#quinta-feira").val();
        var sexta = $("#sexta-feira").val();
        var sabado = $("#sabado").val();
        var domingo = $("#domingo").val();
        
        var id_pdv = "<?php echo $id_pdv;?>";
        //var dias = {key: 'val', key2: 'val2'};
        $.ajax({
            url: 'save_percentual',
            type: 'post',
            data: {id_pdv:id_pdv,'dias':{'segunda-feira': segunda, 'terca-feira':terca, 'quarta-feira':quarta, 'quinta-feira':quinta, 'sexta-feira':sexta, sabado:sabado, domingo:domingo}},
            success: function(response){ 
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Percentual Salvo com Sucesso!',
                showConfirmButton: false,
                timer: 3500
              })
            }
          });
        
    });
</script>
<script>
$(document).ready(function(){

 $('.percentual').click(function(){
   
   var id_pdv = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'modal_percentual',
    type: 'post',
    data: {id_pdv: id_pdv},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body-3').html(response);

      // Display Modal
      $('#myModal').modal('show'); 
    }
  });
 });
}); 
</script>
<script>
function get_peso(id){
    //somar todos os valores das cahves existentes
    var pesos = new Array();
    var peso = document.getElementById(id).value;
    var total_peso = document.getElementById("total_peso").value;
    
    //while (peso!='') {
        //pesos.push([id:peso]);
        //console.log(pesos);
      //}
    if(total_peso=='')
    {
        total_peso = 0;
        total_peso = total_peso + parseInt(peso);
    }
    else
    {
        total_peso = parseInt(total_peso) + parseInt(peso);
    }
        
    
    $('#total_peso').val(total_peso); 
}
</script>
<script>
$(document).ready(function(){

 $('.pedido_id').click(function(){
   
   var id = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'load_modal',
    type: 'post',
    data: {id: id},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body-2').html(response);

      // Display Modal
      $('#empModal').modal('show'); 
    }
  });
 });
});    
</script>
<script>
$('#startDate, #endDate').change(function(){
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    
    var base_url = "<?php echo base_url(); ?>";
    if( (startDate.length!==0) && (endDate.length!==0) )
    {
        
        $.ajax({
            url: base_url+"build_week",
            type: "post",
            data: {startDate:startDate,endDate:endDate} ,
            success: function (response) {
                 $(".build_week").html(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
        
    }
    startDate='';
    endDate='';

})
</script>
<script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#startDate').datepicker({
            locale: 'pt-br',
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            //minDate: today,
            maxDate: function () {
                return $('#endDate').val();
            }
        });
        $('#endDate').datepicker({
            locale: 'pt-br',
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            minDate: function () {
                return $('#startDate').val();
            }
        });
</script>
<script>
    
$(document).ready(function() {
    $('.js-example-basic-single').select2({mtt-
  placeholder: 'Selecione um Entregador',
  allowClear: true,
  width:"100%",
  
});
});
</script>
<script>
$(function() {
    $("#mt_pedido").submit(function(e) {

        $("#mt_login").hide();
        $("#loading").show();
    });

});

</script>