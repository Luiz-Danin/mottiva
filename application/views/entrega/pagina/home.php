<style>
     .bs-example{
    	margin: 20px;
    }
    
    .btn-file {
  position: relative;
  overflow: hidden;
}
.btn-file input[type=file] {
  position: absolute;
  top: 0;
  right: 0;
  min-width: 100%;
  min-height: 100%;
  font-size: 100px;
  text-align: right;
  filter: alpha(opacity=0);
  opacity: 0;
  background: red;
  cursor: inherit;
  display: block;
}
.file-input-label {
	padding: 0px 10px;
	display: table-cell;
	vertical-align: middle;
  border: 1px solid #ddd;
  border-radius: 4px;
}
input[readonly] {
  background-color: white !important;
  cursor: text !important;
}
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5">PDVs</h1>
    <?php if( isset($_SESSION['sucesso_cad_ponto']) )
          {
            echo $_SESSION['sucesso_cad_ponto'];
          }
    ?>
    <?php if( isset($_SESSION['sucesso_edit_ponto']) )
          {
            echo $_SESSION['sucesso_edit_ponto'];
          }
    ?>
    <?php if( isset($_SESSION['sucesso_cad_area']) )
          {
            echo $_SESSION['sucesso_cad_area'];
          }
    ?>
    <?php if( isset($_SESSION['sucesso_cad_rota']) )
          {
            echo $_SESSION['sucesso_cad_rota'];
          }
    ?>
    <?php if( isset($_SESSION['sucesso_cad_entregador']) )
          {
            echo $_SESSION['sucesso_cad_entregador'];
          }
    ?>
    <a href="form_pdv" class="btn btn-primary" role="button">Adicionar PDV <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
<!--        <span class="file-input btn btn-primary btn-file">
            Importar Arquivo&hellip; <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
        </span>-->
    <hr class="mb-4">
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>Nº</th>
                    <th>PDV</th>
                    <!--<th>Endereço</th>-->
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if( isset($pontos[0]) )
                    {
                        $number = 1;
                        foreach ($pontos as $ponto)
                        {
                            if( isset($ponto['is_entrega']) )
                            {
                                //$btn_detalhe = '|<a href="detalhe_ponto/'.$ponto['id'].'" class="btn btn-outline-warning btn-sm">Detalhes <i class="fa fa-info" aria-hidden="true"></i></a>';
                            }
                            echo '<tr>
                                    <td>'.$number.'</td>
                                    <td>'.$ponto['nome_pdv'].'</td>
                                    <td><a href="detalhe_pdv/'.$ponto['id'].'" class="btn btn-success btn-sm"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Entrar</a></td>
                                </tr>';
                            $number++;
                            //$btn_detalhe = '';
                        }
                        //<a href="detalhe_ponto/'.$ponto['id'].'" class="btn btn-outline-warning btn-sm">Detalhes <i class="fa fa-info" aria-hidden="true"></i></a>
                    }
                ?>
<!--                <tr>
                    <td>1</td>
                    <td>Clark</td>
                    <td>clarkkent@mail.com</td>
                    <td>Ações</td>
                </tr>-->
            </tbody>
        </table>
    </div>
</main>
<?php unset($_SESSION['sucesso_cad_ponto']); ?>
<?php unset($_SESSION['sucesso_cad_area']); ?>
<?php unset($_SESSION['sucesso_cad_rota']); ?>
<?php unset($_SESSION['sucesso_cad_entregador']); ?>
