<style>
    container {
        max-width: 960px;
    }

    .border-top { border-top: 1px solid #e5e5e5; }
    .border-bottom { border-bottom: 1px solid #e5e5e5; }
    .border-top-gray { border-top-color: #adb5bd; }

    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

    .lh-condensed { line-height: 1.25; }
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5 text-center">Cadastrar Rota</h1>
    
    <?php if( isset($_SESSION['cad_rota_erro']) )
                    {
                        echo '<div class="row">';
                         echo '<div class="col-md-2 order-md-2 mb-4"></div>';
                            echo '<div class="col-md-8 order-md-2 mb-4">'.$_SESSION['cad_rota_erro'].'</div>';
                         echo '<div class="col-md-2 order-md-2 mb-4"></div>';
                        echo '</div>';
                    }
              ?>
    <div class="row">
        <div class="col-md-2 order-md-2 mb-4">

        </div>
        <div class="col-md-8 order-md-2 mb-4">
            <form method="POST" action="do_rota">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="rota">Nome:</label>
                        <input name="rota" type="text" class="form-control" id="rota" placeholder="Digite um nome para a Rota" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="area">Selecione a Área</label>
                    <select class="form-control" id="area" required="" name="area">
                        <option value="" selected>Selecione...</option>
                        <?php
                          foreach ($areas as $area)
                          {
                              echo '<option value="'.$area['id'].'">'.$area['nome_area'].'</option>';
                          }
                        ?>
                    </select>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <div class="col-md-2 order-md-2 mb-4">

        </div>
    </div>
</main>
<?php unset($_SESSION['cad_rota_erro']); ?>