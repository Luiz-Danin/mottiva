<?php
//echo '<pre>';var_dump($area_rota);die;
?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<style>
    container {
        max-width: 960px;
    }

    .border-top { border-top: 1px solid #e5e5e5; }
    .border-bottom { border-bottom: 1px solid #e5e5e5; }
    .border-top-gray { border-top-color: #adb5bd; }

    .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

    .lh-condensed { line-height: 1.25; }
</style>
<main role="main" class="container-fluid">
    <br>
    <h1 class="mt-5 text-center">Editar Ponto</h1>
    <?php if( isset($_SESSION['erro_edit_ponto']) )
          {
                echo $_SESSION['erro_edit_ponto'];
          }
    ?>
    <div class="row">
        <div class="col-md-2 order-md-2 mb-4">

        </div>
        <div class="col-md-8 order-md-2 mb-4">
            <form method="POST" action="do_edit_pdv">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="ponto">Ponto de Entrega:</label>
                        <input autofocus="" name="ponto" type="text" class="form-control" id="firstName" placeholder="Nome do Ponto de Entrega" value="<?=$ponto['nome_pdv']?>" required="">
                    </div>
                </div>

                <div class="mb-3">
                    <label for="username">Funcionária(o) Responsável:</label>
                    <div class="input-group">
                        <!--            <div class="input-group-prepend">
                                        <span class="input-group-text">@</span>
                                    </div>-->
                        <input value="<?=$ponto['funcionario']?>" name="nome" required="" type="text" class="form-control" id="nome" placeholder="Nome do Responsável" required="" autocomplete="off">
                        <!--                        <div class="invalid-feedback" style="width: 100%;">
                                                    Your username is required.
                                                </div>-->
                    </div>
                </div>

                <div class="mb-3">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="ddd">DDD: <span class="text-muted">(Opcional)</span></label>
                            <input name="ddd" minlength="2" maxlength="2" value="<?=$ponto['ddd']?>" type="tel" class="form-control" id="ddd" placeholder="Número do DDD">
                        </div>
                        <div class="col-md-8">
                            <label for="celular">Celular/Telefone: <span class="text-muted">(Opcional)</span></label>
                            <input name="celular" value="<?=$ponto['telefone']?>" type="tel" class="form-control" minlength="8" maxlength="9" id="celular" placeholder="Número do Celular">
                        </div>    
                    </div>
                </div>

                <div class="mb-3">
                    <label for="cep">CEP: <span class="text-muted">(Opcional)</span></label>
                    <input name="cep" value="<?=$ponto['cep']?>" type="tel" class="form-control" id="cep" placeholder="Digite o CEP" autocomplete="off">
                </div>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="endereco">Endereço: <span class="text-muted">(Opcional)</span></label>
                            <input name="endereco" value="<?=$ponto['endereco']?>" type="text" class="form-control" id="endereco" placeholder="Digite o Endereço" required="" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="numero">Número: <span class="text-muted">(Opcional)</span></label>
                            <input name="numero" value="<?=$ponto['numero']?>" type="tel" class="form-control" id="numero" placeholder="Digite o Número" required="" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="complemento">Complemento: <span class="text-muted">(Opcional)</span></label>
                    <input name="complemento" value="<?=$ponto['complemento']?>" type="text" class="form-control" id="complemento" placeholder="Digite o Complemento do Endereço" autocomplete="off">
                </div>
                <div class="mb-3">
                    <div class="form-group">
                        <label for="single" class="control-label">Selecione a Área/Rota</label><br>
                        <?php
                        echo '<select class="js-example-basic-single js-states form-control"  name="id_rota">'
                        . '<option></option>';

                        $grupo = '';
                        foreach ($area_rota as $rota) {
                          if ($grupo != $rota['nome_area']) {
                            if ($grupo != '') {
                              echo '</optgroup>';
                            }
                            echo '<optgroup label="'.ucfirst($rota['nome_area']).'">';
                          }
                          
                          if($rota['id_rota']==$ponto['id_rota'])
                          {
                              echo '<option selected="selected" value="'.$rota['id_rota'].'">'.htmlspecialchars($rota['nome_rota']).'</option>';
                          }
                          else
                          {
                             echo '<option value="'.$rota['id_rota'].'">'.htmlspecialchars($rota['nome_rota']).'</option>'; 
                          }
                          
                          $grupo = $rota['nome_area'];    
                        }
                        if ($grupo != '') {
                          echo '</optgroup>';
                        }
                        echo ' </select>';
                        
                        ?>
                    </div>
                </div>
                <input type="hidden" name="id_pdv" value="<?php echo $ponto['id'];?>">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Salvar</button>
            </form>
        </div>
        <div class="col-md-2 order-md-2 mb-4">

        </div>
    </div>
</main>
<script>
    
$(document).ready(function() {
    $('.js-example-basic-single').select2({
  placeholder: 'Selecione uma Rota',
  allowClear: true,
  width:"100%",
  tags:true
});
});
</script>
<script>
$(".loading").hide();

$("select#id_area").change(function(){
    var base_url = "<?php echo base_url();?>";
    var id_area = $(this). children("option:selected"). val();
    
    if(id_area.length!=0){
        
        $.ajax({
            beforeSend: function( xhr ) {
                $(".disable_list_rota").hide();
                $(".lista_rota").hide();
                $(".loading").show();
              },
            url: base_url + "lista_rota",
            type: "POST",
            dataType:"html",
            data: {id_area:id_area},
            success: function(data){
                 $(".loading").hide();
                 $(".lista_rota").show();
                 $('.lista_rota').html(data);
                 
                /*setTimeout(function(){
                    $(".loading").hide();
                    $(".lista_rota").show();
                    $('.lista_rota').html(data);                    
                },0);*/
            }
        });
    
    }
    else
    {
        $( "select#id_rota" ).prop( "disabled", true );
    }
})
</script>
<script>
$(document).ready(function() {
$('#cep').on('input', function () {
               $(this).val($(this).val().replace(/[^0-9]/g, ""));
           });
            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#endereco").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "" || cep.length==8) {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#endereco").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#endereco").val(dados.logradouro);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
</script>