<!-- About-->
<section class="page-section bg-primary" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="text-white mt-0">Temos o que você precisa!</h2>
                <hr class="divider light my-4" />
                <p class="text-white-50 mb-4">Inicie o Bootstrap tem tudo o que você precisa para colocar seu novo site em funcionamento em pouco tempo! Escolha um dos nossos aplicativos de código aberto, gratuitos para download e fáceis de usar! Sem condições!</p>
                <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Iniciar</a>
            </div>
        </div>
    </div>
</section>
<!-- Services-->
<section class="page-section" id="services">
    <div class="container">
        <h2 class="text-center mt-0">A seu serviço</h2>
        <hr class="divider my-4" />
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <i class="fas fa-4x fa-gem text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Temas robustos</h3>
                    <p class="text-muted mb-0">Nossos temas são atualizados regularmente para mantê-los livres de bugs!</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Atualizado</h3>
                    <p class="text-muted mb-0">Todas as dependências são mantidas atualizadas para manter as coisas novas.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <i class="fas fa-4x fa-globe text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Pronto para publicar</h3>
                    <p class="text-muted mb-0">Você pode usar este design como está ou pode fazer alterações!</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <i class="fas fa-4x fa-heart text-primary mb-4"></i>
                    <h3 class="h4 mb-2">Feito com amor</h3>
                    <p class="text-muted mb-0">É realmente open source se não for feito com amor?</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Portfolio-->
<section id="portfolio">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/1.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/1.jpg" alt="" />
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a
                >
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/2.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/2.jpg" alt="" />
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a
                >
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/3.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/3.jpg" alt="" />
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a
                >
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/4.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/4.jpg" alt="" />
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/5.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/5.jpg" alt="" />
                    <div class="portfolio-box-caption">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a
                >
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="public/img/site/portfolio/fullsize/6.jpg"
                   ><img class="img-fluid" src="public/img/site/portfolio/thumbnails/6.jpg" alt="" />
                    <div class="portfolio-box-caption p-3">
                        <div class="project-category text-white-50">Categoria</div>
                        <div class="project-name">Nome do Projeto</div>
                    </div></a
                >
            </div>
        </div>
    </div>
</section>
<!-- Call to action-->
<section class="page-section bg-dark text-white">
    <div class="container text-center">
        <h2 class="mb-4">Download grátis no Start Bootstrap!</h2>
        <a class="btn btn-light btn-xl" href="#">Download Now!</a>
    </div>
</section>
<!-- Contact-->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="mt-0">Vamos entrar em contato!</h2>
                <hr class="divider my-4" />
                <p class="text-muted mb-5">Pronto para começar seu próximo projeto conosco? Ligue para nós ou envie um e-mail e entraremos em contato com você o mais breve possível!

</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                <div>+1 (555) 123-4567</div>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fas fa-envelope fa-3x mb-3 text-muted"></i
                ><!-- Make sure to change the email address in BOTH the anchor text and the link target below!--><a class="d-block" href="mailto:contact@yourwebsite.com">contact@yourwebsite.com</a>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->