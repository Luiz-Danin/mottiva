<?php

?>
<!DOCTYPE html>
<html lang="en">
    <head><title>Site Mottiva</title>
        <base href="<?= base_url()?>">

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="public/img/site/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="public/js/site/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Third party plugin CSS-->
        <link href="public/css/site/magnific-popup.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="public/css/site/styles.css" rel="stylesheet" />
        
        <script src="public/js/jquery.min.js"></script>
        <script src="public/js/bootstrap/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="public/js/jquery.easing.min.js"></script>
        <script src="public/js/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="public/js/site/scripts.js"></script>
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container">
<!--                <a href="<?= base_url()?>" class="navbar-brand">
            <img src="http://www.mottivaentregas.com.br/wp-content/themes/yeti-bootstrap/assets/img/logo.png" title="Mottiva">
          </a>-->
                <a class="navbar-brand js-scroll-trigger" href="<?= base_url()?>">Mottiva</a><button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="<?= base_url()?>login">Login</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">Sobre</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">Serviços</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">SUA FONTE FAVORITA DE TEMAS GRATUITOS DE INICIALIZAÇÃO</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">Iniciar O Bootstrap pode ajudá-lo a criar sites melhores usando a estrutura do Bootstrap! Basta baixar um tema e começar a personalizar, sem amarras!</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Descubra mais</a>
                    </div>
                </div>
            </div>
        </header>