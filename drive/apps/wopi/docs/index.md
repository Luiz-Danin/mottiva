# Office Online in Nextcloud

![https://nextcloud.com/wp-content/themes/next/assets/img/features/Collabora_with_Talk_in_sidebar_1.png?x53054](https://nextcloud.com/wp-content/themes/next/assets/img/features/Collabora_with_Talk_in_sidebar_1.png?x53054)
Office Online supports editing your documents in real time with multiple other editors, showing high fidelity, WYSIWYG rendering and preserving the layout and formatting of your documents.

Users can insert and reply to comments and invite others without a Nextcloud account for anonymous editing of files with a public link shared folder.

Office Online supports dozens of document formats including DOC, DOCX, PPT, PPTX, XLS, XLSX + ODF, Import/View Visio, Publisher and many more...

Office Online Development Edition (CODE) is free and under heavy development, adding features and improvements all the time! Enterprise users have access to the more stable, scalable Office Online Enterprise version through a Nextcloud support subscription.

We are able to provide a solution for Online Office for the entire Nextcloud community through our partnership with Collabora in an easy to use docker image for developers and home users. Enterprise users looking for a more reliable solution should contact Nextcloud Sales.
