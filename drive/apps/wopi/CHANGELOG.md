# Changelog

## 3.5.1

### Added

- Static link for file

### Fixed

- Fix error after convert of binary file to oxml

