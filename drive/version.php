<?php 
$OC_Version = array(19,0,4,2);
$OC_VersionString = '19.0.4';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '18.0' => true,
    '19.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2020-10-08T07:49:37+00:00 60cb07794236c445620aa29a13eaf14ee79684df';
$vendor = 'nextcloud';
